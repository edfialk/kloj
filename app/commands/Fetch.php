<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Feed;
use App\Models\Kloj;
use App\Models\Post;

class Fetch extends Command {

	protected $name = 'fetch';
	protected $description = 'Fetch feed items.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$target = $this->argument('target');
		$validator = Validator::make(
			array('target' => $this->argument('target')),
			array('target' => array('regex:/^(all|feed:[0-9]+(-[0-9]+)?|kloj:[0-9]+|post:[0-9]+)$/i'))
		);
		if ($validator->fails()){
			$messages = $validator->messages();
			foreach($messages->all() as $message){
				$this->error($message);
			}
			return;
		}

		$this->validate = $this->option('validate');

		$this->info('Fetching '.$target);
		if ($this->validate){
			$this->info('Validating');
		}

		try{
			$this->fetch($target);
		}catch(Exception $e){
			$this->error($e->getMessage());
			$this->error($e->getFile());
			$this->error($e->getLine());
			$this->error($e->getTrace());
			Log::error($e);
		}

		// $targets = $this->parseTarget($target);
		// $this->fetch($targets);

	}

	protected function fetch($target)
	{
		$max = $this->option('max');
		$items = $this->getItems($target);
		$type = join('', array_slice(explode('\\', get_class($items)), -1));
		if ($type == 'Collection'){
			$items->each(function($feed) use ($max){
				if (!is_null($max)){
					$feed->post_max = $max;
				}
				$feed->fetch($this->validate);
			});
		}else if ($type == 'Feed'){
			if (!is_null($max)){
				$items->post_max = $max;
			}
			$items->fetch($this->validate);
		}else if ($type == 'Post'){
			$items->images()->detach();
			$items->fetch();
		}
	}

	protected function getItems($target)
	{
		if ($target == 'all'){
			return Feed::all();
		}

		$type = explode(":", $target);
		if ($type[0] == 'kloj'){
			$id = $type[1];
			$kloj = Kloj::find($id);
			if (!$kloj){
				$this->error('No kloj with that id.');
				return;
			}

			return $kloj->feeds;

		}else if ($type[0] == 'post'){
			$id = $type[1];
			$post = Post::find($id);
			if (!$post){
				$this->error('No post with that id.');
				return;
			}

			return $post;

		}else if ($type[0] == 'feed'){ //feeds
			$id = $type[1];
			if (stripos($id, '-')){
				$parts = explode('-', $id);
				$start = $parts[0];
				$length = $parts[1];
				return Feed::all()->slice($start, $length);
			}else{
				$feed = Feed::find($id);
				if (!$feed){
					$this->error('No feed with that id.');
					return;
				}

				return $feed;

			}
		}else{
			//shouldn't reach here due to validation, but just in case
			$this->error('Invalid fetch target.');
			return;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		//array($name, $mode, $description, $defaultValue)
		return array(
			array('target', InputArgument::OPTIONAL, 'what to fetch - all, feed:{id}, feed:{start}-{length}, kloj:{id}, post:{id}', 'all'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		//array($name, $shortcut, $mode, $description, $defaultValue)
		return array(
			array('validate', null, InputOption::VALUE_NONE, 'Validate all items in current feeds.'),
			array('max', null, InputOption::VALUE_OPTIONAL, 'Max number of items to fetch per feed.'),
		);
	}

}
