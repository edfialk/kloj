<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageitemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('imageitem')){
			Schema::drop('imageitem');
		}

    Schema::create('imageitem', function($table) {
      $table->increments('id');

      $table->unsignedInteger('post_id');
      $table->unsignedInteger('image_id');

      // $table->foreign('post_id')->references('id')->on('post');
      // $table->foreign('image_id')->references('id')->on('image');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imageitem');
	}

}
