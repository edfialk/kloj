<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemeattributeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('themeattribute')){
			Schema::drop('themeattribute');
		}

    Schema::create('themeattribute', function($table) {
      $table->increments('id');

      $table->unsignedInteger('theme_id');
      $table->string('token');
      $table->string('value');

      // $table->foreign('theme_id')->references('id')->on('theme');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('themeattribute');
	}

}
