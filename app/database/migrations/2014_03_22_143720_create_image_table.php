<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('image')){
			Schema::drop('image');
		}

    Schema::create('image', function($table) {
      $table->increments('id');

      $table->string('url')->unique();
      $table->double('ratio')->default(0);
      $table->integer('width')->default(0);
      $table->integer('height')->default(0);

      $table->string('video')->nullable();
      $table->text('caption')->nullable();

      $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
