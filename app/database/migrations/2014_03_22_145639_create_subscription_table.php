<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('subscription')){
			Schema::drop('subscription');
		}

    Schema::create('subscription', function($table) {
      $table->increments('id');

      $table->unsignedInteger('user_id');
      $table->unsignedInteger('kloj_id');

      // $table->foreign('user_id')->references('id')->on('users');
      // $table->foreign('kloj_id')->references('id')->on('kloj');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscription');
	}

}
