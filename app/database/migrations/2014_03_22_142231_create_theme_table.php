<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('theme')){
			Schema::drop('theme');
		}

    Schema::create('theme', function($table) {
      $table->increments('id');

      $table->unsignedInteger('user_id');
      $table->unsignedInteger('template_id');

      // $table->foreign('user_id')->references('id')->on('users')->nullable();
      // $table->foreign('template_id')->references('id')->on('template');

      $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('theme');
	}

}
