<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKlojTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('kloj')){
			Schema::drop('kloj');
		}

    Schema::create('kloj', function($table) {
      $table->increments('id');
      $table->string('title');
      $table->unsignedInteger('theme_id');
			$table->unsignedInteger('user_id')->nullable();
			$table->text('description')->nullable();
			$table->string('url')->unique()->nullable();
			$table->boolean('isPublic')->default(true);
			$table->boolean('isFeatured')->default(false);

      $table->timestamps();

			// $table->foreign('theme_id')->references('id')->on('theme');
			// $table->foreign('user_id')->references('id')->on('users')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kloj');
	}

}
