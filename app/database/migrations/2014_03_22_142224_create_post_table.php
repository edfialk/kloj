<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('post')){
			Schema::drop('post');
		}

    Schema::create('post', function($table) {
      $table->increments('id');
      $table->string('title');
      $table->string('url')->unique();

			$table->text('text')->nullable();
			$table->string('permalink')->unique()->nullable();
			$table->text('extract')->nullable();
			$table->string('author')->nullable();

			$table->dateTime('date');
      $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post');
	}

}
