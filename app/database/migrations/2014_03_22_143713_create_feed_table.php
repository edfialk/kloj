<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('feed')){
			Schema::drop('feed');
		}

    Schema::create('feed', function($table) {
      $table->increments('id');

      $table->string('url')->unique();
      $table->string('title')->unique();
      $table->string('link');
      $table->string('icon')->nullable();
      $table->timestamps();

      // $table->foreign('category_id')->references('id')->on('category');      
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feed');
	}

}
