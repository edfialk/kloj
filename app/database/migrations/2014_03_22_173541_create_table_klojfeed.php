<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKlojfeed extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('klojfeed')){
			Schema::drop('klojfeed');
		}

    Schema::create('klojfeed', function($table) {
      $table->increments('id');
      $table->unsignedInteger('kloj_id');
			$table->unsignedInteger('feed_id');

			// $table->foreign('theme_id')->references('id')->on('theme');
			// $table->foreign('user_id')->references('id')->on('users')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('klojfeed');
	}

}
