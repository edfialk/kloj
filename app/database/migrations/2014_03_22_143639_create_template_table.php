<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('template')){
			Schema::drop('template');
		}

    Schema::create('template', function($table) {
      $table->increments('id');

      $table->enum('file', array('list', 'grid', 'brick', 'book', 'mag'));
      $table->string('name');
      $table->string('description')->nullable();
      $table->text('attributes');

      $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('template');
	}

}
