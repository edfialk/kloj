<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeditemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if (Schema::hasTable('feeditem')){
			Schema::drop('feeditem');
		}

		Schema::create('feeditem', function($table) {
      $table->increments('id');

      $table->unsignedInteger('post_id');
      $table->unsignedInteger('feed_id');

      // $table->foreign('post_id')->references('id')->on('post');
      // $table->foreign('feed_id')->references('id')->on('feed');

      $table->string('guid')->unqiue();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feeditem');
	}

}
