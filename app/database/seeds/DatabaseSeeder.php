<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
    // $this->call('SentrySeeder');
    // $this->command->info('Sentry tables seeded!');

    $this->call('KlojSeeder');
    $this->command->info('Kloj tables seeded!');

    $this->call('PostSeeder');
    $this->command->info('Post tables seeded!');
	}

}