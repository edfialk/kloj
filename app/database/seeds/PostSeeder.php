<?php

use App\Models\Kloj;

class PostSeeder extends Seeder {

    public function run()
    {
        $posts = array(
            array(
                'title'   => 'First post',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'author' => 'some author',
                'url' => 'http://www.google1.com',
                'date' => '2014-03-22'
            ),
            array(
                'title'   => 'Second post',
                'theme_id' => 1,
                'user_id' => 1,
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'author' => 'some author',
                'url' => 'http://www.google2.com',
                'date' => '2014-03-22'
            ),
            array(
                'title'   => 'Third post',
                'theme_id' => 1,
                'user_id' => 1,
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'author' => 'some author',
                'url' => 'http://www.google3.com',
                'date' => '2014-03-22'
            )
        );

        //truncate the table when we seed
        DB::table('post')->truncate();
        DB::table('post')->insert($posts);
    }

}