<?php

use App\Models\Image;
use App\Models\Post;

/**
 * Kloj Parser Class
 *
 * Helper functions for parsing html, domnodes, etc.
 *
 */

class Parser {

	public function __construct(Post &$post){
		if (!isset($post->url)){
			throw new Exception('Cannot parse post without a url.');
		}

		$this->post = $post;
		$this->doc = $this->getDomDoc($post->url);
		if (!$this->doc){
			throw new Exception('Error getting domDocument for url: '.$post->url);
		}
		$this->xpath = new \DomXpath($this->doc);
	}

	public function queryOne($query, $context = null){
		$node = is_null($context) ? $this->xpath->query($query) : $this->xpath->query($query, $context);
		if (count($node) > 0){
			return $node->item(0);
		}
		return false;
	}

	public function queryEach($query, $context = null){
		return is_null($context) ? $this->xpath->query($query) : $this->xpath->query($query, $context);
	}

	public function getExtract(){
		if (!isset($this->content)){
			throw new Exception("Parser can't get extract without content node.");
		}
		$this->clean();
		$text = $this->doc->saveHTML($this->content);
		return self::trimspaces($text);
		return $text;
	}

	public function remove($queries, $context = null){
		if (is_string($queries)){
			$queries = array($queries);
		}
		if (is_null($context)){
			$context = $this->content;
		}
		foreach($queries as $query){
			$list = $this->queryEach($query, $context);
			if (!$list){
				throw new Exception('Invalid query: '.$query);
			}
			if ($list->length > 0){
				foreach($list as $result){
					$result->parentNode->removeChild($result);
				}
			}
		}
	}

	public function clean(){
		$this->removeTagNodes(['link', 'noscript', 'nav']); //script'

		$this->removeClassNodes([
			'fb-like', 'related', 'share-tools', 'share_tools', 'share-post', 'social', 'adbreak', 'hidden',
	    	'voteDivContent', 'hide', 'follow-button', 'share'
	  	], true);

	  	$this->removeIdNodes(['comments'], true);

	  	$this->remove([
	  		'.//*[contains(@style, "display-none")]',
	  		'.//img[@width="1"] | .//img[@height="1"]',
	  	]);

		$this->removeClasses(['post', 'container', 'row', 'post-title', 'post-image', 'embed', 'embed-wrap'], true);
		$this->removeAttributes(['style', 'onclick', 'data-js', 'itemprop']); //'width', 'height'

		foreach($this->queryEach('.//*[@href]', $this->content) as $a){
		    if (stripos($a->getAttribute('href'), 'javascript') !== false){
		    	$a->setAttribute('href', $this->post->url);
		    }
		    if (stripos($a->getAttribute('href'), 'http') === false){
	    	    $a->setAttribute('href', $this->resolve_href($this->post->url, $a->getAttribute('href')));
		    }
		}

		foreach($this->queryEach('.//img', $this->content) as $img){
		    if (!$img->hasAttribute('src') && $img->hasAttribute('data-src')){
		      $img->setAttribute('src', $img->getAttribute('data-src'));
		      $img->removeAttribute('data-src');
		    }
		    if (!$img->hasAttribute('src') && $img->hasAttribute('data-lazy-src')){
		      $img->setAttribute('src', $img->getAttribute('data-lazy-src'));
		      $img->removeAttribute('data-lazy-src');
		    }
		}

		foreach($this->queryEach('.//*[@src]', $this->content) as $test){
		    if (stripos($test->getAttribute('src'), 'http') === false){
	    	    $old = $test->getAttribute('src');
	        	$new = $this->resolve_href($this->post->url,$old);
	        	$test->setAttribute('src', $new);
		    }
		}

		foreach($this->queryEach('.//form', $this->content) as $form){
		    if ($form->hasAttribute('onsubmit') || $form->hasAttribute('action')){
		      $form->parentNode->removeChild($form);
		    }
		}

	    foreach($this->queryEach('//img[@data-lazy-src]') as $img){
    		$img->setAttribute('src', $img->getAttribute('data-lazy-src'));
    		$img->removeAttribute('data-lazy-src');
	    }


		  //remove empty nodes until none empty left.
		  //i want to do it recursive, this only does 1 level
		  //actually this removes <style>tags too..god damn it
		  // $query = '*[not(*)] | //*[text() = "&nbsp;"]';
		  // $this->remove($query, $this->content);



		  // $empties = $this->queryEach($query, $this->content);
		  // while( $empties ) > 0 ){
		  // 	foreach($empties as $empty){
		  // 		$empty->parentNode->removeChild($empty);
		  // 	}
		  // }

		  //remove HTML comments
		foreach($this->queryEach('.//comment()', $this->content) as $comment){
		  	$comment->parentNode->removeChild($comment);
		}

	}

	/**
	 * Build DomDocument for url
	 * @param  string $url
	 * @return DomDocument
	 */
	public static function getDomDoc($url){
		$options  = array('http' => array('user_agent' => 'Kloj'));
		$context  = stream_context_create($options);
		// $response = file_get_contents('http://domain/path/to/uri', false, $context);
		$html = @file_get_contents($url, false, $context);
		if (!$html){
			return null;
		}
		return self::getDomDocFromText($html);
	}
	/**
	 * Build DomDocument from text string
	 * @param  string $text html string
	 * @return DomDocument
	 */
	public static function getDomDocFromText($text){
	  $doc = new \DOMDocument();
	  libxml_use_internal_errors(true);

	  //supposed to be "all-encompassing" for broken-ish html, seems to fix some problems
	  @$doc->loadHTML('<?xml encoding="UTF-8"><root>'.$text.'</root>');
	  foreach($doc->childNodes as $item){
	    if ($item->nodeType == XML_PI_NODE){
	      $doc->removeChild($item);
	    }
	  }
	  $doc->encoding = 'UTF-8';
	  return $doc;
	}

	/**
	 * Check if url is an embedded video
	 * @param  string  $url  iframe source url
	 * @param  Post    $post post model
	 * @return Boolean       true if found, else false
	 */
	public function getVideoDataFromEmbed($url){
	  if (stripos($url, "youtube.com/embed") !== false){
	    $id = self::getYoutubeId($url);
	    if ($id == false){
	      Log::error("failed to get youtube id for src: ".$url);
	      return false;
	    }
	    $image = self::getYoutubeThumbnail($id);
	    // $this->post->addImage($image);
	    return $image;
	  }
	  if (stripos($url, "player.vimeo.com") !== false){
	    $id = self::getVimeoId($url);
	    if ($id == false){
	      Log::error("failed to get vimeo id for src: ".$url);
	      return false;
	    }
	    $image = self::getVimeoThumbnail($id);
	    // $this->post->addImage($image);
	    return $image;
	  }
	  return false;
	}

	/**
	 * Pick largest (by area) image that's greater than min_area not present in skips array
	 * If $min_area is provided, first larger image returned.
	 * @param  array  $urls             array of urls or array of image arrays ('url', 'width', 'caption', etc.)
	 * @param  integer $min_area        smallest
	 * @param  array  $skips           	array of image urls (or pieces) to skip
	 * @return Image if found or null
	 */
	public static function pickImageFromArray(array $urls, $min_area = null, array $skips = null){
		$max_url = null;
		$max_img = null;
		$max_area = 1; //start at 1 due to 1x1 tracking images

		$_IMAGE_SKIPS = array(
			'sprite', 'rss-icon', 'kotaku-touch-icon',
			'http://www.wired.com/geekmom/wp-content/uploads/2013/03/DSC_2569-e1364504081126.jpg'
		);

		if ($skips !== null){
			$_IMAGE_SKIPS = array_merge($_IMAGE_SKIPS, $skips);
		}

		foreach($urls as $image){
			// echo "testing url: $image\n";
			if (empty($image)) continue;
			if (is_string($image)){
				$image = array('url' => $image);
			}

			if (stripos($image['url'], " ") !== false){  //some people just can't make a gd web address, it's like half-encoded
			  $image['url'] = str_replace(" ", "%20", $image['url']);
			}

			foreach($_IMAGE_SKIPS as $skip){
			  if (stripos($image['url'], $skip)) continue 2; //new $item
			}

			// echo $image['url']."\n";

			// testing getting image size without dling whole img
			$ch = curl_init($image['url']);
			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			$data = curl_exec($ch);
			curl_close($ch);
			if ($data === false){
				continue;
			}

			$contentLen = 0;
			if (preg_match('/Content-Length: (\d+)/', $data, $matches)) {
				$contentLen = (int)$matches[1];
			}
			// echo $contentLen." vs $max_area \n";
			if ($min_area !== null & $contentLen > $min_area){
				return $image;
			}
			if ($contentLen > $max_area) {
			  	$max_area = $contentLen;
			  	$max_img = $image;
			}
		}


		if (isset($max_img)){
			return $max_img;
		}

	  	return null;
	}

	/**
	 * Remove nodes by tag ("div", "img", "script", etc)
	 * @param array of strings $tags tags to remove
	 */
	public function removeTagNodes(array $tags){
		if (!isset($this->content)){
			throw new Exception('Cannot call removeTagNodes before setting content.');
		}
	  foreach($tags as $tag){
	  	$this->remove('.//'.$tag);
	  }
	}

	/**
	 * Remove nodes by ID (exact match)
	 * @param  array of strings $ids ids to remove
	 * @param  Boolean $exact whether to match exact class name or not (default false)
	 */
	public function removeIdNodes(array $ids, $exact = true){
		if (!isset($this->content)){
			throw new Exception('Cannot call removeIdNodes before setting content.');
		}
	  foreach($ids as $id){
	  	$exact ? $this->remove('.//*[@id="'.$id.'"]') : $this->remove('.//*[contains(@id, "'.$id.'")]');
	  }
	}

	/**
	 * Remove nodes by class
	 * @param  array of strings $classes classes to remove
	 * @param  Boolean $exact whether to match exact class name or not (default false)
	 * @return DomNode
	 */
	public function removeClassNodes(array $classes, $exact = false){
		if (!isset($this->content)){
			throw new Exception('Cannot call removeClassNodes before setting content.');
		}
  	    foreach($classes as $c){
	  	 	$exact ? $this->remove('.//*[@class="'.$c.'"]') : $this->remove('.//*[contains(@class, "'.$c.'")]');
	 	}
	}

	/**
	 * Remove classes (keeps nodes)
	 * @param  array of strings $classes classes to remove
	 * @param  Boolean $exact whether to match exact class name or not (default false)
	 */
	public function removeClasses(array $classes){
		if (!isset($this->content)){
			throw new Exception('Cannot call removeClasses before setting content.');
		}

	  foreach($classes as $class){
	    foreach($this->queryEach('.//*[contains(@class, "'.$class.'")]', $this->content) as $remove){
	      $nodeClasses = explode(" ", trim($remove->getAttribute('class')));
	      if (($i = array_search($class, $nodeClasses)) !== false) unset($nodeClasses[$i]);
	      $new = implode(" ", $nodeClasses);
	      count($nodeClasses) > 0 ? $remove->setAttribute('class', $new) : $remove->removeAttribute('class');
	    }
	    if ($this->content->hasAttribute('class')){ //check top level node
	      $nodeClasses = explode(" ", trim($this->content->getAttribute('class')));
	      if (($i = array_search($class, $nodeClasses)) !== false) unset($nodeClasses[$i]);
	      $new = implode(" ", $nodeClasses);
	      count($nodeClasses) > 0 ? $this->content->setAttribute('class', $new) : $this->content->removeAttribute('class');
	    }
	  }
	}

	/**
	 * Remove All Classes from $node or $content and children
	 * @param  DomNode $node
	 */
	public function removeAllClasses($node = null){
		if ($node === null && !isset($this->content)){
			throw new Exception('Nothing to removeAllClasses on.');
		}
		if ($node === null){
			$node = $this->content;
		}
		if ($node->hasAttribute('class')){
			$node->removeAttribute('class');
		}
		foreach($this->queryEach('.//*[@class]', $node) as $classNode){
			$classNode->removeAttribute('class');
		}
	}

	/**
	 * Remove ids from node (keeps nodes)
	 * @param  array of strings $ids ids to remove
	 * @param  Boolean $exact whether to match exact id or not (default true)
	 */
	public function removeIds(array $ids, $exact = true){
		if (!isset($this->content)){
			throw new Exception('Cannot call removeIdNodes before setting content.');
		}

	  foreach($ids as $id){
	  	$query = $exact ? './/*[@id="'.$id.'"]' : './/*[contains(@id, "'.$id.'")]';
	    foreach($this->queryEach($query, $this->content) as $r){
	      $r->removeAttribute('id');
	    }
	  }
	}

	/**
	 * Remove attributes from node (keeps nodes)
	 * @param  DomNode $node
	 * @param  array of strings $atts attributes to remove
	 */
	public function removeAttributes(array $atts){
	  foreach($atts as $att){
	    if ($this->content->hasAttribute($att)){
	      $this->content->removeAttribute($att);
	    }
	    foreach($this->queryEach('.//*[@'.$att.']', $this->content) as $r){
	      $r->removeAttribute($att);
	    }
	  }
	}

	/**
	 * Remove ALL classes and IDS from node and subnodes
	 * @param  DomNode $node
	 * @return DomNode
	 */
	public static function removeAllClassIdFromNode($node){
		if (empty($node)){
			throw new Exception('Invalid argument supplied to removeAllClassIdFromNode: '.print_r($node, true));
		}
	  $xpath = new DomXPath($node->ownerDocument);
	  foreach($xpath->query('.//*[@class]', $node) as $n){
	    $n->removeAttribute('class');
	  }
	  foreach($xpath->query('.//*[@id]', $node) as $n){
	    $n->removeAttribute('id');
	  }
	  if ($node->hasAttribute('id')){
	    $node->removeAttribute('id');
	  }
	  if ($node->hasAttribute('class')){
	    $node->removeAttribute('class');
	  }
	  return $node;
	}

	/**
	 * Change a relative url to an absolute url
	 * @param  string $base url for page
	 * @param  string $href relative url
	 * @return string       absolute url
	 */
	public static function resolve_href($base, $href) {
	    // href="" ==> current url.
	    if (!$href) return $base;

	    //I suppose my relatives are ok
	    if (stripos($href, "img/") === 0) return $href;

	    //if "//imageurl/etc"
	    if (stripos($href, "//") === 0) return "http:".$href;

	    // href="http://..." ==> href isn't relative
	    $rel_parsed = parse_url($href);

	    if (array_key_exists('scheme', $rel_parsed)) {
	      return $href;
	    }

	    // add an extra character so that, if it ends in a /, we don't lose the last piece.
	    $base_parsed = parse_url("$base ");
	    // if it's just server.com and no path, then put a / there.
	    if (!array_key_exists('path', $base_parsed)) {
	        $base_parsed = parse_url("$base/ ");
	    }

	    // href="/ ==> throw away current path.
	    if ($href{0} === "/") {
	        $path = $href;
	    } else {
	        $path = dirname($base_parsed['path']) . "/$href";
	    }

	    // bla/./bloo ==> bla/bloo
	    $path = preg_replace('~/\./~', '/', $path);

	    // resolve /../
	    // loop through all the parts, popping whenever there's a .., pushing otherwise.
	        $parts = array();
	        foreach (
	            explode('/', preg_replace('~/+~', '/', $path)) as $part
	        ) if ($part === "..") {
	            array_pop($parts);
	        } elseif ($part!="") {
	            $parts[] = $part;
	        }

	    return (
	      (array_key_exists('scheme', $base_parsed)) ?
	        $base_parsed['scheme'] . '://' . $base_parsed['host'] : ""
	    ) . "/" . implode("/", $parts);

	}

	/**
	 * Find best (biggest or video thumb) <img> in $content
	 * @param  array    $skips array of img strings to skip
	 * @return boolean  if image is found
	 */
	public function scrape(array $skips = null){

		if (!isset($this->content)){
			throw new Exception('Cannot scrape before setting content');
		}

		$urls = array();
		foreach($this->queryEach('.//img', $this->content) as $image){
			if ($image->hasAttribute('data-src')){
				$src = $image->getAttribute('data-src');
			}else if ($image->hasAttribute('data-lazy-src')){
				$src = $image->getAttribute('data-lazy-src');
			}else if ($image->hasAttribute('src')){
	    		$src = $image->getAttribute('src');
			}

			if (isset($src)){
	    		if (stripos($src, "http") !== 0){
	    			$src = $this->resolve_href($this->post->url, $src);
	    		}
	    		if (!in_array($src, $urls)){
	        		array_push($urls, $src);
	      		}
			}
	  	}

		  // foreach($this->queryEach('//meta[@property="og:image"]') as $image){
		  // 	if ($image->hasAttribute('content')){
		  // 		$src = $image->getAttribute('content');
		  // 		if (stripos($src, "http") !== 0) $src = $this->resolve_href($this->post->url, $src);
		  // 		if (!in_array($src, $urls)){
		  // 			array_push($urls, $src);
		  // 		}
		  // 	}
		  // }

	  	// echo "scrape src: ".$src."\n";

	  	$acceptable_area = 50000;
	  	if ($image = self::pickImageFromArray($urls, $acceptable_area, $skips)){
	    	return $this->post->addImage($image);
	  	}

	  	foreach($this->queryEach('.//iframe', $this->content) as $frame){
	    	if ($image = $this->getVideoDataFromEmbed($frame->getAttribute('src'))){
	      		return $this->post->addImage($image);
	    	}
	  	}

	  	return false;
	}

	/**
	 * Remove double spaces from html string
	 * @param  string $text html string
	 * @return string       html string
	 */
	public static function trimspaces($text){
	  $search = array(
	      '/\>[^\S ]+/s', //strip whitespaces after tags, except space
	      '/[^\S ]+\</s', //strip whitespaces before tags, except space
	      '/(\s)+/s'  // shorten multiple whitespace sequences
	      );
	  $replace = array( '>', '<', '\\1' );

	  $blocks = preg_split('/(<\/?pre[^>]*>)/', $text, null, PREG_SPLIT_DELIM_CAPTURE); //keep <pre> spaces
	  $text = '';
	  foreach($blocks as $i => $block){
	    if ($i % 4 == 2) $text .= $block; //break out <pre>...</pre> with \n's
	    else $text .= preg_replace($search, $replace, $block);
	  }
	  return $text;
	}

	public static function getYoutubeId($url){
		if (preg_match('/desktop_uri=%2Fwatch%3Fv%3D([^&]+)&amp;/', $url, $matches)){
			return $matches[1];
		}

		if (stripos($url, "youtube.com/attribution_link?") !== false){
			$parsed = parse_url($url);
			$query = explode("&amp;", $parsed['query']);
			foreach($query as $q){
				if (stripos($q, "u=") === 0){
					$url = $q;
				}
			}
			$url = urldecode($url);
			$url = str_replace('u=', 'http://www.youtube.com', $url);
		}

		//whatever, easier than fixing regexp
		$url = preg_replace('/desktop_uri=%2Fwatch%3F/', '', $url);
		$url = preg_replace('/feature=[^&]+&amp;/', '', $url);
		$url = str_replace("_popup", "", $url);
		$url = str_replace("f&amp;", "", $url);
		$url = str_replace("m.youtube", "youtube", $url);
		$url = str_replace("?rel=0", "", $url);

		$pattern = '#^(?:https?:)?(//)?';    # Optional URL scheme. Either http or https.
		$pattern .= '(?:www\.)?';         #  Optional www subdomain.
		$pattern .= '(?:';                #  Group host alternatives:
		$pattern .=   'youtu\.be/';       #    Either youtu.be,
		$pattern .=   '|youtube\.com';    #    or youtube.com
		$pattern .=   '(?:';              #    Group path alternatives:
		$pattern .=     '/embed/';        #      Either /embed/,
		$pattern .=     '|/v/';           #      or /v/,
		$pattern .=     '|/watch\?v=';    #      or /watch?v=,
		$pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
		$pattern .=   ')';                #    End path alternatives.
		$pattern .= ')';                  #  End host alternatives.
		$pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
		$pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.
		preg_match($pattern, $url, $matches);

		if (isset($matches[2])){
			return $matches[2];
		}
		preg_match('#youtube.com/embed/([a-zA-Z0-9_-]+)#', $url, $mathches);
		if (isset($matches[1])){
			return $matches[1];
		}
	}

	public static function getYoutubeThumbnail($id){
		return array(
			'url' => 'http://img.youtube.com/vi/'.$id.'/0.jpg',
			'video' => 'youtube:'.$id
		);
	}

	public static function getVimeoId($url){
		if (stripos($url, "//") === 0){ //sometimes starts with //
			$url = "http:".$url;
		}
   		if ((($url = parse_url($url)) !== false) && (preg_match('~vimeo[.]com$~', $url['host']) > 0)){
      		$url = array_filter(explode('/', $url['path']), 'strlen');
      		if ($url[1] == 'video' && isset($url[2])){
      			return $url[2];
      		}
      		if (in_array($url[0], array('album', 'channels', 'groups')) !== true){
          		array_unshift($url, 'users');
      		}
      		return $url[1];
    	}
    	return false;
	}

	public static function getVimeoThumbnail($id){
	  $url = "http://vimeo.com/api/v2/video/".$id.".php";
	  $response = file_get_contents($url);
	  if (!$response){
	  	return false;
	  }
	  $vimeo = unserialize($response);
	  $thumb_url = $vimeo[0]['thumbnail_medium'];
	  return array(
	  	'url' => $thumb_url,
	  	'video' => 'vimeo:'.$id
	  );
	}

	public static function getPie($url){

		@$file = trim(file_get_contents($url));
		if (!$file){
			echo "Failed to fetch feed: ".$url."\n";
			return null;
		}
	    $ret = "";
	    $current;
	    $length = strlen($file);
	    for ($i=0; $i < $length; $i++){
	        $current = ord($file{$i});
	        if (($current == 0x9) ||
	            ($current == 0xA) ||
	            ($current == 0xD) ||
	            (($current >= 0x20) && ($current <= 0xD7FF)) ||
	            (($current >= 0xE000) && ($current <= 0xFFFD)) ||
	            (($current >= 0x10000) && ($current <= 0x10FFFF))){
	            $ret .= chr($current);
	        }else{
	            $ret .= " ";
	        }
	    }
		// echo $ret;
		// die();

		$pie = new SimplePie();
		$pie->enable_cache(true);
		$pie->force_feed(true);

		// $pie->set_feed_url($url);
		$pie->set_raw_data($ret);
		$pie->init();

		// if ($pie->error()){
		// 	echo("Pie error: ".$pie->error(). "\n");
		// }

		// stop simplepie from stripping iframes
		$strip_htmltags = $pie->strip_htmltags;
		array_splice($strip_htmltags, array_search('iframe', $strip_htmltags), 1);
		$pie->strip_htmltags($strip_htmltags);

		return $pie;
	}


	/** test if url is a valid image. taken from http://stackoverflow.com/questions/676949/best-way-to-determine-if-a-url-is-an-image-in-php */
	function isImage($url)
	{
		$params = array('http' => array(
			'method' => 'HEAD'
		));
		$ctx = stream_context_create($params);
		$fp = @fopen($url, 'rb', false, $ctx);
		if (!$fp)
			return false;  // Problem with url

		$meta = stream_get_meta_data($fp);
		if ($meta === false)
		{
			fclose($fp);
			return false;  // Problem reading data from url
		}

		$wrapper_data = $meta["wrapper_data"];
		if(is_array($wrapper_data)){
			foreach(array_keys($wrapper_data) as $hh){
				if (substr($wrapper_data[$hh], 0, 19) == "Content-Type: image") // strlen("Content-Type: image") == 19
				{
					fclose($fp);
					return true;
				}
			}
		}

		fclose($fp);
		return false;
	}
}