<?php

// use App\Models\User;
use App\Models\Feed;

class UserController extends BaseController {

	protected $layout = 'layouts.application';

	public function getRegister() {
		return Auth::check()
			?	Redirect::route('home')
			:	View::make('layouts.bones')->nest('content', 'users.register');
	}

	public function postRegister() {
		$validator = Validator::make(Input::all(), User::$rules);
		if ($validator->passes()){
			try{
			    $user = User::create(Input::all());
			    // $user->username = Input::get('username');
			    // $user->email = Input::get('email');
			    // $user->password = Hash::make(Input::get('password'));
			    // $user->save();
			    if ($user){
			    	Auth::login($user);
			    	return Redirect::to('/');
			    }
			}catch(Exception $e){
				return View::make('layouts.bones')->nest('content', 'users.register', array('messages' => true, 'message' => $e->getMessage()));
			}

			return Redirect::to('/');

		    // return $this->postLogin();

		}else{
			Log::info('Validator failed, messages: '.$validator->messages());
			$messages = array();
			$validatorMessages = json_decode($validator->messages());
			foreach($validatorMessages as $message){
				array_push($messages, array('message' => $message[0]));
			}
			Log::info(print_r($messages, true));
			return View::make('layouts.bones')->nest('content', 'users.register', array('messages' => $messages));
		}
	}

	public function getLogin() {
		return Auth::check()
			?	Redirect::route('home')
			: 	View::make('layouts.bones')->nest('content', 'users.login');
	}

	public function postLogin() {
	    $credentials = array(
	      'username' => Input::get('username'),
	      'password' => Input::get('password')
	    );

	    if (Input::get('format') == 'json'){
			if (Auth::attempt($credentials)){
			    return Response::json($user);
			}else{
				return Response::json(array('status' => 'error', 'message' => 'Invalid username or password.'));
			}
/*	    	try{
	    		$user = Sentry::authenticateAndRemember($credentials, false);
	    		return Response::json($user);
	    	}catch(Exception $e){
	    		return Response::json(array('status' => 'error', 'message' => 'Invalid username or password.'));
	    	}*/
	    }

		if (Auth::attempt($credentials)){
	    	return Redirect::route('home');
		}else{
		 	return View::make('layouts.bones')
		 		->nest('content', 'users.login', array('messages' => true, 'message' => 'Invalid username or password.'));
		}

/*	    try{
	    	$user = Sentry::authenticateAndRemember($credentials, false);
	    	return Redirect::route('home');
	    }catch(Exception $e){
		 	return View::make('layouts.bones')
		 		->nest('content', 'users.login', array('messages' => true, 'message' => 'Invalid username or password.'));
	    }*/
	}

	public function getLogout() {
		Auth::logout();
		if (Input::get('format') != 'json'){
			return Redirect::route('login');
		}
	}

	public function getUser() {
		$user = Auth::user();
		return Response::json($user);
	}

	public function getRecent() {
		if (!Auth::check()){
			return Response::json([]);
		}
		$user = Auth::user();
		$feeds = array();
		$feed_ids = array();
		$kloj = $user->kloj->load('feeds');
		$posts = array();
		$feed_klojs = array();

		//get all feeds in user klojs
		foreach($kloj as $k){
			foreach($k->feeds as $feed){
				if (!in_array($feed->id, $feed_ids)){
					array_push($feeds, $feed);
					array_push($feed_ids, $feed->id);
				}

				if (!isset($feed_klojs[$feed->id])){
					$feed_klojs[$feed->id] = array();
				}
				array_push($feed_klojs[$feed->id], $k->toArray());
			}
		}

		//get all posts in those feeds
		foreach($feeds as $feed){
			$temp = $feed->toArray();
			$feed->load('posts');
			if ($feed->posts->count() > 0){
				$newest = $feed->posts->sortByDesc(function($post){
					return $post->date;
				})->shift();
				$newest->source = $temp;
				$newest->klojs = $feed_klojs[$feed->id];
				array_push($posts, $newest->load('images')->toArray());
			}
		}

		return Response::json($posts);
	}
}
