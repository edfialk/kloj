<?php
namespace V1;

use BaseController;
use ThemeRepositoryInterface;
use Input;
use View;

class ThemeController extends BaseController {

    /**
     * We will use Laravel's dependency injection to auto-magically
     * "inject" our repository instance into our controller
     */
    public function __construct(ThemeRepositoryInterface $themes)
    {
        $this->themes = $themes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->themes->findAll();
    }
    /**
     * Return themes where isDefault = true
     * Route: /api/theme/defaults
     * @return Response
     */
    public function getDefaults(){
        return $this->themes->findDefaults();
    }

    /**
     * Store a newly created theme in storage.
     *
     * @return Response
     */
    public function store()
    {
        return $this->themes->store( Input::all() );
    }

    /**
     * Display the specified theme.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $theme = $this->themes->findById($id);
        $theme->template = $theme->template;
        $theme->options = $theme->loadOptions();
        return $theme;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        return $this->themes->update($id, Input::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->themes->destroy($id);
        return '';
    }

}