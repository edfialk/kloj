<?php
namespace V1;

use BaseController;
use KlojRepositoryInterface;
use Input;
use View;
use Response;
use Auth;

use \App\Models\Kloj;
use \App\Models\Feed;
use \App\Models\Theme;
use \App\Models\Template;

class KlojController extends BaseController {

	/**
	 * We will use Laravel's dependency injection to auto-magically
	 * "inject" our repository instance into our controller
	 */
	public function __construct(KlojRepositoryInterface $klojs)
	{
		$this->klojs = $klojs;
	}

	/**
	 * Display a listing of all klojs
	 * route: GET /kloj
	 * @return Response
	 */
	public function index()
	{
		return $this->klojs->findAllWithRelations();
	}

	/**
	 * Display the specified kloj.
	 * route: GET /kloj/{id}
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$kloj = $this->klojs->findByIdWithRelations($id);
		if (!$kloj){
			return Response::json(array('error' => 'Kloj not found'), 500);
		}
		$kloj->theme->template = $kloj->theme->template; //I load(theme.template) in repository but for some reason it's not loading

		return $kloj;
	}

	/**
	 * Store a _new_ kloj in database
	 * route: POST /kloj
	 * @return Response
	 */
	public function store()
	{
		$kloj = new Kloj;
		$resp = array();

		if (Auth::check()){
			$user = Auth::user();
			$kloj->user_id = $user->id;
		}

		if (!Input::get('theme') && !Input::get('theme_id')){
			$resp['status'] = 'error';
			$resp['message'] = 'You must provide a theme.';
			return $resp;
		}

		if (Input::get('theme')){
			$theme = new Theme(Input::get('theme'));
			$template = Template::find(Input::get('theme.template.id'));
			if (!$template){
				$resp['status'] = 'error';
				$resp['message'] = 'Invalid template.';
				return $resp;
			}
			if (isset($user)){
				$theme->user_id = $user->id;
			}
			$theme->template_id = $template->id;
			$theme->save();
		}else if (Input::get('theme_id')){
			$theme = Theme::find(Input::get('theme_id'));
		}

		$kloj->theme_id = $theme->id;

		$kloj->title = Input::get('title', null);
		$kloj->description = Input::get('description', null);
		$kloj->url = Input::get('url', null);
		$kloj->save();

		$kloj->feeds()->attach(array_fetch(Input::get('feeds', []), 'id'));


		$kloj->theme = $kloj->theme;
		$kloj->theme->template = $kloj->theme->template;
		$kloj->feeds = $kloj->feeds;

		return $kloj;
	}

	/**
	 * Update the specified kloj.
	 * route: PUT/PATCH /kloj/{id}
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$kloj = $this->klojs->findById($id);

		//update theme
		if (Input::get('theme.id')){
			$theme = Theme::find(Input::get('theme.id'));
			if (Input::get('theme.options')){
				$theme->setOptions(Input::get('theme.options'));
			}
			if (Input::get('theme.template.id')){
				$template = Template::find(Input::get('theme.template.id'));
				if ($template){
					$theme->template_id = $template->id;
				}else{
					throw Exception("Saving Kloj with invalid theme template id.");
				}
			}
			$theme->save();
		}
		//yeah ill get back to that

		//update feeds
		$kloj->feeds()->sync(array_fetch(Input::get('feeds', []), 'id'));
		$kloj->title = Input::get('title', null);
		$kloj->isFeatured = Input::get('isFeatured', 0);
		$kloj->isPublic = Input::get('isPublic', 0);
		$kloj->url = Input::get('url', null);
		$kloj->save();

		return $kloj;
	}

	/**
	 * Show posts for this kloj
	 * route: GET /kloj/{id}/posts
	 * @return Response
	 */
	public function getPosts($id)
	{
		$length = intval(Input::get('length', '20'));

		$kloj = $this->klojs->findById($id);
		if (!$kloj){
			return Response::json(array('error' => 'Kloj not found'), 500);
		}

		return $kloj->getPosts($length, Input::get('after', null));

		// $feeds = $kloj->feeds;
		// $posts = array();

		// //get posts from each feed
		// foreach($feeds as $feed){
		// 	$feed_posts = $feed->posts->load('images')->toArray();

		// 	//each post need a reference to source object without source's post property. probably should be feed id rather than whole source
		// 	foreach($feed_posts as &$post){
		// 		$f = $feed;
		// 		unset($f->posts);
		// 		$post['source'] = $f->toArray();
		// 	}

		// 	$posts = array_merge($posts, $feed_posts);
		// }

		// usort($posts, function($a, $b){
		// 	return $a['date'] < $b['date'];
		// });

		// if (Input::has('after')){
		// 	$after = Input::get('after');
		// 	for($i = 0; $i < count($posts); $i++){
		// 		if ($posts[$i]['id'] == $after){
		// 			return array_slice($posts, $i+1, $length);
		// 		}
		// 	}
		// }

		// return array_slice($posts, 0, $length);
	}

	/**
	 * Remove the specified kloj.
	 * route: DELETE /kloj/{id}
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$resp = array();
		if (!Auth::check()){
			$resp['status'] = 'error';
			$resp['message'] = "You don't have permission to do that.";
			return $resp;
		}
		$kloj = $this->klojs->findById($id);
		$user = Auth::user();
		if ($user->id != $kloj->user_id && $user->id != 3){ //admin, yes I need to do permissions
			$resp['status'] = 'error';
			$resp['message'] = "You don't have permission to do that.";
			return $resp;
		}

		try{
			foreach($kloj->feeds as $feed){
				$kloj->feeds()->detach($feed->id);
			}
			$kloj->delete();
			$resp['status'] = 'success';
		}catch(Exception $e){
			$resp['status'] = 'error';
			$resp['message'] = $e->getMessage();
		}
		return $resp;
	}

	/**
	 * Bootstrap kloj model for backbone
	 * route: GET /k/{id} - not part of api
	 * @param  int $id
	 * @return Response
	 */
	public function getKlojView($id){
		$view = View::make('layouts.application');

		$kloj = $this->klojs->findByIdWithRelations($id);
		$kloj->posts = $kloj->getPosts();

		if ($kloj){
			$json = $kloj->toJson();
			$view->nest('head', 'kloj.klojmodel', array('json' => $json));
		}

		return $view;
	}

}