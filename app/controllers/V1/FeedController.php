<?php
namespace V1;

use BaseController;
use FeedRepositoryInterface;
use Input;
use View;
use App\Models\Feed;
use App\Models\Category;

class FeedController extends BaseController {

    /**
     * We will use Laravel's dependency injection to auto-magically
     * "inject" our repository instance into our controller
     */
    public function __construct(FeedRepositoryInterface $feeds)
    {
        $this->feeds = $feeds;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->feeds->findAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $feed = new Feed;
        $feed->url = Input::get('url');
        $feed->title = Input::get('title');
        $feed->description = Input::get('description');
        $feed->link = Input::get('link');

        if (Input::get('category')){
            $category = Category::find(Input::get('category.id'));
            if (!$category){
                $feed->category_id = 6; //'other';
            }else{
                $feed->category_id = $category->id;
            }
        }

        //if only google feed api got the channel image
        $feed->fetchImage();

        if (isset($feed->error)){
            return ['status' => 'error', 'message' => $feed->error];
        }

        $feed->save();
        return $feed;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $feed = $this->feeds->findById($id);
        $feed->category = $feed->category;
        // $feed->posts = $feed->posts;
        unset($feed->category_id);
        return $feed;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $feed = $this->feeds->findById($id);
        return View::make('feed._form', compact('feed'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        return $this->feeds->update($id, Input::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->feeds->destroy($id);
        return '';
    }

}