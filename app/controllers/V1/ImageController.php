<?php
namespace V1;

use BaseController;
use ImageRepositoryInterface;
use Input;
use View;
use \App\Models\Image;

class ImageController extends BaseController {

    /**
     * We will use Laravel's dependency injection to auto-magically
     * "inject" our repository instance into our controller
     */
    public function __construct(ImageRepositoryInterface $images)
    {
        $this->images = $images;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->images->findAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $image = $this->images->instance();
        return View::make('image._form', compact('image'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        return $this->images->store( Input::all() );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->images->findById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $image = $this->images->findById($id);
        return View::make('image._form', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        return $this->images->update($id, Input::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->images->destroy($id);
        return '';
    }

}