<?php

use App\Models\Template;

class EloquentTemplateRepository implements TemplateRepositoryInterface {

  public function findById($id)
  {
    $template = Template::where('id', $id)->first();

    if(!$template) throw new NotFoundException('Template Not Found');
    return $template;
  }

  public function findAll()
  {
    return Template::orderBy('created_at', 'desc')->get();
  }

  public function paginate($limit = null)
  {
    return Template::paginate($limit);
  }

  public function store($data)
  {
    $this->validate($data);
    return Template::create($data);
  }

  public function update($id, $data)
  {
    $theme = $this->findById($id);
    $theme->fill($data);
    $this->validate($theme->toArray());
    $theme->save();
    return $theme;
  }

  public function destroy($id)
  {
    $theme = $this->findById($id);
    $theme->delete();
    return true;
  }

  public function validate($data)
  {
    $validator = Validator::make($data, Template::$rules);
    if($validator->fails()) throw new ValidationException($validator);
    return true;
  }

  public function instance($data = array())
  {
    return new Template($data);
  }

}