<?php

use App\Models\Theme;

class EloquentThemeRepository implements ThemeRepositoryInterface {

  public function findById($id)
  {
    $theme = Theme::where('id', $id)->first();

    if(!$theme) throw new NotFoundException('Theme Not Found');
    return $theme;
  }
  public function findAll()
  {
    return Theme::orderBy('created_at', 'desc')->get();
  }

  public function paginate($limit = null)
  {
    return Theme::paginate($limit);
  }

  public function store($data)
  {
    $this->validate($data);
    return Theme::create($data);
  }

  public function update($id, $data)
  {
    $theme = $this->findById($id);
    $theme->fill($data);
    $this->validate($theme->toArray());
    $theme->save();
    return $theme;
  }

  public function destroy($id)
  {
    $theme = $this->findById($id);
    $theme->delete();
    return true;
  }

  public function validate($data)
  {
    $validator = Validator::make($data, Theme::$rules);
    if($validator->fails()) throw new ValidationException($validator);
    return true;
  }

  public function instance($data = array())
  {
    return new Theme($data);
  }

  public function findDefaults()
  {
    return Theme::where('isDefault', '1')->get()->load('template');
  }

}