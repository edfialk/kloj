<?php

use App\Models\Kloj;

class EloquentKlojRepository implements KlojRepositoryInterface {

  public function findById($id)
  {
    $kloj = Kloj::where('id', $id)->first();
    if(!$kloj){
      return false;
    }

    return $kloj;
  }

  public function findByIdWithRelations($id)
  {
    $kloj = Kloj::where('id', $id)->first();

    if (!$kloj){
      return false;
    }

    $kloj->load('feeds')->load('feeds.category')->load('theme')->load('theme.template');
    $kloj->theme->loadOptions();

    if ($kloj->user && $kloj->user->username){
      $kloj->author = $kloj->user->username;
    }

    return $kloj;
  }

  public function findAll()
  {
    return Kloj::orderBy('created_at', 'desc')->get();
  }

  public function findAllWithRelations()
  {
    $klojs = Kloj::orderBy('created_at', 'desc')->get()->load('feeds')->load('theme')->load('feeds.category')->load('theme.template');
    $klojs->each(function($kloj){
      $kloj->theme->loadOptions();
    });
    return $klojs;
  }

  public function paginate($limit = null)
  {
    return Kloj::paginate($limit);
  }

  public function store($data)
  {
    $this->validate($data);
    return Kloj::create($data);
  }

  public function update($id, $data)
  {
    $kloj = $this->findById($id);
    $kloj->fill($data);
    $this->validate($kloj->toArray());
    $kloj->save();
    return $kloj;
  }

  public function destroy($id)
  {
    $kloj = $this->findById($id);
    $kloj->delete();
    return true;
  }

  public function validate($data)
  {
    $validator = Validator::make($data, Kloj::$rules);
    if($validator->fails()) throw new ValidationException($validator);
    return true;
  }

  public function instance($data = array())
  {
    return new Kloj($data);
  }

}