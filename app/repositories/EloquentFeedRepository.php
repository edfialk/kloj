<?php

use App\Models\Feed;

class EloquentFeedRepository implements FeedRepositoryInterface {

  public function findById($id)
  {
    $feed = Feed::where('id', $id)->first();

    if(!$feed) throw new NotFoundException('Kloj Not Found');
    return $feed;
  }

  public function findAll()
  {
    return Feed::orderBy('created_at', 'desc')->get()->load('category');
  }

  public function paginate($limit = null)
  {
    return Feed::paginate($limit);
  }

  public function store($data)
  {
    $this->validate($data);
    return Feed::create($data);
  }

  public function update($id, $data)
  {
    $feed = $this->findById($id);
    $feed->fill($data);
    $this->validate($feed->toArray());
    $feed->save();
    return $feed;
  }

  public function destroy($id)
  {
    $feed = $this->findById($id);
    $feed->delete();
    return true;
  }

  public function validate($data)
  {
    $validator = Validator::make($data, Feed::$rules);
    if($validator->fails()) throw new ValidationException($validator);
    return true;
  }

  public function instance($data = array())
  {
    return new Feed($data);
  }

}