<?php

use App\Models\Image;

class EloquentImageRepository implements ImageRepositoryInterface {

  public function findById($id)
  {
    $image = Image::where('id', $id)->first();

    if(!$image) throw new NotFoundException('Image Not Found');
    return $image;
  }

  public function findAll()
  {
    return Image::orderBy('created_at', 'desc')->get();
  }

  public function paginate($limit = null)
  {
    return Image::paginate($limit);
  }

  public function store($data)
  {
    $this->validate($data);
    return Image::create($data);
  }

  public function update($id, $data)
  {
    $image = $this->findById($id);
    $image->fill($data);
    $this->validate($image->toArray());
    $image->save();
    return $image;
  }

  public function destroy($id)
  {
    $image = $this->findById($id);
    $image->delete();
    return true;
  }

  public function validate($data)
  {
    $validator = Validator::make($data, Image::$rules);
    if($validator->fails()) throw new ValidationException($validator);
    return true;
  }

  public function instance($data = array())
  {
    return new Image($data);
  }

}