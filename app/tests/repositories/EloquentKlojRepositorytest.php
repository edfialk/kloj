<?php

class EloquentKlojRepositoryTest extends TestCase {

  public function setUp()
  {
    parent::setUp();
    $this->repo = App::make('EloquentKlojRepository');
  }

  public function testFindByIdReturnsModel()
  {
    $kloj = $this->repo->findById(1);
    $this->assertTrue($kloj instanceof Illuminate\Database\Eloquent\Model);
  }

  public function testFindAllReturnsCollection()
  {
    $kloj = $this->repo->findAll();
    $this->assertTrue($kloj instanceof Illuminate\Database\Eloquent\Collection);
  }

  public function testValidatePasses()
  {
    $reply = $this->repo->validate(array(
      'title'    => 'This Should Pass',
      'theme_id' => 1,
      'user_id' => 1
    ));

    $this->assertTrue($reply);
  }

  public function testValidateFailsWithoutTitle()
  {
    try {
      $reply = $this->repo->validate(array(
        'theme_id' => 1,
      ));
    }
    catch(ValidationException $expected)
    {
      return;
    }

    $this->fail('ValidationException was not raised');
  }

  public function testValidateFailsWithoutThemeID()
  {
    try {
      $reply = $this->repo->validate(array(
        'title'    => 'This Should Pass'
      ));
    }
    catch(ValidationException $expected)
    {
      return;
    }

    $this->fail('ValidationException was not raised');
  }

/*  public function testStoreReturnsModel()
  {
    $post_data = array(
      'title'    => 'This Should Pass',
      'content'   => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.',
      'author_name' => 'Testy McTesterson'
    );

    $post = $this->repo->store($post_data);

    $this->assertTrue($post instanceof Illuminate\Database\Eloquent\Model);
    $this->assertTrue($post->title === $post_data['title']);
    $this->assertTrue($post->content === $post_data['content']);
    $this->assertTrue($post->author_name === $post_data['author_name']);
  }*/

  public function testUpdateSaves()
  {
    $kloj_data = array(
      'title' => 'The Title Has Been Updated'
    );

    $kloj = $this->repo->update(1, $kloj_data);

    $this->assertTrue($kloj instanceof Illuminate\Database\Eloquent\Model);
    $this->assertTrue($kloj->title === $kloj_data['title']);
  }

  public function testDestroySaves()
  {
    $reply = $this->repo->destroy(1);
    $this->assertTrue($reply);

    try {
      $this->repo->findById(1);
    }
    catch(NotFoundException $expected)
    {
      return;
    }

    $this->fail('NotFoundException was not raised');
  }

  public function testInstanceReturnsModel()
  {
    $kloj = $this->repo->instance();
    $this->assertTrue($kloj instanceof Illuminate\Database\Eloquent\Model);
  }

  public function testInstanceReturnsModelWithData()
  {
    $kloj_data = array(
      'title' => 'Un-validated title'
    );

    $kloj = $this->repo->instance($kloj_data);
    $this->assertTrue($kloj instanceof Illuminate\Database\Eloquent\Model);
    $this->assertTrue($kloj->title === $post_data['title']);
  }

}