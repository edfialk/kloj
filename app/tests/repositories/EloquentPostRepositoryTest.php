<?php

class EloquentPostRepositoryTest extends TestCase {

  public function setUp()
  {
    parent::setUp();
    $this->repo = App::make('EloquentPostRepository');
  }

  public function testFindByIdReturnsModel()
  {
    $post = $this->repo->findById(1);
    $this->assertTrue($post instanceof Illuminate\Database\Eloquent\Model);
  }

  public function testFindAllReturnsCollection()
  {
    $posts = $this->repo->findAll();
    $this->assertTrue($posts instanceof Illuminate\Database\Eloquent\Collection);
  }

  public function testValidatePasses()
  {
    $reply = $this->repo->validate(array(
      'title' => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.',
      'url' => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.',
      'date' => '2014-03-22'
    ));

    $this->assertTrue($reply);
  }

  public function testValidateFailsWithoutUrl()
  {
    try {
      $reply = $this->repo->validate(array(
        'title'   => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.',
        'date' => '2014-03-22'
      ));
    }
    catch(ValidationException $expected)
    {
      return;
    }

    $this->fail('ValidationException was not raised');
  }

  public function testValidateFailsWithoutDate()
  {
    try {
      $reply = $this->repo->validate(array(
        'title' => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.',
        'url' => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.'
      ));
    }
    catch(ValidationException $expected)
    {
      return;
    }

    $this->fail('ValidationException was not raised');
  }

/*  public function testStoreReturnsModel()
  {
    $comment_data = array(
      'content'   => 'Lorem ipsum Fugiat consectetur laborum Ut consequat aliqua.',
      'author_name' => 'Testy McTesterson'
    );

    $comment = $this->repo->store(1, $comment_data);

    $this->assertTrue($comment instanceof Illuminate\Database\Eloquent\Model);
    $this->assertTrue($comment->content === $comment_data['content']);
    $this->assertTrue($comment->author_name === $comment_data['author_name']);
  }*/

  public function testUpdateSaves()
  {
    $post_data = array(
      'title' => 'The Content Has Been Updated'
    );

    $post = $this->repo->update(1, $post_data);

    $this->assertTrue($post instanceof Illuminate\Database\Eloquent\Model);
    $this->assertTrue($post->title === $post_data['title']);
  }

  public function testDestroySaves()
  {
    $reply = $this->repo->destroy(1);
    $this->assertTrue($reply);

    try {
      $this->repo->findById(1);
    }
    catch(NotFoundException $expected)
    {
      return;
    }

    $this->fail('NotFoundException was not raised');
  }

  public function testInstanceReturnsModel()
  {
    $post = $this->repo->instance();
    $this->assertTrue($post instanceof Illuminate\Database\Eloquent\Model);
  }

  public function testInstanceReturnsModelWithData()
  {
    $post_data = array(
      'title' => 'Un-validated title'
    );

    $post = $this->repo->instance($post_data);
    $this->assertTrue($post instanceof Illuminate\Database\Eloquent\Model);
    $this->assertTrue($post->title === $post_data['title']);
  }

}