<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/


// View::make('view.path')->nest($sectionName, $nestedViewPath, $viewVariables); // for reference

App::bind('PostRepositoryInterface', 'EloquentPostRepository');
App::bind('KlojRepositoryInterface', 'EloquentKlojRepository');
App::bind('FeedRepositoryInterface', 'EloquentFeedRepository');
App::bind('ThemeRepositoryInterface', 'EloquentThemeRepository');
App::bind('ImageRepositoryInterface', 'EloquentImageRepository');
App::bind('TemplateRepositoryInterface', 'EloquentTemplateRepository');
App::bind('CategoryRepositoryInterface', 'EloquentCategoryRepository');

// Routes belonging to API v1
Route::group(array('prefix' => 'v1'), function()
{
  //specific routes
  Route::get('user', array('as' => 'user', 'uses' => 'UserController@getUser'));
  Route::get('kloj/{id}/posts', array('uses' => 'V1\KlojController@getPosts'));
  Route::get('theme/defaults', array('uses' => 'V1\ThemeController@getDefaults'));

  //controller routes
  Route::resource('kloj', 'V1\KlojController');
  Route::resource('post', 'V1\PostController');
  Route::resource('theme', 'V1\ThemeController');
  Route::resource('feed', 'V1\FeedController');
  Route::resource('image', 'V1\ImageController');
  Route::resource('template', 'V1\TemplateController');
  Route::resource('category', 'V1\CategoryController');
});

// server specific
Route::get('login', array('as' => 'login', 'uses' => 'UserController@getLogin'));
Route::post('login', array('as' => 'login.post', 'uses' => 'UserController@postLogin'));
Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@getLogout'));
Route::get('register', array('as' => 'register', 'uses' => 'UserController@getRegister'));
Route::post('register', array('as' => 'register.post', 'uses' => 'UserController@postRegister'));
Route::get('user/recent', array('as' => 'recent', 'uses' => 'UserController@getRecent'));

// bootstrap kloj model for kloj view
Route::get('k/{id}', array('as' => 'KlojView', 'uses' => 'V1\KlojController@getKlojView'));


/** backbone routes **/

Route::get('welcome', array('as' => 'welcome', function(){
  return View::make('layouts.bones')->nest('content', 'welcome');
}));
Route::get('browse', function(){
  return View::make('layouts.application');
});
Route::get('create', function(){
  return View::make('layouts.application');
});

//routes requiring authentication
Route::group(array('before' => 'auth.user'), function(){

  Route::get('/', array('as' => 'home', function(){
    return View::make('layouts.application');
  }));

  Route::get('home', function(){
    return View::make('layouts.application');
  });

  Route::get('profile', function(){
    return View::make('layouts.application');
  });

  Route::get('edit/{id}', function(){
    return View::make('layouts.application');
  });

});

//Backbone app route
// I went with specifying each route
// In this method, Any route not caught by laravel goes to backbone
// we are basically just giving it an optional parameter of "anything"
// this also means there's no server-side 404 page.
/*Route::get('/{path?}', function($path = null)
{
  return View::make('layouts.application');
})->where('path', '.*'); //regex to match anything*/