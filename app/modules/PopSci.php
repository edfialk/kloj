<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class PopSci extends Module {

	public function match($post)
	{
		if (stripos($post->url, "popsci.com")){
			echo "PopSci handling...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

		if ($author = $parser->queryOne('//span[@class="byline_author"]//a')){
			$name = $author->nodeValue;
			$address = Parser::resolve_href($this->post->permalink, $author->getAttribute('href'));
			$this->post->author = '<a href="'.$address.'" rel="author">'.$name.'</a>';
		}

		foreach($parser->queryEach('//img[@data-src]') as $img){
			$src = $img->getAttribute('data-src');
			$src = substr($src, 0, stripos($src, "?itok"));
			$img->setAttribute('src', $src);
			$img->removeAttribute('data-src');
		}

		if ($content = $parser->queryOne('//div[contains(@class, "node-article")]//div[contains(@class, "content")]')){
			$parser->content = $content;
			if ($parser->scrape()){
				$this->post->extract = $this->getExtract();
				$this->post->handled = true;
				return false;
			}
		}

		if (isset($parser->content) && !isset($this->post->images)){
			$this->post->addImage($this->getDefault());
			$this->post->handled = true;
			return false;
		}
		$this->post->handled = false;
		return false;
	}

	public function getExtract(){
		$parser = $this->parser;
		$content = $parser->content;
		$css = '';

		return $parser->getExtract();
	}
	public function getDefault(){
		return array( 'url' => '/img/feedicons/feed.jpg', 'ratio' => 0.978, 'width' => 1.25, 'height' => 360);
	}

}