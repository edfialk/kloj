<?php

namespace App\Modules;

use Parser;
use App\Models\Image;
use Log;

class Flickr extends Module {

	private $key = 'daa6540fd763d743e6d31903bd8147e0';

	public function match($post)
	{
		if (stripos($post->url, "flic.kr") || stripos($post->url, "flickr.com")){
			$this->post = $post;
			return $this->handle();
		}
	}

	public function handle(){
		echo "Flickr...\n";

		$post = $this->post;
		$link = $post->url;
	  if (stripos($link, "/with") !== false){
	  	//sometimes people post flickr links straight to photo viewing...or something
	  	$link = str_replace("/with", "", $link);
	  }

	  if (preg_match('/flic\.kr\/p\/(.*)/', $link, $matches)){
	  	//standard flickr /p/ photo
	    $codedId = $matches[1];
	    $photoId = $this->decode_flickr_id($codedId);
	    // debug("Flickr photo id: ".$photoId);
	    //ex: http://api.flickr.com/services/rest?api_key=daa6540fd763d743e6d31903bd8147e0&method=flickr.photos.getInfo&photo_id=8952583448&format=json
	    $req = "https://api.flickr.com/services/rest?api_key=".$this->key."&method=flickr.photos.getSizes&format=json&nojsoncallback=1&photo_id=";
	    $req .= $photoId;

	    $response = file_get_contents($req);

	    if (!$response){
	    	Log::error('!! FLICKR NO RESPONSE TO REQUEST: '.$req);
	    	$post->error = 'No flickr response';
	    	return;
	    }

	    $json = json_decode($response, true);

	    if (!$json || $json['stat'] != 'ok'){
	      Log::error("!! Error with flickr request: ($req), json: ".print_r($json, true));
	      $post->error = 'Bad flickr request';
	      return;
	    }

	    $image = array();
	    $widest = 0;
	    foreach($json['sizes']['size'] as $size){
	    	if ($size['width'] > $widest){
	    		$image['url'] = $size['source'];
	    		$image['width'] = $size['width'];
	    		$image['height'] = $size['height'];
	    		$image['ratio'] = intval($size['width']) / intval($size['height']);
	    		$widest = $size['width'];
	    	}
	    }
	    if (isset($image['url'])){
	    	$post->addImage($image);
	    	$post->handled = true;
	    	return false;
	    }
	  }

	  if (preg_match('/sets\/([0-9]+)/', $link, $matches)){
	  	//flickr photo sets
	    $setId = $matches[1];

	    $req = "https://api.flickr.com/services/rest?api_key=".$this->key."&method=flickr.photosets.getPhotos&format=json&nojsoncallback=1&photoset_id=";
	    $req .= $setId;

	    $json = json_decode(file_get_contents($req), true);
	    foreach($json['photoset']['photo'] as $photo){
	    	$img = "http://farm".$photo['farm'].".staticflickr.com/".$photo['server']."/".$photo['id']."_".$photo['secret'].".jpg";
	    	$post->addImage($img);
	    }
	    $post->handled = true;
	    return false;
	  }

  	if (preg_match('/photos\/[^\/]+\/([0-9]+)/', $link, $matches)) {
	    $photoId = $matches[1];
	    // debug("flickr photo Id: $photoId");

	    //ex: http://api.flickr.com/services/rest?api_key=daa6540fd763d743e6d31903bd8147e0&method=flickr.photos.getInfo&photo_id=8952583448&format=json
	    $req = "https://api.flickr.com/services/rest?api_key=".$this->key."&method=flickr.photos.getSizes&format=json&nojsoncallback=1&photo_id=";
	    $req .= $photoId;

	    $response = file_get_contents($req);
	    $json = json_decode($response, true);

	    if (!$json || $json['stat'] != 'ok'){
	      Log::error("!! Error with flickr request: ($req), json: ".print_r($json, true));
	      $post->error = 'Bad flickr request';
	      return;
	    }

	    $largest = 0;
	    $pick;
	    foreach($json['sizes']['size'] as $size){
	      $w = $size['width'];
	      if ($w > $largest){
	      	$pick = $size;
	      	$largest = $w;
	      }
	    }
	    if ($largest > 0){
	    	$image = array(
	    		'url' => $pick['source'],
	    		'width' => $pick['width'],
	    		'height' => $pick['height'],
	    		'ratio' => intval($pick['width']) / intval($pick['height'])
	    	);
	    	$post->addImage($image);
	    	$post->handled = true;
	    	return false;
	    }
	  }

	}

	//sometimes flickr compresses their ids
	private function decode_flickr_id($num){
	  $alphabet = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	  $decoded = 0;
	  $multi = 1;
	  while (strlen($num) > 0) {
	    $digit = $num[strlen($num)-1];
	    $decoded += $multi * strpos($alphabet, $digit);
	    $multi = $multi * strlen($alphabet);
	    $num = substr($num, 0, -1);
	  }
	  return $decoded;
	}

}