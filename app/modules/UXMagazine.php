<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class UXMagazine extends Module {

	public function match($post)
	{
		if (stripos($post->url, "uxmag.com")){
			echo "UX Magazine...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

		if ($author = $parser->queryOne('//a[@rel="author"]')){
			$name = $author->nodeValue;
			$address = Parser::resolve_href($this->post->permalink, $author->getAttribute('href'));
			$this->post->author = '<a href="'.$address.'" rel="author">'.$name.'</a>';
		}

		if ($content = $parser->queryOne('//div[@itemscope]/div[contains(@class, "content")]')){
			$parser->content = $content;
			if ($image = $parser->scrape()){
				//remove trash around img
				if ($imgContainer = $parser->queryOne('.//div[contains(@class, "field-name-field-article-node-page-banner")]', $content)){
					$newImgContainer = $parser->doc->createElement('img');
					$newImgContainer->setAttribute('src', $image->url);
					$content->replaceChild($newImgContainer, $imgContainer);
				}

				$this->post->extract = $this->getExtract();
				$this->post->handled = true;
				return false;
			}
		}

		if (isset($parser->content) && !isset($this->post->images)){
			$this->post->addImage($this->getDefault());
			$this->post->handled = true;
			return false;
		}
		$this->post->handled = false;
		return false;
	}

	public function getExtract(){
		$parser = $this->parser;
		$content = $parser->content;
		$css = '';

		// while ($content->hasAttributes()){
		//   $content->removeAttributeNode($content->attributes->item(0));
		// }

		$parser->removeClassNodes(['print-link']);
		$parser->removeAttributes(['width', 'height', 'typeof', 'property']);
		//need to clean up <div><div><div>, TODO

		$parser->removeAllClasses();

		$content->setAttribute('class', 'uxmag');

		return $parser->getExtract();
	}
	public function getDefault(){
		return array( 'url' => '/img/feedicons/UXMagainze', 'ratio' => 1, 'width' => 400, 'height' => 400);
	}

}