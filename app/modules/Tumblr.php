<?php

namespace App\Modules;

use Log;
use Parser;
use App\Models\Image;

class Tumblr extends Module {

	private $key = "B85DucuIhqKzPGmSSeKGadwPPjcPqJLMw9uMhsN4xmunn2nQla";

	public function match($post){
		if (stripos($post->url, ".tumblr.com") !== false){
			echo "TUMBLR handling...\n";
			$this->post = $post;
			// $this->parser = new Parser($post);

			return $this->handle();

		}
	}
	public function handle(){

		$blog = $this->getBlog($url);
		$blogId = $blog[1];
		$blogType = $blog[2];
		$id = $this->getId($url);

		$req = "http://api.tumblr.com/v2/blog/$blogId/posts?id=$id&api_key=".$this->key;
		$json = json_decode(file_get_contents($req));

		if ($json->meta->status != 200){
			Log::error("Tumblr request fail, request: $req, response: ".$json->meta->msg."\n");
			$this->post->error = 'Error with tumblr request';
			return true;
		}

		$jsonPost;
		if (isset($json->response->posts)){
			$jsonPost = $json->response->posts;
		}else{
			Log::error("--NOT PREPARED FOR THIS TUMBLR: ".print_r($json, true));
			$this->post->error = 'Not prepared for this tumblr.';
			return true;
		}

		if (is_array($jsonPost)) $jsonPost = $jsonPost[0];

		if ($jsonPost->type == "audio"){
			if (isset($jsonPost->album_art)){
				$this->post->addImage($jsonPost->album_art);
				$this->post->handled = true;
				return false;
			}
			Log::error('Tumblr audio post has no album art.'.print_r($jsonPost, true));
		}

		if ($jsonPost->type == "video"){
			if (isset($jsonPost->thumbnail_url)){
				$this->post->addImage($jsonPost->thumbnail_url);
				$this->post->handled = true;
				return false;
			}
			Log::error('Tumblr video post with no thumbnail.'.print_r($jsonPost, true));
		}

		if (isset($jsonPost->photos)){
			$photos = $jsonPost->photos;
			if (count($photos) > 0){
				foreach($photos as $photo){
					if (isset($photo->original_size) && isset($photo->original_size->url)){
						$this->post->addImage($photo->original_size->url);
					}else{
						Log::info('tumblr photo does not have original_size->url'.var_dump($photo, true));
					}
				}
				$this->post->handled = true;
				return false;
			}
		}

		if (isset($jsonPost->image_permalink)){
			$this->post->addImage($jsonPost->image_permalink);
			$this->post->handled = true;
			return true;
		}

		$imgs = array();
		if (isset($jsonPost->description)){
			$desc = $jsonPost->description;
		    $doc = new DOMDocument();
		    $doc->loadXML("<root>".$desc."</root>");
		    $xpath = new DomXPath($doc);
		    foreach($xpath->query('//img') as $img){
		    	array_push($imgs, $img->getAttribute('src'));
		    }
		}
		if (isset($jsonPost->body)){
			$text = $jsonPost->body;
		    $doc = new DOMDocument();
		    $doc->loadXML("<root>".$text."</root>");
		    $xpath = new DomXPath($doc);
		    foreach($xpath->query('//img') as $img){
		    	array_push($imgs, $img->getAttribute('src'));
		    }
		}
		if (($img = Parser::pickImageFromArray($imgs)) !== null){
			$this->post->addImage($img);
			$this->post->handled = true;
			return false;
		}

		$this->post->addImage($this->getDefault());
		$this->post->handled = true;
		return false;
	}
	private function getBlog($url){
		if (preg_match('/\:\/\/([^.]+\.tumblr.com)\/(post|image)\/([0-9]+)/', $url, $matches) != 0){
			return $matches;
		}
	}
	private function getId($url){
		if (preg_match('/\:\/\/([^.]+)\.tumblr.com\/(post|image)\/([0-9]+)/', $url, $matches) != 0){
			return $matches[3];
		}
	}
	private function getDefault(){
		return array( 'url' => 'imgs/tumblr.jpg', 'ratio' => 1.047 );
	}
}