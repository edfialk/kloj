<?php

namespace App\Modules;

use App\Models\Image;

class Imgur extends Module {

	private $key = "c3a132642eb95f9";

	public function match($post)
	{
		if (stripos($post->url, 'imgur.com') !== false){
			$this->post = $post;
			$this->handle($post);
			return false; //stop other event listeners
		}

	}

	public function handle(){
		echo "Imgur...\n";
		$post = $this->post;
		$urls = array();
		if (false != ($id = $this->isImgurAlbum($post->url))){
  		$urls = $this->getImgurAlbumImgs($id);
		}else if (false != ($id = $this->isImgurGallery($post->url))){
			$urls = $this->getImgurGalleryImgs($id);
		}else if (false != ($urls = $this->isMultiples($post->url))){
			// debug(print_r($urls));
		}else{
			$urls[] = $post->url;
		}

		for ($i=0; $i < count($urls); $i++) {
			$url = $urls[$i];

			//fix imgur.com/id (no .jpg, .gif, etc.)
			if (!preg_match('#imgur\.com\/[^.]+\.[a-z]{3}#i', $url)){
				$url = $url.".jpg";
			}

			//fix imgur.com/id.jpg?0
			if (strpos($url, "?") !== false){
				$url = substr($url, 0, strpos($url, "?"));
			}

			//direct imgur images use i.imgur.com
			$url = preg_replace("#https?://(www\.)?imgur\.com(.*)#", "http://i.imgur.com$2", $url);
			$urls[$i] = $url;
		}

		foreach($urls as $url){
			$post->addImage($url);
		}
		$post->handled = true;
		return false;
	}

	private function isMultiples($url){	//abc.jpg,def.jpg
		$pattern = '#imgur\.com\/((\w+),((\w+),?)+)#';
		preg_match($pattern, $url, $matches);
		if (count($matches) == 0){
			return false;
		}
		$imgs = array();
		$ids = explode(",", $matches[1]);
		foreach($ids as $id){
			array_push($imgs, "http://i.imgur.com/$id.jpg");
		}
		return $imgs;
	}
	private function isImgurAlbum($url){	//imgur.com/a/asdf
	  $pattern = '/imgur\.com\/a\/(.*+)/i';
	  preg_match($pattern, $url, $matches);
	  return (isset($matches[1])) ? $matches[1] : false;
	}
	private function getImgurAlbumImgs($id){
		$function_start = microtime(true);
	  $url = "https://api.imgur.com/3/album/".$id;
	  $header = array();
	  $header[] = 'Authorization: Client-ID '.$this->key;
	  $c = curl_init($url);
	  curl_setopt($c, CURLOPT_HTTPHEADER, $header);
	  curl_setopt($c, CURLOPT_RETURNTRANSFER,1);
	  $r = curl_exec($c);
	  $data = json_decode($r, true);
	  if ($data['status'] != '200'){
	  	debug("Problem with imgur request: " . $data['error']);
	  	error_log("!! IMGUR PROBLEM: ".$data['error']);
	  	error_log(print_r($data, true));
	  }
	  $imgs = array();
	  foreach($data['data']['images'] as $image){
	  	array_push($imgs, $image['link']);
	  }
	  return $imgs;
	}
	private function isImgurGallery($url){
	  $pattern = '/imgur\.com\/gallery\/(.*+)/i';
	  preg_match($pattern, $url, $matches);
	  return (isset($matches[1])) ? $matches[1] : false;
	}
	private function getImgurGalleryImgs($id){
		$url = "https://api.imgur.com/3/gallery/".$id;
		$c = curl_init($url);
		$header = array();
		$header[] = 'Authorization: Client-ID '.$this->key;
		curl_setopt($c, CURLOPT_HTTPHEADER, $header);
		curl_setopt($c, CURLOPT_RETURNTRANSFER,1);
		$r = curl_exec($c);
		$data = json_decode($r, true);
		if ($data['status'] != '200'){
			echo("Problem with imgur request, status=".$data['status'].", url=".$url."\n");
			if (isset($data['error'])){
				echo "Message: ".$data['error'];
				Log::error("IMGUR PROBLEM: ".$data['error']);
			}else{
				print_r($data);
			}
		}
		$imgs = array();
		if (!isset($data['data']['images'])){
			if (isset($data['data']['link'])){
				array_push($imgs, $data['data']['link']);
				return $imgs;
			}else{
				Log::error("!! IMGUR GALLERY DOESNT HAVE IMAGE FOR POST: ".$post['title']);
			}
		}
		foreach($data['data']['images'] as $img){
			array_push($imgs, $img['link']);
		}
		return $imgs;
	}

}