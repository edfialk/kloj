<?php

namespace App\Modules;

use App\Models\Image;
use Log;

class ImageModule extends Module {

	public function match($post)
	{
	  	$params = array(
	  		'http' => array('method' => 'HEAD')
	  	);
	  	$ctx = stream_context_create($params);
		$fp = @fopen($post->url, 'rb', false, $ctx);
		if (!$fp){ // Problem with url
			Log::error('Image cannot open stream. '.print_r($post, true));
			return;
		}

		$meta = stream_get_meta_data($fp);
		if ($meta === false){ // Problem reading data from url
			fclose($fp);
			Log::error('Image cannot read stream. '.print_r($post, true));
			return;
		}

		$wrapper_data = $meta["wrapper_data"];
		if(is_array($wrapper_data)){
			foreach(array_keys($wrapper_data) as $hh){
		  		if (substr($wrapper_data[$hh], 0, 19) == "Content-Type: image"){ // strlen("Content-Type: image") == 19
		    		fclose($fp);
		    		$post->addImage($post->url);
		    		$post->handled = true;
		    		echo "Image...\n";
		    		return false;
		  		}
			}
		}

		fclose($fp);
	}

	public function handle(){}

}