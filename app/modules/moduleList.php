<?php

//className with match() function
$module_list =
[
	'ImageModule',
	'Imgur',
	'Youtube',
	'Vimeo',
	'Flickr',
	'Tumblr',
	'Engadget',
	'NPR',
	'BBC',
	'Gawker',
	'UXMagazine',
	// 'PopSci',
	'Wired',
	'Polygon'
	// 'Mashable'
];

foreach($module_list as $module)
{
	Event::listen('post.fetch', 'App\\Modules\\'.$module.'@match');
}
