<?php

namespace App\Modules;

use Parser;
use App\Models\Image;
use Log;

class Polygon extends Module {

	public function match($post)
	{
		if (stripos($post->url, "polygon.com")){
			echo "Polygon handling...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

		if ($content = $parser->queryOne('//*[contains(@class,"m-entry__content")]')){
			$parser->content = $content;
		}

		// if ($image = $parser->queryOne('//*[contains(@class, "m-entry__image")]/img')){
		// 	$this->post->addImage($image);
		// }

		$parser->scrape();

		if ($movie = $parser->queryOne('//*[contains(@class, "m-entry__image")]/div[contains(@class, "video-wrap")]')){
			//embed code
			//<script height="413px" width="735px" src="http://player.ooyala.com/iframe.js#pbid=2ff6d6fff2b2457bb9ea2cfcf77dc25b&ec=c3cjlwcjrqTO9v722qX0ZL2TrosL_htW"></script>
			//source
			//window.ooyala_player_id = '2ff6d6fff2b2457bb9ea2cfcf77dc25b';
			//"provider_video_id":"c3cjlwcjrqTO9v722qX0ZL2TrosL_htW"

			$ht = $parser->doc->saveHTML();
			preg_match_all("/window\.ooyala_player_id\s?=\s?['\"](.*)['\"]/", $ht, $matches);
			if (count($matches) > 0 && count($matches[1] > 0)){
				$id = $matches[1][0];
			}
			preg_match_all("/provider_video_id\":\"([^\"]*)\"/", $ht, $matches);
			if (count($matches) > 0 && count($matches[1] > 0)){
				$ec = $matches[1][0];
			}
			if (isset($id) && isset($ec)){
				$embed = $parser->doc->createElement('script');
				$embed->setAttribute('height', '413px');
				$embed->setAttribute('width', '735px');
				$embed->setAttribute('src', 'http://player.ooyala.com/iframe.js#pbid='.$id."&ec=".$ec);
				// $embed->setAttribute('src', 'http://player.ooyala.com/iframe.js#pbid=2ff6d6fff2b2457bb9ea2cfcf77dc25b&ec=c3cjlwcjrqTO9v722qX0ZL2TrosL_htW');
				$embed->setAttribute('data-orig', 'kloj');
				$wrap = $parser->doc->createElement('div');
				$wrap->appendChild($embed);
				$wrap->appendChild($parser->content);
				$parser->content = $wrap;

				if ($image = $parser->queryOne('//meta[@property="og:image"]')){
					$src = $image->getAttribute('content');
					$image = array('url' => $src, 'video' => 'embed');
					$this->post->addImage($image);
				}

			}else{
				Log::error('Cannot find polygon embed video id: '.$this->post->url);
			}
		}

		if (isset($parser->content)){
			$this->getExtract();
			$this->post->handled = true;
			return false;
		}else{
			Log::error('Polygon failed to parse content.');
			return true;
		}
	}

	public function getExtract(){
		$parser = $this->parser;
		$content = $parser->content;
		$css = '';

		// echo $content->ownerDocument->saveHTML($content);

		$parser->removeClassNodes(['m-ad', 'm-follow-bar', 'm-share-buttons', 'm-entry__attribution']);

		$this->post->extract = $parser->getExtract();
		// echo $this->post->extract;
	}
	public function getDefault(){
		return array( 'url' => '/img/feedicons/feed.jpg', 'ratio' => 0.978, 'width' => 1.25, 'height' => 360);
	}

}