<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class Engadget extends Module {

	public function match($post)
	{
		if (stripos($post->url, "engadget.com")){
			echo "Engadget...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}

	}

	public function handle(){
		$parser = $this->parser;

		if ($content = $parser->queryOne('//div[contains(@class, "post-body")]')){
			$parser->content = $content;
			if ($parser->scrape()){
				$this->post->extract = $this->getExtract();
				$this->post->handled = true;
				return false;
			}
			// if ($parser->scrapeNode($content)){
			// 	$post->extract = $this->getExtract($content);
			// 	$post->handled = true;
			// 	return false;
			// }
		}

		$post->addImage($this->getDefault());
	}

	public function getExtract(){
		$parser = $this->parser;
		$node = $parser->content;

		$parser->remove([
			'.//div[@class="post-gallery"]',
			'.//p[@class="read-more"]'
		]);

		return $parser->getExtract();
	}

	public function getDefault(){
		return array('url' => '/img/feedicons/engadget.jpg', 'ratio' => 0.966);
	}

}