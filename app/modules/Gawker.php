<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class Gawker extends Module {

	public function match($post)
	{
		if (
			stripos($post->url, "http://feeds.gawker.com/") !== false ||
 			stripos($post->url, "lifehacker.com") !== false ||
			stripos($post->url, "gizmodo.com") !== false ||
			stripos($post->url, "kotaku.com") !== false ||
			stripos($post->url, "io9.com") !== false
		){
			echo "Gawker...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

    $skips = array('kotaku-touch-icon');

    if ($content = $parser->queryOne('//div[contains(@class,"post-content")]')){
    	$parser->content = $content;
	  	if ($parser->scrape($skips)){
	    	$this->getExtract();
	    	$this->post->handled = true;
	  		return false;
	  	}
	  	//hard to have gawker default, covers 5 sites
	  	$this->getExtract();
	  	$this->post->handled = true;
	  	return false;
    }

    return true;
	}

	//change to gawker!
	public function getExtract(){
		$parser = $this->parser;
		$content = $parser->content;

		$parser->remove(
			array(
				'.//aside[contains(@class, "referenced-wide")]',
				'.//span[@class="magnifier lightBox hide"]'
			)
		);

		$attributes = array(
			'onclick', 'data-textannotation-id', 'data-ids', 'data-chomp-id', 'data-asset-url', 'data-model', 'data-id',
			'data-description', 'data-imagetag', 'data-forpost'
		);
		$parser->removeAttributes($attributes);

		$classes = array('avatar-shared-by-container', 'top-meta', 'webchatsswitch', 'icon notranslate');
		$parser->removeClassNodes($classes);

		$classes = array('twelve', 'mobile', 'columns', 'end', 'content-column', 'js_ad300-companion', 'transform-{$transform}');
		$parser->removeClasses($classes);

		$parser->content->removeAttribute('class');

		$css = '';
		if ($parser->queryOne('.//p[contains(@class, "media-640")]', $content))
			$css .= '.media-640 img{display:block; margin-bottom: 10px;}';

		if ($css != '') {
			$style = $content->ownerDocument->createElement('style', $css);
			$content->appendChild($style);
		}

		$this->post->extract = $parser->getExtract();
	}

}