<?php

namespace App\Modules;

abstract class Module {
	abstract public function match($post);
	abstract public function handle();

}