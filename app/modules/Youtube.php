<?php

namespace App\Modules;

use App\Models\Image;

class Youtube extends Module {

	private $id;

	public function match($post)
	{
		if ($this->getId($post->url) !== false){
			$this->post = $post;
			return $this->handle();
		}
	}

	public function handle(){
		$post = $this->post;

		if (!isset($this->id)){
			$this->getId($post->url);
		}

		$thumbnail = 'http://img.youtube.com/vi/'.$this->id.'/0.jpg';
		$image = array(
			'url' => $thumbnail,
			'video' => 'youtube:'.$this->id
		);
		$post->addImage($image);
		$post->handled = true;
		echo "Youtube...\n";
		return false;
	}

	public function getId($url){
		if (preg_match('/desktop_uri=%2Fwatch%3Fv%3D([^&]+)&amp;/', $url, $matches)){
			$this->id = $matches[1];
			return $this->id;
		}

		if (stripos($url, "youtube.com/attribution_link?") !== false){
			$parsed = parse_url($url);
			$query = explode("&amp;", $parsed['query']);
			foreach($query as $q){
				if (stripos($q, "u=") === 0){
					$url = $q;
				}
			}
			$url = urldecode($url);
			$url = str_replace('u=', 'http://www.youtube.com', $url);
		}

		$url = preg_replace('/desktop_uri=%2Fwatch%3F/', '', $url);
		$url = preg_replace('/feature=[^&]+&amp;/', '', $url);
		$url = str_replace("_popup", "", $url);
		$url = str_replace("f&amp;", "", $url);
		$url = str_replace("m.youtube", "youtube", $url);
		$url = str_replace("?rel=0", "", $url);
		//whatever, easier than fixing regexp

		$pattern = '#^(?:https?:)?(//)?';    # Optional URL scheme. Either http or https.
		$pattern .= '(?:www\.)?';         #  Optional www subdomain.
		$pattern .= '(?:';                #  Group host alternatives:
		$pattern .=   'youtu\.be/';       #    Either youtu.be,
		$pattern .=   '|youtube\.com';    #    or youtube.com
		$pattern .=   '(?:';              #    Group path alternatives:
		$pattern .=     '/embed/';        #      Either /embed/,
		$pattern .=     '|/v/';           #      or /v/,
		$pattern .=     '|/watch\?v=';    #      or /watch?v=,
		$pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
		$pattern .=   ')';                #    End path alternatives.
		$pattern .= ')';                  #  End host alternatives.
		$pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
		$pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.
		preg_match($pattern, $url, $matches);

		if (isset($matches[2])){
			$this->id = $matches[2];
			return $this->id;
		}
		preg_match('#youtube.com/embed/([a-zA-Z0-9_-]+)#', $url, $mathches);
		if (isset($matches[1])){
			$this->id = $matches[1];
			return $this->id;
		}
		return false;
	}

}