<?php

namespace App\Modules;

use Parser;
use App\Models\Image;
use Log;

class BBC extends Module {

	public function match($post)
	{
		if (stripos($post->url, "bbc.co.uk")){
			echo "BBC...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

		$this->post->permalink = str_replace("#sa-ns_mchannel=rss&amp;ns_source=PublicRSS20-sa", "", $this->post->url);

		if ($author = $parser->queryOne('//span[@class="byline-name"]')){
			$this->post->author = str_ireplace("by ", "", $author->nodeValue);
		}

		if ($content = $parser->queryOne('//div[@class="story-body"]')){
			$parser->content = $content;
			$this->post->extract = $this->getExtract();

			if ($video = $parser->queryOne('//meta[@property="og:video"]')){
				$video_url = $video->getAttribute('content');
				$embed = $parser->doc->createElement('iframe');
				$embed->setAttribute('frameborder', '0');
				$embed->setAttribute('src', $video_url);
				$div = $parser->doc->createElement('div');
				$div->appendChild($embed);
				$div->appendChild($content);
				$parser->content = $div;
				// this isn't found...it must be added to page from script, i don't know how to get video thumb
				// if ($image = $parser->queryOne('.//img[contains(@id, "media-asset-placeholder")]')){
				// 	$img = array('url' => $image->getAttribute('src'), 'video' => 'embed');
				// 	$this->post->addImage($img);
				// 	$this->post->handled = true;
				// 	return false;
				// }
			}

			$imageQuery = './/div[contains(@class, "caption")]//img | .//div[contains(@class, "story-feature image")]//img';
			if ($image = $parser->queryOne($imageQuery, $content)){
				if ($image->hasAttribute('src')){
					$this->post->addImage($image->getAttribute('src'));
					$this->post->handled = true;
					return false;
				}
			}

			if ($parser->scrape()){
				$this->post->handled = true;
				return false;
			}

		}else if ($gallery = $parser->queryOne('//div[@id="pictureGallery"]')){
			if ($gallery = $parser->queryOne('.//noscript//ul[@id="gallery"]', $gallery)){
				foreach($parser->queryEach('.//li', $gallery) as $li){
					$image = array();

					$src = $parser->queryOne('.//a[@href]', $li);
					$image['url'] = $src->getAttribute('href');

					if ($caption = $parser->queryOne('.//span[@class="picGalCaption"]', $li)){
						$image['caption'] = $caption->nodeValue;
					}

					$this->post->addImage($image);
				}
				if (count($this->post->images) > 0){
					$this->post->handled = true;
					return false;
				}
			}
		}

		if ($parser->queryOne('//div[contains(@class, "videoInStory")]')){
			if ($img = $parser->queryOne('//img[@class="holding"]')){
				$this->post->addImage($img->getAttribute('src'));
			}
			if ($content = $parser->queryOne('//div[@id="meta-information"]')){ //bbc got a typo in description
				$content->removeAttribute('class');
			}
		}

		if ($content = $parser->queryOne('//div[contains(@class, "layout-block")]')){
			if ($image = $parser->queryOne('//meta[@property="og:image"]')){
				if ($image->hasAttribute('content')){
					$this->post->addImage($image->getAttribute('content'));
					$this->post->extract = $this->getExtract();
					$this->post->handled = true;
					return false;
				}
			}
		}

		if (isset($parser->content)){
			$this->post->extract = $this->getExtract();
		}
		if (!empty($post->images)){
			$this->post->handled = true;
			return false;
		}

		$this->post->addImage($this->getDefault());
		if (!isset($this->post->extract)){
			$this->post->error = 'BBC failed extract';
			Log::error('BBC failed extract: '.$this->post->url);
			return true;
		}
		$this->post->handled = true;
		return false;
	}

	public function getExtract(){
		$parser = $this->parser;
		$content = $parser->content;
		$css = '';

		$parser->remove([
			'.//div[@id="page-bookmark-links-head"]', './/h2[@class="quote"]/span', './/div[@id="bbccomWebBug"]'
		]);

		$parser->removeClassNodes([
			'share-help', 'share-tools', 'bbccom_advert', 'ss_controls', 'player-wrapper', 'endquote', 'story-header',
			'blq-hide', 'hyperpuff', 'share-body-bottom', 'story-related', 'top-index-stories', 'story-date', 'page-timestamp'
		]);

		$parser->removeClasses([
			'lead', 'media-landscape', 'has-caption', 'container', 'selected', 'map-body display-feature-phone-only'
		]);

		$parser->removeAttributes([
			'data-asset-uri', 'width', 'height'
		]);

/*		foreach($parser->queryEach('.//div[@class="emp"]', $content) as $node){
			$parser->remove('.//object', $node);
			foreach($parser->queryEach('.//noscript', $node) as $noscript){
				$value = $node->ownerDocument->saveHTML($noscript);
				if (preg_match('/<img.*src="([^"]+).*>/', $value, $matches)){
					$src = $matches[1];
					$image = $node->ownerDocument->createElement('img');
					$image->setAttribute('src', $src);
					$image->setAttribute('title', 'Full video available at BBC.com');
					$node->appendChild($image);
					if (preg_match('/<img.*alt="([^"]+).*>/', $value, $matches)){
						$caption = $node->ownerDocument->createElement('div');
						$caption->setAttribute('class', 'caption');
						$text = $node->ownerDocument->createElement('span', $matches[1]. " - video available at BBC.");
						$caption->appendChild($text);
						$node->appendChild($caption);
					}
				}
				$noscript->parentNode->removeChild($noscript);
			}
		}*/

		if ($parser->queryOne('.//div[@class="hypertabs"]', $content)){
			// $css .= '.hypertabs{background:#EEE;}.hypertabs ul{margin-top:10px;}.hypertabs li{float:left;margin:0 15px;}';
			foreach($parser->queryEach('.//div[@class="hypertabs"]//ul', $content) as $ul){
				$clearfix = $content->ownerDocument->createElement('div', '');
				$clearfix->setAttribute('class', 'clearfix');
				$ul->appendChild($clearfix);
			}
		}
/*		if ($parser->queryOne('.//span[@class="story-date"]')){
			$css .= '.story-date{display:block;color: #505050; font-size: 13px;}'
					. '.story-date date{font-weight: bold;}.story-date .time-text{color: #666;}';
		}
		if ($parser->queryOne('.//span[contains(@class,"byline")]')){
			$css .= '.byline-photo{display:block;height:64px;margin-bottom:8px;}'
					. '.byline img{float:left;margin:0 8px 0 0;}'
					. '.byline-name{padding-bottom:2px;font-weight:bold;display:block;}'
					. '.byline-title{display:block;}';
		}
		if ($parser->queryOne('.//*[contains(@class,"embedded-hyper")]')){
			$css .= '.embedded-hyper{float: right; clear: right;margin:5px}'
					. '.embedded-hyper h2,.embedded-hyper h2 a{ color: #FFF; background: #D60000; font-size: 18px; padding: 13px 8px;}'
					. '.embedded-hyper ul{ margin: 0 0 0 10px;}';
		}

		if ($parser->queryOne('.//div[contains(@class, "body-width")]')){
			$css .= '.body-width{margin:0 0 15px;}.body-width span{display:block;font-style:italic;}';	//caption
		}

		$css .= '.story-body ul{ list-style-type: none;}'
			. '.story-body span.cross-head{font-weight: bold; display: block; margin: 0 0 16px 0; color: #505050;}'
			. '.story-body .caption span{display: block; padding-bottom: 10px; font-style: italic;}'
			. '.story-body .story-header{font-size: 1.8em; line-height: 36px;}'
			. '.story-body .related{position: relative; float: right; clear: right; display: inline; padding-left: 20px; width: 180px;}'
			. '.story-body .related h2{font-size: 1.231em; line-height: 16px; border-top: 1px solid #D8D8D8; border-bottom: 1px solid #D8D8D8; padding: 11px 0 12px; margin: 0 0 8px 0;}'
			. '.story-body #also-related-links h3{font-size: 18px;}'
			. '.story-body #lead .story-feature{float:none;width:auto;}'
			. '.story-body .wide{width: 300px;}'
			. '.story-body h2.quote{border-bottom: 1px solid #D8D8D8; border-top: 1px solid #D8D8D8;}';

		if ($parser->queryOne('.//h1', $content)){
			$css .= '.story-body h1{font-size:30px;line-height:1;margin-bottom:16px;}';
		}
		if ($parser->queryOne('.//*[contains(@class, "story-feature")]',$content)){
			$css .= '.story-feature{float:right;clear:right;width:160px;margin-left:20px;}'
					. '.story-feature.full, .story-feature.full-width{width:100%;}'
					. '.story-feature blockquote{float:left;font-weight:bold;padding:0;margin;0;}';
		}
		if ($parser->queryOne('.//p[@class="introduction"]',$content)){
			$css .= '.introduction{margin-top:10px;font-weight: bold;}';
		}
		if ($parser->queryOne('.//span[@class="info"]//a[contains(@class,"tag")]',$content)){
			$css .= '.info .tag{margin-left:5px;}';
		}*/

		$style = $content->ownerDocument->createElement('style', $css);
		$content->appendChild($style);

		return $parser->getExtract();
	}
	public function getDefault(){
		return array( 'url' => '/img/feedicons/bbc.jpg', 'ratio' => 1.25, 'width' => 450, 'height' => 360);
	}

}