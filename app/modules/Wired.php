<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class Wired extends Module {

	public function match($post)
	{
		if (stripos($post->url, "feeds.wired.com")){
			echo "Wired...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

		if ($author = $parser->queryOne('//a[@rel="author"]')){
			if ($author->hasAttribute('onclick')){
				$author->removeAttribute('onclick');
			}
			$this->post->author = $parser->doc->saveHTML($author);
		}

		$content = $parser->queryOne('//article | //div[@class="entry"]');
		if (!$content){
			return false;
		}
	    $parser->content = $content;

	    if ($image = $parser->queryOne('//meta[@property="og:image"]')){
	    	$src = $image->getAttribute('content');
	    	if ($parser->isImage($src)){
	    		$this->post->addImage($src);
	    		$this->post->extract = $this->getExtract();
	    		$this->post->handled = true;
	    		return false;
	    	}
	    }

	    if ($gallery = $parser->queryOne('//div[@class="gallery-caption"]')){
	    	$parser->content = $gallery;
	    	if ($img = $parser->queryOne('//div[@class="gallery-slide"]//noscript/img')){
	    		$src = $img->getAttribute('src');
	    		if ($parser->isImage($src)){
		    		$this->post->addImage($src);
		    		$this->post->extract = $this->getExtract();
		    		$this->post->handled = true;
		    		return false;
	    		}
	    	}
	    }

	    if ($gallery = $parser->queryOne('.//div[contains(@class, "wp35-gallery")]')){
	    	foreach($parser->queryEach('.//div[@class="slide"]', $gallery) as $slide){
	    		$image = array();
	    		$img = $parser->queryOne('.//img', $slide);
	    		if ($img->hasAttribute('src')){
	    			$image['url'] = $img->getAttribute('src');
	    		}else if ($img->hasAttribute('data-gallery-lazy-src')){
	    			$image['url'] = $img->getAttribute('data-gallery-lazy-src');
	    		}
	    		if ($img->hasAttribute('data-image-width')){
	    			$image['width'] = $img->getAttribute('data-image-width');
	    		}
	    		if ($img->hasAttribute('data-image-height')){
	    			$image['height'] = $img->getAttribute('data-image-height');
	    		}
	    		if ($caption = $parser->queryOne('.//p[@class="caption"]', $slide)){
	    			$image['caption'] = $caption->nodeValue;
	    		}
	    		$this->post->addImage($image);
	    	}
	    	$gallery->parentNode->removeChild($gallery);
	    }

	    if ($gallery = $parser->queryOne('.//div[contains(@id,"wpgallery")]')){
	    	if ($description = $parser->queryOne('.//li[contains(@class,"gallery-description")]', $gallery)){
	    		$parser->removeClassNodes(['clear'], true);

				//change <li>text</li> to <div>text</div>
				$div = $doc->createElement('div', '');
				$children = $description->childNodes;
				foreach($children as $child){
					if ($child->parentNode == $description){
						$kid = clone($child);
						$div->appendChild($kid);
					}
				}
				$parser->content = $div;
			}
			if ($video = $parser->queryOne('.//li[contains(@class,"gallery-slide")]//iframe', $gallery)){
	    		if ($parser->getVideoDataFromEmbed($video->getAttribute('src'))){
	    			//only displaying first iframe in gallery
	    			// $this->post->extract = "<p><a href='".$post->url."'>See the rest of the videos</a> at wired.com.</p>" . $post->extract;
	    			$this->post->extract = $this->getExtract();
	    			$this->post->handled = true;
	    			return false;
	    		}
	    		Log::error("!! Wired - video slides with no video data: ".$this->post->url);
	    	}
			foreach($parser->queryEach('.//li[contains(@class,"gallery-slide")]//noscript/img') as $imgNode){
				if ($imgNode->hasAttribute('src')){
					$this->post->addImage($imgNode->getAttribute('src'));
				}
			}

			$gallery->parentNode->removeChild($gallery);
			$this->post->extract = $this->getExtract();
			$this->post->handled = true;
			return false;
	    }

	    if ($parser->queryOne('.//div[@id="liveblog-hdr"]')){
	    	$parser->removeClassNodes(['api-shortcode']);
	    	$parser->removeIdNodes(['mobi-show'], false);
	    	$ht = "Check out the <a href='".$this->post->url."'>automatically refreshing liveblog</a>.";
	    	$parser->content->appendChild($parser->doc->createElement('p', $ht));
	    }

	    if ($parser->scrape()){
	    	$this->post->extract = $this->getExtract();
	    	$this->post->handled = true;
	    	return false;
	    }
	}

	public function getExtract(){
		$parser = $this->parser;

		$parser->removeClassNodes(['visually-hidden']);

		foreach($parser->queryEach('//figure') as $figure){
			if ($figure->hasAttribute('class')){
				$figure->removeAttribute('class');
			}
			if ($figure->hasAttribute('id')){
				$figure->removeAttribute('id');
			}
		}

		foreach($parser->queryEach('//img') as $img){
			if ($img->hasAttribute('width')){
				$img->removeAttribute('width');
			}
			if ($img->hasAttribute('height')){
				$img->removeAttribute('height');
			}
			if ($img->hasAttribute('class')){
				$img->removeAttribute('class');
			}
		}

		return $parser->getExtract();
	}
	public function getDefault(){
		return array( 'url' => '/img/feedicons/feed.jpg', 'ratio' => 0.978, 'width' => 1.25, 'height' => 360);
	}

}