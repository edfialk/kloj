<?php

namespace App\Modules;

use Log;
use Parser;
use App\Models\Image;

class Vimeo extends Module {

	private $id;

	public function match($post){
		$id = self::getId($post->url);
		if ($id !== false){
			echo "VIMEO handling...\n";
			$this->id = $id;
			$this->post = $post;
			return $this->handle();
		}
	}

	public static function getId($url){
		if (stripos($url, "//player.vimeo.com/video") === 0){
			$url = "http:".$url;
		}
		if ((($url = parse_url($url)) !== false) && (preg_match('~vimeo[.]com$~', $url['host']) > 0)){
			$url = array_filter(explode('/', $url['path']), 'strlen');
			if ($url[1] == 'video' && isset($url[2])){
				return $url[2];
			}
			if (in_array($url[0], array('album', 'channels', 'groups')) !== true){
				array_unshift($url, 'users');
			}
			return $url[1];
		}
		return false;
	}
	public function handle(){
		$request = "http://vimeo.com/api/v2/video/".$this->id.".php";
		$response = file_get_contents($request);
		if (!$response){
			Log::error('Vimeo unable to get response: '.$response);
			return true;
		}
		$vimeo = unserialize($response);
		if (isset($vimeo[0]) && isset($vimeo[0]['thumbnail_medium'])){
			$thumb_url = $vimeo[0]['thumbnail_medium'];
			$image = array('url' => $thumb_url, 'video' => 'vimeo:'.$this->id);
			$this->post->addImage($image);
			$this->post->handled = true;
			return false;
		}
		return true;
	}
}