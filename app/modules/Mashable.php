<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class Mashable extends Module {

	public function match($post)
	{
		echo "mashable match url: ".$post->url."\n";
		if (stripos($post->url, "mashable.com")){
			echo "Mashable handling...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}
	}

	public function handle(){
		$parser = $this->parser;

		if ($content = $parser->queryOne('//section[@class="article-content"]')){
			$parser->content = $content;
			// $this->post->extract = $this->getExtract();
		}

	    //if article has one featured image/video
	    if ($figure = $parser->queryOne('//figure[@class="article-image"]')){
	    	if ($frame = $parser->queryOne('.//iframe', $figure)){
	    		if(($image = $parser->getVideoDataFromEmbed($frame->getAttribute('src'))) !== false){
	    			$frame->parentNode->removeChild($frame);
	    			$this->post->addImage($image);
		    		$this->post->extract = $this->getExtract();
		    		$this->post->handled = true;
		    		return false;
	    		}
	    	}
	    	if ($img = $parser->queryOne('.//img[@data-fragment="lead-image"]', $figure)){
		    	$this->post->addImage($image->getAttribute('src'));
		    	$this->post->extract = this->getExtract();
		    	$this->post->handled = true;
		    	return false;
	    	}
	    }

	    if ($parser->scrape() !== false)
	  		$this->post->extract = $this->getExtract();
	  		$this->post->handled = true;
	  		return false;
	    }

	    $this->post->addImage($this->getDefault());
		$this->post->handled = true;
		return false;
	}

	public function getExtract(){
		$parser = $this->parser;
		$content = $parser->content;

		$parser->remove([
			'.//section[@data-display-mode="gallery"]'
		]);

		//fix up gallery list to display better in kloj
		foreach($parser->queryEach('.//section[@data-display-mode="list"]') as $node){
/*			foreach($parser->queryEach('.//h1', $node) as $header){
				$newHead = $header->ownerDocument->createElement('h3', $header->nodeValue);
				$header->parentNode->replaceChild($newHead, $header);
			}
			foreach($parser->queryEach('.//li//h2') as $lihead){
				$newhead = $lihead->ownerDocument->createElement('h3', $lihead->nodeValue);
				$lihead->parentNode->replaceChild($newhead, $lihead);
			}*/

/*			$css = "section ol {list-style-type: none; text-align: center;}";
			$style = $entry->ownerDocument->createElement('style', $css);
			$entry->appendChild($style);*/
		}

		$parser->removeAttributes([
			'data-fragment', 'data-micro', 'class', 'id', 'name', 'data-width', 'data-image', 'data-crackerjax',
			'data-id', 'data-slide-title-pos', 'data-slug', 'data-content-source', 'data-skip-rerender', 'data-thumb'
		]);

		return $parser->getExtract();
	}
	public function getDefault(){
		return array( 'url' => '/img/feedicons/feed.jpg', 'ratio' => 0.978, 'width' => 1.25, 'height' => 360);
	}

}