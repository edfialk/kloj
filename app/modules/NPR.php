<?php

namespace App\Modules;

use Parser;
use App\Models\Image;

class NPR extends Module {

	public function match($post)
	{
		if (stripos($post->url, "www.npr.org")){
			echo "NPR...\n";
			$this->post = $post;
			$this->parser = new Parser($post);
			return $this->handle();
		}

	}

	public function handle(){
		$parser = $this->parser;
		$html = $parser->doc->saveHTML();
		// if (stripos($this->post->url, '?ft=1') !== false){
		// 	$this->post->permalink = substr($url, 0, stripos($url, '?'));
		// }
		// echo $html;
		// die();
		if (preg_match('/NPR\.Player\.openPlayer\((\d+),%20(\d+)/', $html, $matches)){
			// print_r($matches);
			$this->story_id = $matches[1];
			$this->media_id = $matches[2];
		}

		if ($content = $parser->queryOne('//div[contains(@class, "transcript")]')){
			$parser->content = $content;
			if (!$parser->scrape()){
				$this->post->addImage($this->getDefault());
			}
			$this->post->extract = $this->getExtract();
			$this->post->handled = true;
			return false;
		}
		if ($content = $parser->queryOne('//div[@id="storytext"]')){
			// foreach($parser->queryEach('.//div[@class="imagewrap"]//img', $content) as $img){
			// 	if ($img->hasAttribute('data-original')){
			// 		$original = $img->getAttribute('data-original');
			// 		$src = str_replace(".jpg", "-s3.jpg", $original);	//pick better size image
			// 		$img->setAttribute('src', $src);
			// 	}
			// }
			$parser->content = $content;
			if (!$parser->scrape()){
				echo "fail scrape post ".$this->post->id."\n";
				$this->post->addImage($this->getDefault());
			}
			$this->post->extract = $this->getExtract();
			$this->post->handled = true;
			return false;
		}
		$this->post->addImage($this->getDefault());
		$this->post->handled = true;
		return false;
	}

	public function getExtract(){
		$parser = $this->parser;
		$node = $parser->content;

		$delClassNodes = array(
			'enlargebtn', 'enlarge_measure', 'enlarge_html', 'pnnavwrap', 'resaudio', 'spacer', 'hide-caption', 'toggle-caption'
		);
		$parser->removeClassNodes($delClassNodes);

		$parser->remove('.//b[@class="credit"]');

		$delIdNodes = array('resMore', 'commentBlock');
		$parser->removeIdNodes($delIdNodes);

		$delAtts = array('data-original', 'previewtitle', 'data-metrics');
		$parser->removeAttributes($delAtts);

		$removeClasses = array('insettwocolumn', 'video', 'youtube-video', 'graphic300');
		$parser->removeClasses($removeClasses);

		$css = '';
		if ($parser->queryOne('.//div[@class="tags"]')){
			$css .= '.tags ul{list-style-type:none;vertical-align:baseline;}'
				. '.tags li{float: left;margin: 0 3px 3px 0; text-transform:lowercase;display:list-item;}';
		}
		if ($parser->queryOne('.//div[@class="captionwrap"]')){
			$css .= '.captionwrap{padding:10px;font-style:italic;}.captionwrap p{margin-bottom:0;}';
		}
		if ($parser->queryOne('.//span[@class="creditwrap"]')){
			$css .= '.creditwrap{display:block;padding: 0 10px 10px;font-style:italic;font-size:0.8em;}';
		}
		if ($parser->queryOne('.//div[contains(@class, "bucketwrap")]')){
			$css .= '.bucketwrap{clear:left;margin-bottom:10px;}';
			$css .= '.bucketwrap h3{font-size:11px;}';
		}
		if ($parser->queryOne('.//div[contains(@class, "bucket img")]')){
			$css .= '.bucket.img img{float:left; width:80px;margin: 0 10px 0 0;border:none;}'
				. '.bucketblock{box-sizing:content-box;min-height:62px;margin-left:80px;padding: 9px 11px 9px 17px;}'
				. '.bucketblock h3{font-size:12px;line-height:1.273;margin-bottom:5px;margin-top:0;}'
				. '.selected .container.small{width:33%;margin-right:15px;float:left;}'
				. '.bucketwrap.internallink{margin-bottom:15px;}';
		}
		if ($parser->queryOne('.//p[contains(@class, "disclaimer")]')){
			$css .= '.disclaimer{font-style:italic;}';
		}

		if ($css != ''){
			$style = $node->ownerDocument->createElement('style', $css);
			$node->appendChild($style);
		}

		foreach($parser->queryEach('.//div[contains(@class, "con1col")]', $node) as $div){
			$class = $div->getAttribute('class');
			$class = str_replace('con1col', 'col-sm-4', $class);
			if (stripos($class, 'bucketwrap') === false) $class .= ' bucketwrap';
			$div->setAttribute('class', $class);
		}
		foreach($parser->queryEach('.//div[contains(@class, "bucketwrap image medium")]', $node) as $div){
			$div->setAttribute('class', 'bucketwrap col-sm-4');
		}
		foreach($parser->queryEach('.//div[contains(@class, "bucketwrap image small")]', $node) as $div){
			$div->setAttribute('class', 'bucketwrap col-sm-3');
		}
		foreach($parser->queryEach('.//*[contains(@id, "con")] | .//*[contains(@id, "res")]', $node) as $removeId){
			$removeId->removeAttribute('id');
		}
		foreach($parser->queryEach('.//div[contains(@class, "inset2col")]', $node) as $col){
			$classes = str_replace('inset2col', 'col-md-4', $col->getAttribute('class'));
			$col->setAttribute('class', $classes);
		}
		foreach($parser->queryEach('.//div[contains(@class, "medium")]', $node) as $n){
			$classes = str_replace('medium', 'col-md-6', $n->getAttribute('class'));
			$n->setAttribute('class', $classes);
		}

		if (isset($this->story_id) && isset($this->media_id)){
			$embed = $node->ownerDocument->createElement('embed');
			$embed->setAttribute('src', 'http://www.npr.org/v2/?i='.$this->story_id.'&#38;m='.$this->media_id.'&#38;t=audio');
			// $embed->setAttribute('height', '386');
			$embed->setAttribute('wmode', 'opaque');
			$embed->setAttribute('allowfullscreen', 'true');
			// $embed->setAttribute('width', '400');
			$embed->setAttribute('base', 'http://www.npr.org');
			$embed->setAttribute('type', 'application/x-shockwave-flash');

			// $node->ownerDocument->insertBefore($embed, $node->firstChild);
			$div = $node->ownerDocument->createElement('div');
			$div->appendChild($embed);
			$div->appendChild($node);
			$node = $div;
		}

		$parser->content = $node;
		return $parser->getExtract();
	}

	public function getDefault(){
		return array('url' => '/img/feedicons/npr-icon.png', 'ratio' => 1, 'width' => 50, 'height' => 50);
	}

}