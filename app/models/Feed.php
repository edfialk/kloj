<?php namespace App\Models;

use \Exception;
use Log;
use Parser;
use App\Models\Post;

class Feed extends \Eloquent {

  protected $table = 'feed';
  protected $MAXPOSTAGE = '-6 months';

  public $post_max = 10;

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $post->fill()
  * @var array
  */
  protected $fillable = array(
  	'link', 'icon'
  );

  protected $hidden = array('pivot', 'category_id');

  public static $rules = array(
  	'url'       => 'required',
    'title'     => 'required',
    'link'      => 'required'
  );

  public function fetchImage()
  {
    //get image from feed
    try {
      $feed = simplexml_load_file($this->url);
      $image = $feed->xpath('//channel/image/url');
      if ($image){
        $this->icon = $image->nodeValue;
      }
    }catch(Exception $e){
      Log::error('Feed fetchimage: '.$e->getMessage());
      Log::error($e->getTraceAsString());
      $this->error = 'Sorry, we cannot parse this feed.';
    }

    // $xml = file_get_contents($this->url);
    // $doc = new \DOMDocument('2.0', 'UTF-8');
    // $doc->loadXML($xml);
    // $xpath = new \DomXPath($doc);
  }

  /**
   * Fetch posts for this feed
   * @param  boolean $validate whether to re-fetch items already fetched
   * @return void
   */
  public function fetch($validate = false)
  {
    echo "fetching feed: ".$this->title."\n";

    $post_count = 0;

    $pie = Parser::getPie($this->url);
    if (!$pie){
      echo "Failed to get pie: ".$this->id."\n";
      return;
    }
    $items = $pie->get_items();
    if (count($items) === 0){
      echo "Feed has no items!\n";
    }
    foreach($items as $item){
      if ($this->post_max != -1 && $post_count >= $this->post_max){
        echo "max post count\n";
        return;
      }
      $post_count++;

      $query = Post::where('url', '=', $item->get_link())->get();
      if ($query->count() > 0){
        if ($validate){
          $post = $query[0];
          $post->images()->detach();
          $post->fetch();
        }
        continue;
      }

      if ( is_null($item->get_date()) || strtotime($item->get_date()) < strtotime($this->MAXPOSTAGE) ){
        echo "Cannot get date info for item, skipping.\n";
        continue;
      }
      $date = date('Y-m-d H:i:s', strtotime($item->get_date()));

      $post = Post::create(
        array(
          'title' => $item->get_title(),
          'url' => $item->get_link(),
          'date' => $date
        )
      );
      $post->feeds()->save($this);

      if (stripos($item->get_id(), "http") !== false){ //get_id checks guid, then link, then title. SOME feeds have permalink in <guid>
        $post->permalink = $item->get_id(); //hopefully actual link to page rather than feed rss gibberish link
      }else{
        //check for feedburner 'original link'
        $orig = $item->get_item_tags('http://rssnamespace.org/feedburner/ext/1.0', 'origLink');
        if (isset($orig[0]) && isset($orig[0]['data'])){
          $post->permalink = $orig[0]['data'];
        }
      }

      $post->text = $item->get_content() != null ? $item->get_content() : $item->get_description();

      try{
        $post->save();
      }catch(Exception $e){
        echo "Exception: ".$e->getMessage()."\n";
        Log::error($e);
        $post->feeds()->delete($this);
        $post->delete();
        continue;
      }
      echo('post id: '.$post->id.' ... ');

      $post->enclosure = $item->get_enclosures();
      try{
        $post->fetch($item);
      }catch(Exception $e){
        $post->error = 'Exception: '.$e->getMessage();
      }

      if (!$post->handled){
        //generic or something
        //for now, just delete it
        $post->error = "Post not handled: ".$post->url."\n";
      }
      if ($post->images->count() == 0){
        //generic image, TODO
        $post->error = "no image: ".$post->url."\n";
      }

      if (isset($post->error)){
        echo('Error: '.$post->error);
        $post->images()->detach();
        $post->feeds()->detach($this);
        $post->delete();
      }
    }
  }

  /**
  * Define the relationship with other tables
  */
  public function posts()
  {
    return $this->belongsToMany('App\Models\Post');
  }
  public function klojs()
  {
    return $this->belongsToMany('App\Models\Kloj');
  }
  public function category()
  {
    return $this->belongsTo('App\Models\Category');
  }
}