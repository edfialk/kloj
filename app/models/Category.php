<?php namespace App\Models;

class Category extends \Eloquent {

  protected $table = 'category';

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $post->fill()
  * @var array
  */
  protected $fillable = array(
  	'name'
  );

  public static $rules = array(
  	'name'       => 'unique|required'
  );

  /**
  * Define the relationship with other tables
  */
  public function feeds()
  {
    return $this->hasOne('App\Models\Feed');
  }
}