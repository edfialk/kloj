<?php namespace App\Models;

class Kloj extends \Eloquent {

  protected $table = 'kloj';

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $post->fill()
  * @var array
  */
  protected $fillable = array(
  	'title', 'description', 'theme_id', 'user_id'
  );

  protected $hidden = array('pivot', 'theme_id');

  // public static $rules = array(
  // );

  public function getPosts($length = 20, $after = null)
  {
    $feeds = $this->feeds;
    $posts = array();

    foreach($feeds as $feed){
      $feed_posts = $feed->posts->load('images')->toArray();

      //each post needs a reference to its feed without the feed's post property
      foreach($feed_posts as &$post){
        $f = $feed;
        unset($f->posts);
        $post['source'] = $f->toArray();
      }

      $posts = array_merge($posts, $feed_posts);
    }

    usort($posts, function($a, $b){
      return $a['date'] < $b['date'];
    });

    if (!is_null($after)){
      for($i = 0; $i < count($posts); $i++){
        if ($posts[$i]['id'] == $after){
          return array_slice($posts, $i+1, $length);
        }
      }
    }

    return array_slice($posts, 0, $length);
  }

  /**
  * Define the relationship with other tables
  */
  public function feeds()
  {
    return $this->belongsToMany('App\Models\Feed');
  }
  public function theme()
  {
    return $this->belongsTo('App\Models\Theme');
  }
  public function user()
  {
    return $this->belongsTo('User');
  }
}