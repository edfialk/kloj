<?php

namespace App\Models;

class ThemeAttribute extends \Eloquent {

  protected $table = 'themeattribute';

  public $timestamps = false;

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $theme->fill()
  * @var array
  */
  protected $fillable = array(
  	'token', 'value'
  );

  protected $hidden = array(
    'id', 'theme_id'
  );

  public static $rules = array(
  	'theme_id'      => 'required',
	  'token'  => 'required',
    'value'   => 'required'
  );

  /**
  * Define the relationship with the post table
  * @return Collection collection of Models
  */
  public function theme()
	{
		return $this->belongsTo('App\Models\Theme');
	}

}