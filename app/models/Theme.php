<?php namespace App\Models;

use Log;
use App\Models\ThemeAttribute;

class Theme extends \Eloquent {

  protected $table = 'theme';

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $theme->fill()
  * @var array
  */
  protected $fillable = array(
  	'user_id', 'template_id', 'created_at', 'updated_at'
  );

  protected $hidden = array('template_id');

  public static $rules = array(
  	'user_id'      => 'required',
	  'template_id'  => 'required',
    'created_at'   => 'required'
  );

  public function setOptions($options = array()){
    $this->options()->delete();
    $models = array();
    foreach($options as $key=>&$value){
      if ($value === false){
        $value = 'false';
      }else if ($value === true){
        $value = 'true';
      }
      array_push($models, new ThemeAttribute(array('token' => $key, 'value' => $value)));
    }
    $this->options()->saveMany($models);
  }

  public function loadOptions(){
    $attributes = ThemeAttribute::where('theme_id', '=', $this->id)->get();
    $tokens = $attributes->lists('token');
    $values = $attributes->lists('value');
    $models = array_combine($tokens, $values);
    if (count($models) > 0){
      $this->options = $models;
    }
  }

  /**
  * Define the relationship with the post table
  * @return Collection collection of Models
  */
  public function template()
	{
		return $this->belongsTo('App\Models\Template');
	}

  public function user()
  {
    return $this->belongsTo('App\Models\User');
  }

  public function options()
  {
    return $this->hasMany('App\Models\ThemeAttribute');
  }

  public function klojs()
  {
    return $this->belongsToMany('App\Models\Kloj');
  }

}