<?php namespace App\Models;

use Event;
use Exception;
use App\Models\Image;

class Post extends \Eloquent {

  protected $table = 'post';

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $post->fill()
  * @var array
  */
  protected $fillable = array(
  	'title', 'text', 'extract', 'author', 'date', 'url'
  );

  public static $rules = array(
  	'title'       => 'required',
	  'url'         => 'required',
    'date'        => 'required'
  );

  protected $hidden = array('pivot', 'created_at', 'updated_at');

  public function addImage($var)
  {
    if (is_string($var)){
      $image = Image::firstOrCreate(array('url' => $var));
    }else if (is_array($var) && isset($var['url'])){
      $image = Image::firstOrCreate(array('url' => $var['url']));
      foreach($var as $key=>$val){
        $image->$key = $val;
      }
      $image->save();
    }else if ($var instanceof DOMElement || get_class($var) == "DOMElement"){
      if ($var->hasAttribute('data-original')){ //data-original first, sometimes src is filler
        $url = $var->getAttribute('data-original');
      }else if ($var->hasAttribute('src')){
        $url = $var->getAttribute('src');
      }else {
        throw new Exception('Cannot find src for node: '.$var);
      }
      $image = Image::firstOrCreate(array('url' => $url));
    }else{
      throw new Exception('Invalid argument type for addImage');
    }

    $this->images()->attach($image);
    return $image;
  }

  public function fetch($item = null)
  {
    // echo "Fetching post: ".$this->title."\n";

    if (!isset($this->url)){
      Log::error('cannot fetch post without url. '.print_r($post->toArray(), true));
      $this->error = 'Invalid url.';
      return false;
    }

    if ($this->isReddit()){
      $this->temp = $this->url;
      $this->url = $this->redditLink();

      //returns reddit user name
      preg_match('/submitted by <a href="([^"]+)">([^<]+)/i', $this->text, $matches);
      $this->author = '<a href="http://reddit.com/user/'.$matches[2].'">'.$matches[2]."</a>";

    }else if (!is_null($item) && $author = $item->get_author()){
      if ($link = $author->get_link()){
        $this->author = '<a href="'.$link.'">'.$author->get_name().'</a>';
      }else{
        $this->author = $author->get_name();
      }
    }

    Event::fire('post.fetch', $this);

    if ($this->isReddit()){
      $this->url = $this->temp;
      unset($this->temp);
    }

    $handled = $this->handled;
    unset($this->handled);
    unset($this->enclosure);
    unset($this->reddit);

    $this->save();
    $this->handled = $handled;
  }

  public function isReddit()
  {
    if (isset($this->reddit)) return $this->reddit;
    if (stripos($this->url, "www.reddit.com") !== false){
      $this->reddit = true;
      return true;
    }
    $this->reddit = false;
    return false;
  }

  public function redditLink()
  {
    preg_match('/<a href="([^"]+)">\[link\]<\/a>/', $this->text, $matches);
    if (isset($matches[1])){
      return $matches[1];
    }

    Log::error("unable to get redditlink from text: ".$this->text);
    throw new Exception('Unable to get redditlink for text: '.$this->text);
  }

  /**
  * Define the relationship with the post table
  * @return Collection collection of Post Models
  */
  public function feeds()
  {
    return $this->belongsToMany('App\Models\Feed');
  }

  public function images()
  {
    return $this->belongsToMany('App\Models\Image');
  }

}