<?php

// namespace App\Models;

use Illuminate\Auth\UserInterface;
// use Illuminate\Auth\Reminders\RemindableInterface;
use Mmanos\Social\SocialTrait;

//used to "implements UserInterface, RemindableInterface"

class User extends Eloquent implements UserInterface {

	use SocialTrait;

	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	public static $rules = array(
	    'username'=>'required|alpha_num|min:2|unique:users',
	    'email'=>'required|email|unique:users',
	    'password'=>'required|alpha_num|between:6,12'
   );

	/**
	 *  Get user name
	 *
	 *  @return string
	 */
	public function getUserName()
	{
		return $this->getUsername();
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Get klojs created by the user
	 * @return Array of Kloj
	 */
	public function kloj()
	{
		return $this->hasMany('App\Models\Kloj');
	}

	/**
	 * Get settings for the user.
	 * @return Array of UserSettings
	 */
	public function settings()
	{
		return json_decode($this->settings);
	}

	public function addSetting($token, $value){
		$settings = $this->settings();
		$settings[$token] = $value;
		$this->setSettings($settings);
	}

	public function setSettings($settings)
	{
		$this->settings = json_encode($settings);
		$this->save();
	}

	public function getSetting($token)
	{
		$settings = $this->settings();
		if (isset($settings[$token])){
			return $settings[$token];
		}else{
			return null;
		}
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

}