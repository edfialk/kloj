<?php namespace App\Models;

use Log;

class Image extends \Eloquent {

  protected $table = 'image';

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $post->fill()
  * @var array
  */
  protected $fillable = array(
  	'ratio', 'width', 'height', 'video', 'caption', 'created_at', 'updated_at', 'url'
  );

  public static $rules = array(
  	'url'       => 'required',
    'ratio'     => 'required',
    'width'     => 'required',
    'height'    => 'required'
  );

  protected $hidden = array('pivot', 'created_at', 'updated_at');

  /**
  * Define the relationship with the post table
  * @return Collection collection of Post Models
  */
  public function posts()
	{
		return $this->belongsToMany('Post');
	}

  public static function boot()
  {
    parent::boot();

    static::saving(function($image)
    {
      if (!isset($image->ratio) || $image->ratio == 0 || !isset($image->width) || $image->width == 0 || !isset($image->height) || $image->height == 0)
      {
        $info = array();
        $url = str_replace(" ", "%20", $image->url);
        if (stripos($url, '/img/') === 0){
          $url = 'http://kloj.net'.$url;
        }
        $sizes = @getimagesize($url);
        if (!$sizes){
          Log::error("Cannot get image info : ".$url);
          return;
        }

        $image->width = $sizes[0];
        $image->height = $sizes[1];
        $image->ratio = $image['width'] / $image['height'];
      }
      return true;
    });
  }


}