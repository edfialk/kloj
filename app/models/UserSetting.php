<?php namespace App\Models;

class UserSetting extends \Eloquent {

  protected $table = 'usersettings';

  public $timestamps = false;

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $theme->fill()
  * @var array
  */
  protected $fillable = array(
  	'token', 'value'
  );

  protected $hidden = array(
    'id', 'user_id'
  );

  public static $rules = array(
  	'user_id'      => 'required',
	  'token'  => 'required',
    'value'   => 'required'
  );

  /**
  * Define the relationship with the post table
  * @return Collection collection of Models
  */
  public function user()
	{
		return $this->belongsTo('App\Models\User');
	}

}