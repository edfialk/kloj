<?php namespace App\Models;

class Template extends \Eloquent {

  protected $table = 'template';

  /**
  * Items that are "fillable"
  * meaning we can mass-assign them from the constructor
  * or $post->fill()
  * @var array
  */
  protected $fillable = array(
  	'file', 'name', 'description', 'attributes', 'created_at', 'updated_at'
  );

  public static $rules = array(
  	'file'        => 'required',
    'name'        => 'required',
    'attributes'  => 'required'
  );

  /**
  * Define the relationship with other tables
  */
  public function themes(){
    return $this->belongsToMany('App\Models\Themes');
  }
}