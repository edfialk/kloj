define(function(require, exports, module){
  "use strict";

  var App = require('app'),
      Post = require('models/Post');

  var Posts = Backbone.Collection.extend({

    model: Post,
    start: 0,

    url: function () {
      var base = App.root + App.apiVersion + '/kloj/'+this.id+'/posts';
      return this.after ? base + '?after=' + this.after : base;
    },

    initialize: function(models, options){
      options = options || {};
      if (options.id){
        this.id = options.id;
      }
    },

    sortByTitle: function(){
      this.comparator = function(post){
        return post.get("title");
      };
      this.sort();
    },
    sortByDate: function(){
      this.comparator = function(post){
        return post.get("date");
      };
      this.sort();
    },
    sortByAttr: function(attr){
      this.comparator = function(post){
        var key = post.get(attr);
        if (typeof(key)=='string') key = key.toLowerCase();
        return key;
      };
      this.sort();
    },
    clear: function(){
      this.reset();
      this.after = undefined;
      this.start = 0;
    }

  });

  return Posts;
});
