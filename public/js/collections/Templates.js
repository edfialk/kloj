define(function(require, exports, module){
  "use strict";

  var App = require('app'),
      Template = require('models/Template');

  var Templates = Backbone.Collection.extend({

    model: Template,

    url: function(){
      return App.root + App.apiVersion + '/template'
    },

  });

  return Templates;
});
