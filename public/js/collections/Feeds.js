define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Feed = require('models/Feed');

  var Feeds = Backbone.Collection.extend({

    model: Feed,

     url: function(){
      return App.root + App.apiVersion + '/feed';
    },

    initialize: function(){
      var c = this;
      this.on('add', function(){
        delete c.categories;
      });
    },

    /**
     * returns array of {id, value} for categories in THIS feed collection
     */
    getCategories: function(){
      if (this.categories != undefined){
        return this.categories;
      }

      var cats = _.map(this.models, function(model){
        return model.get('category');
      });
      cats = _.uniq(cats, function(cat){
        return cat.id;
      });
      this.categories = cats;
      return cats;
    },

    sortByTitle: function(){
      this.comparator = function(feed){
        return feed.get("title");
      };
      this.sort();
    },

    getBySelected: function(){
      return this.where({selected: true});
    },

    getSelectedIds: function(){
      return _.pluck(this.getBySelected(), 'id').join(',');
    },

    getByCategory: function(id){
      return _.filter(this.models, function(feed){
        return feed.get('category').id == id;
      });
    },

    /** returns an array of feeds Ids with the category id supplied **/
    getIdsByCategory: function(categoryId){
      var feedIds = [];
      $(this.models).each(function(i, feed){
        if (feed.get("category").id == categoryId){
          feedIds.push(feed.get("id"));
        }
      });
      return feedIds;
    },

    getIcons: function(){
      return _.pluck(this, 'icon');
    }

  });

  return Feeds;
});
