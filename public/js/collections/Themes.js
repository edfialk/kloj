define(function(require, exports, module){
  "use strict";

  var App = require('app'),
      Theme = require('models/Theme');

  var Themes = Backbone.Collection.extend({

    model: Theme,

    url: App.root + App.apiVersion + '/theme',

    initialize: function(models, options){
      options = options || {};
    	if (options.defaults){
    		this.url = this.url + '/defaults';
    	}
    },

    fetchDefault: function(){
    	var $req = $.ajax({
    		url: this.url + '/defaults',
    		type: 'GET',
    		dataType: 'json'
    	});
    	$req.done(function(json){

    	});
    },

    getDefault: function(){
    	return this.where({isDefault: true});
    }

  });

  return Themes;
});
