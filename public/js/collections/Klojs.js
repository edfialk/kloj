define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Kloj = require('models/Kloj');

  var Klojs = Backbone.Collection.extend({

    model: Kloj,

    url: function(){
      return App.root + App.apiVersion + '/kloj'
    },

    /** returns an array of klojs that have a feed with the category id supplied **/
    // used for BrowseView filtering Kloj List
    filterByCategory: function(id, feedCollection){
    	// var catFeedIds = feedCollection.getIdsByCategory(id);
      return _.filter(this.models, function(kloj){
        return kloj.get('feeds').getIdsByCategory(id).length > 0;
        // _.pluck(kloj.get('feeds').models, 'category').forEach(function(id, i){
        //   if ($.inArray(feed, catFeedIds) != -1){
        //     return true;
        //   }
        // });
        // return false;
      });
/*    	$(this.models).each(function(i, kloj){
        var found = false;
    		var klojfeeds = _.pluck(kloj.get('feeds').models, 'id');
    		$(klojfeeds).each(function(j, feed){
    			if ($.inArray(feed, feedIds) != -1){
            found = true;
    			}
    		});
        if (!found){
          collection.remove(this);
        }
    	});*/
    }

  });

  return Klojs;
});

