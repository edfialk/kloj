'use strict';

require.config({

  paths: {
    jquery: '//code.jquery.com/jquery-2.1.1.min',
    lodash: '//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min',
    backbone: 'backbone-min',
    bootstrap: '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min',
    // isotope: 'libs/jquery/jquery.isotope.min',       //commercial requires license
    minicolors: 'libs/miniColors/jquery.minicolors',
    // bookblock: 'libs/BookBlock/js/jquery.bookblock',
    jquerypp: 'libs/BookBlock/js/jquerypp.custom',
    modernizr: 'libs/BookBlock/js/modernizr.custom',
    // mobify: '//cdn.mobify.com/mobifyjs/build/mobify-2.0.0.min',
    // flowplayer: '//releases.flowplayer.org/5.4.4/flowplayer.min',
    // disqus: '//kloj.disqus.com/embed',
    // less: '//cdnjs.cloudflare.com/ajax/libs/less.js/1.7.0/less.min', // DEV ONLY
    // mustache: 'mustache',
    deepmodel: 'deep-model.min',
    async: 'libs/async',
    goog: 'libs/goog',
    propertyParser: 'libs/propertyParser',
    unveil: 'libs/jquery.unveil',
    echo: 'libs/echo.min',
    lazyload: 'libs/jquery.lazyload',
    masonry: 'libs/masonry.pkgd.min',
    imagesLoaded: 'libs/imagesloaded.pkgd.min'
  },

  map: {
    "*" : { "underscore" : "lodash" }
  },

  shim: {
    lodash: {
      exports: "_"
    },
    backbone : {
      deps: [ 'lodash', 'jquery' ],
      exports: 'Backbone'
    },
    bookblock: {
      deps: [ 'jquery', 'jquerypp', 'modernizr' ]
    },
    bootstrap: {
      deps: ['jquery']
    },
    minicolors: {
      deps: ['jquery']
    },
    flowplayer: {
      deps: ['jquery']
    },
    isotope: {
      deps: ['jquery']
    },
    deepmodel: {
      deps: [ 'lodash', 'backbone']
    }
  }

});