define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Template = require('models/Template');

  var Theme = Backbone.DeepModel.extend({

    defaults: {
      options: {}
    },

    urlRoot: App.apiVersion + '/theme/',

    map: {
      'bg'            : {selector: '.bg',                       property: 'background'},
      'postbg'        : {selector: '.postbg',                     property: 'background'},
      // 'textcolor'     : {selector: '.post',                     property: 'color'},
      'textfont'      : {selector: '.textfont,.textfont a',     property: 'font-family'}, //removed .post
      'linkcolor'     : {selector: '.post a, .selected a',      property: 'color'},
      'popupcolor'    : {selector: '.popupcolor',               property: 'color'},
      'headlinecolor' : {selector: '.headlinecolor',            property: 'color', important: false},
      'headlinesize'  : {selector: '.headlinesize',             property: 'font-size'},
      'headlinefont'  : {selector: '.headlinefont',             property: 'font-family'},
      'postheadlinecolor' : {selector: '.postheadlinecolor',    property: 'color'},
      'postborder'    : {selector: '.post',                     property: 'border'},
      'popupbg'       : {selector: '.popupbg',                  property: 'background'},
      'popupborder'   : {selector: '.selected',                 property: 'border'},
      'popbordercolor': {selector: '.selected',                 property: 'border-color'},
      'popbordersize' : {selector: '.selected',                 property: 'border-width'},
      'headlinebold'  : {selector: '.headline, .headline a',    property: 'font-weight'},
      'subtextfont'   : {selector: '.subtextfont',              property: 'font-family'},
      'subtextcolor'  : {selector: '.subtextcolor',             property: 'color'}
    },

    /* NOT LISTED */
    //headlinesize - list template
    //TODO: combos...i.e. 'color', 'background', 'font', ...

    url: function () {
      return App.root + App.apiVersion + '/theme/'+this.id;
    },

    //takes ajax response, returns object
    parse: function(response){
      response.isDefault = !!+response.isDefault; // 1 or 0 => true or false
      response.isPublic = !!+response.isPublic; // 1 or 0 => true or false

      var themeOpts = {};

      if (response.template){
        var template = new Template(response.template);
        response.file = template.get('file');
        response.template = template;
        themeOpts = template.get('options');
      }

      if (response.options){
        for (var key in response.options){
          var val = response.options[key];
          if (val.toLowerCase() == 'false' || val.toLowerCase() == 'true'){
            themeOpts[key] = ( val.toLowerCase() === 'true' );
          }
        }
      }

      response.options = themeOpts;

      return response;
    },

    //called on new Theme(data)
    initialize: function(data){
      _.bindAll(this);

      data = data || {};

      if (data.template){
        var template = new Template(data.template);
        this.set('options', template.get('options'));
        this.set('file', template.get('file'));
        this.set('template', template);
      }
      if (data.isDefault){
        this.set('isDefault', !!+data.isDefault);
      }
      if (data.isPublic){
        this.set('isPublic', !!+data.isPublic);
      }
      if (data.options){
        var options = this.get('options');
        for (var key in data.options){
          var val = data.options[key];
          if (val.toLowerCase() == 'false' || val.toLowerCase() == 'true'){
            this.set('options.'+key, val.toLowerCase() == 'true');
          }else{
            this.set('options.'+key, val);
          }
        }
      }
    },

    save: function(){
      return $.ajax({
        url: App.apiVersion + '/theme/save',
        type: 'POST',
        data: {
          theme: JSON.stringify(this)
        },
        dataType: 'json'
      });
    },

    getCSS: function(key){
      var map = this.map[key];
      if (map === undefined){
        console.error('Cannot get css for theme key: ' + key);
        return null;
      }
      if (this.get('options.'+key)){
        return map.selector + '{'+map.property+':'+this.get('options.'+key)+( map.important ? '!important' : '' )+';}';
      }
      return '';
    },

    hexToRgb: function(hex) {
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function(m, r, g, b) {
          return r + r + g + g + b + b;
      });
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? parseInt(result[1], 16) + "," + parseInt(result[2], 16) + "," + parseInt(result[3], 16) : null;
    }
  });

  return Theme;
});
