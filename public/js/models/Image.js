define(function(require, exports, module){
  "use strict";
  var App = require('App');

  var Image = Backbone.Model.extend({

    urlRoot: App.apiVersion + '/image/',

    url: function () {
      return App.apiVersion + '/image/'+this.id;
    },

    initialize: function(options){
      _.bindAll(this);
    }

  });

  return Image;

});