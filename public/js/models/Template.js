define(function(require, exports, module){
  "use strict";
  var App = require('app');

  var Template = Backbone.Model.extend({

    urlRoot: App.apiVersion + '/template/',

    url: function () {
      return App.apiVersion + '/template/'+this.id;
    },

    initialize: function(data){
      _.bindAll(this);

      if (data.attributes){
        this.set('options', JSON.parse(data.attributes));
        this.unset('attributes');
      }

    }

  });

  return Template;
});
