define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      KlojModel = require('models/Kloj');

  var User = Backbone.DeepModel.extend({

    urlRoot: App.apiRoot + '/user/',

    url: function(){
      return App.apiRoot + '/user';
    },

    initialize: function(options){
      _.bindAll(this);
      this._fetched = false;
    },

    login: function(username, pwd){
      var user = this;
      var $req = $.ajax({
        url: 'login',
        type: 'POST',
        data: {
          username: username,
          password: pwd,
          format: 'json'
        },
        dataType: 'json'
      });

      $req.done(function(data){
        if (data.status != 'error'){
          $.each(data, function(key, value){
            user.set(key, value);
          });
          user.trigger('login');
        }
      });

      return $req;
    },

    logout: function(){
      this.clear();
      var $req = $.ajax({
        url: '/logout',
        type: 'GET',
        format: 'json'
      });
      var user = this;
      $req.done(function(){
        user.trigger('logout');
      });
      return $req;
    },

    register: function(username, email, pwd){
      return $.ajax({
        url: 'register',
        type: 'POST',
        data: {
          username: username,
          email: email,
          password: pwd,
          format: 'json'
        },
        dataType: 'json'
      });
    },

    subscribeTo: function(kloj){
      if (!kloj.id){ throw new Error('Cannot unsubscribe to kloj without id!'); }

      var user = this,
          $req = $.ajax({
            url: App.apiVersion + '/user/subs/add',
            type: 'POST',
            data: {
              id: kloj.id
            },
            dataType: 'json'
          });

      $req.done(function(json){
        if (json.status != 'error'){
          var subs = user.get('subbed');
          subs.push({id: kloj.id, name: kloj.getTitle() });
          user.set('subbed', subs);
          user.trigger('user.subscribe', kloj);
        }
      });

      return $req;
    },

    unsubscribeTo: function(kloj){
      if (!kloj.id){ throw new Error('Cannot unsubscribe to kloj without id!'); }

      var user = this;
      var $req = $.ajax({
        url: App.apiVersion + '/user/subs/delete',
        type: 'POST',
        data: {
          id: kloj.id
        },
        dataType: 'json'
      });

      $req.done(function(json){
        if (json.status != 'error'){
          var subs = user.get('subbed');
          var index = subs.map(function(i) { return i.id; }).indexOf(kloj.id);
          subs.splice(index,1);
          user.set('subbed', subs);
          user.trigger('user.unsubscribe', kloj);
        }
      });

      return $req;
    },

    isSubbedTo: function(id){
      var subbed = this.get('subbed') || [], found = false;
      $(subbed).each(function(){
        if (this.id == id){
          found = true;
          return false;
        }
      });
      return found;
    },

    isLoggedIn: function(){
      return this.id !== undefined;
    },

    isAdmin: function(){
      return this.get('role') == 'admin';
    },

    getBookmarks: function(){
      return $.ajax({
        url: App.apiVersion + '/user/bookmarks',
        type: 'GET',
        dataTYpe: 'json'
      });
    },

    addBookmark: function(post){
      return $.ajax({
        url: App.apiVersion + '/user/bookmarks/add',
        type: 'POST',
        data: {
          id: post.id
        },
        dataType: 'json'
      });
    },

    delBookmark: function(post){
      return $.ajax({
        url: App.apiVersion + '/user/bookmarks/delete',
        type: 'POST',
        data: {
          id: post.id
        },
        dataType: 'json'
      });
    },

    setting: function(key, value){
      if (value === null){
        return this.get('settings.'+key);
      }else{
        this.set('settings.'+key, value);
      }
    }
  });

  return new User();
});
