define(function(require, exports, module){
  "use strict";
  var App = require('app');
  var Theme = require('models/Theme');
  var Posts = require('collections/Posts');
  var Feeds = require('collections/Feeds');

  var Kloj = Backbone.Model.extend({

    //attributes not for saving, updating, etc.
    blacklist: ['open', 'width'],

    url: function () {
      var root = App.root + App.apiVersion + '/kloj';
      return this.id ? root + '/' + this.id : root;
    },

    initialize: function(options){
      _.bindAll(this);
      options = options || {};

      if (options.theme){
        this.set('theme', new Theme(options.theme));
      }
      if (options.feeds){
        this.set('feeds', new Feeds(options.feeds));
      }
      if (this.get('theme') && this.get('feeds')){
        this.deferred = $.Deferred();
        this.deferred.resolve();
      }
      if (this.get('isFeatured')){
        this.set('isFeatured', this.get('isFeatured') === '1');
      }
      if (this.get('isPublic')){
        this.set('isPublic', this.get('isPublic') === '1');
      }

      if (options.posts){
        this.collection = new Posts(options.posts, {id: this.id});
        this.collection.deferred = $.Deferred();
        this.collection.deferred.resolve();
      }
    },

    parse: function(data){
      if (data.status == 'error'){
        this.error = 'error';
        this.message = status.message;
      }else{
        //have to do this in init and parse for 2 methods of creating kloj (init with options or with fetch)
        if (data.theme){
          this.set('theme', new Theme(data.theme));
          delete data.theme;
        }
        if (data.feeds){
          this.set('feeds', new Feeds(data.feeds));
          delete data.feeds;
        }
        if (data.isFeatured){
          this.set('isFeatured', data.isFeatured === '1');
        }
        if (data.isPublic){
          this.set('isPublic', data.isPublic === '1');
        }
        return data;
      }
    },

    toJSON: function(options){
      return _.omit(this.attributes, this.blacklist);
    },

    getURL: function(){
      return App.root + App.klojRoot + this.id;
    },

    getTitle: function(){
      // return this.get('name') || this.get('id');
      return this.get('name') || '';
    },

    getFollowerCount: function(){
      return 10;
    },

    filterByTag: function(tag){
      var results = this.collection.filter(function(post){
        return post.get('tags').indexOf(tag) != -1;
      });
      this.originalCollection = this.collection;
      this.collection = new PostCollection(results);
      this.collection.klojid = this.id;
      this.collection.tag = tag;
      this.collection.after = results[results.length - 1].id;
    },

    clearTheme: function(){
      this.unset('theme');
      if (this.collection){
        this.collection.invoke('set', {'el' : undefined, 'width' : undefined, 'content' : undefined, 'filler' : undefined});
      }
    }

  });

  return Kloj;

});