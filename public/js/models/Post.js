define(function(require, exports, module){
  "use strict";

  var App = require('app');
  // var Mobify = require('mobify');
  var Feed = require('models/Feed');

  var Post = Backbone.Model.extend({

    defaults: {
      size: "medium",
      reddit: false
    },

    // urlRoot: App.apiVersion + '/post/',

    url: function () {
      return App.root + App.apiVersion + '/post/' + this.id
    },

    initialize: function(options) {
      //called when fetching from collection or creating in js
      this.initPost();
    },

    parse: function(response){
      //called when fetching
      return response;
    },

    //seperate function so i can call after refresh
    initPost: function(){
      if (this.get('images') && this.get('images').length > 0){
        var image = this.get('images')[0];
        if (this.isDefaultImage(image.url)){
          this.set('defaultImage', true);
        }
        if (image.video != undefined && image.video != ''){
          this.set('video', image.video);
        }
      }
      if (this.get('source') && typeof this.get('source') == 'object'){
        this.set('source', new Feed(this.get('source')));
      }
      if (this.get('url') && this.get('url').indexOf('reddit.com') != -1){
        this.set('reddit', true);
        if (this.get('text').indexOf('submitted by') != -1){
          //not reddit self post, text is useless
          this.unset('text');
        }
      }
    },

    refresh: function(){
      var post = this,
          req = $.ajax({
            url: App.apiVersion + '/post/'+this.id+'/refresh',
            data: {
              id: this.id,
              feed: this.get('feedId')
            },
            type: 'POST',
            dataType: 'json'
          });

      req.done(function(data){
        var refreshFields = ['author', 'date', 'extract','imageIds','images','text','title','url', 'video', 'defaultImage', 'popup'];
        refreshFields.forEach(function(element){
          post.unset(element);
        });
        for (var key in data){
          post.set(key, data[key]);
        }
        post.initPost();
      });
      return req;
    },

    getThumbnailByWidth: function(width, index){
      var images = this.get("images");
      if (images == undefined || images.length == 0 || images.length < index) return '';
      if (index == null || isNaN(index)) index = 0;
      var img = images[index];
      if (img.url != undefined) img = img.url;
      if (img.indexOf(".gif") != -1) return img;
      //if (img.indexOf("/kloj/imgs") != -1 && document.location.host.indexOf("mykloj.com") != -1) img = img.replace("/kloj", "");
      // if (img.indexOf("imgs/") === 0) img = '/kloj/'+img;
      return "/thumb/phpThumb.php?w="+width+"&src="+escape(img);
    },
    getThumbnailByHeight: function(height, index){
      var images = this.get("images");
      if (images == undefined || images.length == 0 || images.length < index) return "";
      if (index == null || isNaN(index)) index = 0;
      var img = images[index];
      if (img.url != undefined) img = img.url;
      if (img.indexOf(".gif") != -1) return img;
      //if (img.indexOf("/kloj/imgs") != -1 && document.location.host.indexOf("mykloj.com") != -1) img = img.replace("/kloj", "");
      // if (img.indexOf("imgs/") === 0) img = '/kloj/'+img;
      return "/thumb/phpThumb.php?h="+height+"&src="+escape(img);
    },

    getAuthor: function(){
      var author = this.get("author");
      if (author == undefined || author == '') return '';
      if (typeof author === 'string') return author;
      if (author.name && author.link) return '<a href="'+author.link+'">'+author.name+'</a>';
      if (author.name) return author.name;
      return '';
    },
    getSource: function(){
      return this.get('source') || '';
    },
    getSourceLink: function(){
      var source = this.get('source');
      var link = this.get('feedlink');
      if (source == undefined) return '';
      if (link == undefined) return source;
      return "<a href='"+link+"'>"+source+"</a>";
    },
    getRatio: function(){
      if (this.get("images") === undefined || this.get("images").length === 0) return 0;
      return this.get("images")[0].ratio;
    },
    hasVideo: function(){
      if (this.get('video')){
        return true;
      }
      if (this.get('images') && this.get('images').length > 0 && this.get('images')[0].video){
        this.set('video', true);
        return true;
      }

      return false;
    },
    getVideo: function(){
      if (!this.hasVideo()){

      }
    },
    hasImage: function(){
      return this.get("images") && this.get("images").length > 0;
    },
    getImage: function(){
      if (!this.get('images') || this.get('images').length == 0){
        throw new Error('Post does not have an iamge.');
      }
      return this.get('images')[0];
    },
    hasGallery: function(){
      if (this.get('gallery')){
        return true;
      }
      if (this.get("images") != undefined && this.get("images").length > 1){
        this.set('gallery', true);
        return true;
      }

      return false;
    },
    isImgurGallery: function(){
      if (!this.hasGallery()) return false;
      var url = this.get('url');
      if (this.isReddit()) url = this.getRedditLink();

      return url.indexOf('imgur.com/') != -1;
    },
    isVideoInText: function(text){
      if (!this.hasVideo()){
        return false;
      }
      var post = this, found = false;
      var video = this.get('images')[0].video.split(":"), vidId = video[1];
      if (video == "embed"){
        return true;
      }
      $("<div>"+text+"</div").find("iframe").each(function(){
        if ($(this).attr('src').indexOf(vidId) != -1){
          found = true;
          return;
        }
      });
      return found;
    },
    //tests all of post images to see if url is present in text. text is probably extract or rss text
    isImageInText: function(){
      var post = this, found = false, text = this.get('extract');
      var urls = $.map(post.get("images"), function(o){ return o.url });
      $("<div>"+text+"</div>").find("img").each(function(){
        if (urls.indexOf($(this).attr("src")) != -1 ){
          found = true;
          return false; //break $.each;
        }
      });
      return found;
    },
    getExtract: function(){
      var text = this.get("extract") || this.get("text");
      if (!text){
        return '';
      }
      if (this.isReddit() && text.indexOf("submitted by") != -1){
        if (!this.isRedditSelf()) return ''; //reddit post with no extract and standard thumbnail, etc. I dont want any of that
        text = text.substring(0, text.indexOf("submitted by")); //relevant part of reddit self text
      }
      var $text = $('<div>'+text+'</div>');
      $text.find('iframe').each(function(){
        if ($(this).attr('src') == undefined){
          //twitter etc.
          return;
        }
        if ($(this).attr('src').indexOf('instagram.com') == -1 && !$(this).parent().hasClass('embed') && !$(this).parent().hasClass('video-container')){
          $(this).wrap('<div class="embed" />');  //instagram is entirely different ratio (which is purpose of embed class)
        }
      });
      $text.find('.embed').wrap($("<div class='embed-wrap'/>")); //for css styles
      text = $text.html();
      return text;
    },
    //[[TODO]] server side set image.default
    isDefaultImage: function(image){
      if (image.url != undefined) image = image.url;

      //this really should be regexp
      var defaults = [
        'static-secure.guim.co.uk/icons/social/og/gu-logo-fallback.png',
        'nytimes.com/images/icons/t_logo_',
        'nyt.com/images/icons/t_logo_',
        'nytimes.com/images/blogs_v3/../icons/t_logo_',
        'newsimg.bbc.co.uk/media/images/67373000/jpg/_67373987_09f1654a-e583-4b5f-bfc4-f05850c6d3ce.jpg',
        'sites/usatoday/images/site-masthead-logo',
        'img/feedicons'
      ];

      var is = false;
      defaults.forEach(function(url){
        if (image.indexOf(url) != -1) is = true;
      });

      return is;
    },
    getDate: function(){
      var date = new Date(this.get("date"));
      var today = new Date();
      if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getFullYear() == today.getFullYear()){
        var H = date.getHours(), M = date.getMinutes(), T = 'AM';
        if (H > 12){
          H -= 12;
          T = 'PM';
        }
        if (H == 12) T = 'PM';
        if (H == 0) H = '12';
        if (M < 10) M = '0'+M;
        return H+":"+M+" "+T;
      }else{
        return ( date.getMonth() + 1 ) + '/' + date.getDate();
      }
      return date.toLocaleString();
    },
    isReddit: function(){
      return this.get("reddit") == true;
    },
    isRedditSelf: function(){
      return this.get("url") == this.getRedditLink();
    },
    getRedditLink: function(){
      if (!this.isReddit()) throw new Error('Post is not reddit.');
      return this.get('redditLink');
    },


  });

  return Post;

});
