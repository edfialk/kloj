define(function(require, exports, module){
  "use strict";
  var App = require('app');
  var Feed = Backbone.Model.extend({

    defaults: {
      selected: false
    },

    urlRoot: App.apiVersion + '/feed/',

    url: function () {
      var root = App.root + App.apiVersion + '/feed';
      return this.id ? root + '/' + this.id : root;
    },

    toggle: function() {
      this.set("selected", !this.get("selected"));
    },
    select: function() {
      this.set("selected", true);
    },
    deselect: function() {
      this.set('selected", false);')
    }

  });

  return Feed;

});
