define(function(require, exports, module){
  'use strict';

  var App = require('app');

  var Router = Backbone.Router.extend({

    routes: {
      'browse' : 'browse',
      // 'about' : 'about',
      'profile' : 'profile',
      'edit/:id' : 'edit',
      'login' : 'login',
      'signin' : 'login',
      'logout' : 'logout',
      'signout' : 'logout',
      'signup' : 'signup',
      'register' : 'signup',
      'create' : 'create',
      // 'admin': 'admin',
      // 'saved' : 'saved',
      'k/:klojid(/)(:post)' : 'kloj',
      // 'k/:klojid/page/:page' : 'klojPage',

      'home' : 'index',
      '*defaultAction': 'index',
    },

    index: function(){
      App.User.deferred.done(function(){
        if (!App.User.isLoggedIn()){
          // window.location = '/welcome';
          var WelcomeView = require('views/WelcomeView');
          var view = App.cache.welcome || new WelcomeView();
          App.showView(view);
        }else{
          var HomeView = require('views/home/HomeView');
          var view = App.cache.home || new HomeView();
          App.showView(view);
        }
      });
    },

    kloj: function(klojid, postid){
      var KlojView = require('views/kloj/KlojView');
      App.showView(new KlojView({klojid: klojid, postid: postid}));
    },

    edit: function(id){
      var KlojView = require('views/kloj/KlojView');
      var EditView = require('views/kloj/EditView');
      App.showView(new EditView({
        klojview: new KlojView({klojid: id})
      }));
    },

/*    about: function(){
      require(['views/about/AboutView'], function(AboutView){
        app.showView(AboutView);
      });
    },*/

    browse: function(){
      var BrowseView = require('views/BrowseView');
      var view = App.cache.browse || new BrowseView();
      App.showView(view);
    },

    create: function(page){
      var View = require('views/kloj/create/CreateView');
      App.showView(new View());
    },

    admin: function(){
      var router = this;
      app.User.deferred.done(function(){
        if (app.User.isAdmin()){
          require(['views/admin/AdminView'], function(AdminView){
            app.showView(AdminView);
          });
        }else{
          router.navigate( "", {trigger: true} );
        }
      });
    },

    login: function(){
      var LoginView = require('views/LoginView');
      var view = new LoginView();
      App.showView(view);
      view.$el.find('#username').focus();
    },

    logout: function(){
      App.User.logout().done(function(){
        window.location = App.domain + App.root + 'welcome';
      });
      // this.navigate('/welcome', {trigger: true});
    },

    signup: function(){
      var View = require('views/SignupView');
      App.showView(new View());
    },

    profile: function(){
      var View = require('views/ProfileView');
      App.showView(new View());
    },

    saved: function(){
      app.User.deferred.done(function(){
        if (!app.User.isLoggedIn()){
          app.Router.navigate("", {trigger: true});
        }else{
          require(['views/account/SavedView'], function(SavedView){
            app.showView(SavedView);
          });
        }
      });
    }

  });

  return Router;
  // module.exports = Router;
});