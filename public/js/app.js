define(function(require, exports, module){
  'use strict';

  //globally required js files
  var
    _ = require('lodash'),
    $ = require('jquery'),
    Backbone = require('backbone'),
    DeepModel = require('deepmodel'),
    // mustache = require('mustache'),
    Bootstrap = require('bootstrap');

  var App = module.exports;
  var AppView = require('views/AppView');

  _(App).extend(Backbone.Events);

  App.domain = 'http://kloj.net';
  App.root = '/';
  App.klojRoot = 'k/';
  App.apiVersion = 'v1';
  App.apiRoot = App.root + App.apiVersion;
  App.cache = {};

  App.User = require('models/User');
  App.User.deferred = App.User.fetch();
  App.User.deferred.done(function(){
    App.User._fetched = true;
    App.trigger('user:ready');
    console.log("User: %O", App.User);
  });

  App.$el = $('#app');
  App.$body = $('body');
  App.$doc = $(document);
  App.view = new AppView({el: App.$el});


  // App.render = function(){
    App.$el.html(App.view.render());
  // };
  // App.render();

  App.showView = function(view){
    document.title = view.title ? 'Kloj - ' + view.title : 'Kloj';
    this.view.renderView(view);
  };

  App.getProgressBar = function(){
    return $("<div class='progress progress-striped active' style='width:50%;'><div class='progress-bar' role='progressbar' style='width:100%;'></div></div>");
  };

  App.alert = function(type, msg, timeout, $element, dismissable){
    type = type || 'success';
    var $alert = $('<div class="alert alert-'+type+' alert-dismissable alert-kloj"/>');
    if (dismissable !== false){
      $alert.append('<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times</button>');
    }
    $alert.append(msg);

    $element = $element || this.$el;
    $element.prepend($alert);

    if (timeout){
      window.setTimeout(function(){
        $alert.slideUp("slow", function(){
          $(this).remove();
        });
      }, timeout);
    }
  };

  App.getCategories = function(){
    return $.getJSON(App.apiRoot + '/category');
  };

  App.renderNav = function(page){
    this.view.renderNav(page);
  };

  App.getScrollbarWidth = function() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
  }


});
