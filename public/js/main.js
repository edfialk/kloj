if (typeof console == "undefined"){
  console = {
    log: function(){},
    debug: function(){}
  };
}

require(['config'], function(){
  require(['app', 'router'], function(app, Router){

    'use strict';

    app.Router = new Router();

    Backbone.history.start({ pushState: true, root: app.root, hashChange: false });

    // All navigation that is relative should be passed through the navigate
    // method, to be processed by the router. If the link has a `data-bypass`
    // attribute, bypass the delegation completely.
    $(document).on("click", "a[href]:not([data-bypass])", function(evt) {
      // Get the absolute anchor href.
      var href = { prop: $(this).prop("href"), attr: $(this).attr("href") };
      // Get the absolute root.
      var root = location.protocol + "//" + location.host + app.root;

      // Ensure the root is part of the anchor href, meaning it's relative.
      if (href.prop.slice(0, root.length) === root && href.attr.indexOf('#') !== 0) {
        // Stop the default event to ensure the link will not cause a page
        // refresh.
        evt.preventDefault();

        // `Backbone.history.navigate` is sufficient for all Routers and will
        // trigger the correct events. The Router's internal `navigate` method
        // calls this anyways.  The fragment is sliced from the root.
        Backbone.history.navigate(href.attr, true);
      }
    });
  });
}, function(err){
  console.log(err);
  $('#page').html('<div class="well text-center">We took too long to generate this page, sorry!</div>');
});