{{ HTML::style('/packages/mmanos/laravel-social/css/socialbuttons.css') }}

<div id='welcome'>

  <section id='intro'>
    <nav class='navbar navbar-default navbar-static-top' role='navigation'>
      <div class='container-fluid'>
        <div class='navbar-header'>
          <a href='/'><img src='img/headerlogo-trans.png' width='58px' height='40px'></a>
          <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navbar-collapse'>
            <span class='sr-only'>Toggle navigation</span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
            <span class='icon-bar'></span>
          </button>
        </div>
        <div class='collapse navbar-collapse' id='navbar-collapse'>
          <ul class='nav navbar-nav navbar-right'>
            <li class='dropdown'>
              <a href='' class='dropdown-toggle' data-toggle='dropdown'>Sign in <b class='caret'></b></a>
              <ul class='dropdown-menu' style='padding: 15px;min-width:250px;'>
                <li>
                  <div class='row'>
                    <div class='col-md-12'>
                      <form class='form' role='form' id='login-nav' action='login' method='POST'>
                        <div class='form-group'>
                          <label class='sr-only' for='username'>Username</label>
                          <input type='text' class='form-control username' name='username' placeholder='Username' required>
                        </div>
                        <div class='form-group'>
                          <label class='sr-only' for='password'>Password</label>
                          <input type='password' class='form-control password' name='password' placeholder='Password' required>
                        </div>
                        <div class='form-group'>
                          <button type='submit' class='btn btn-success btn-block'>Sign in</button>
                          <div id='login-status' class='status'></div>
                        </div>
                        <div>
                          <a href="{{ route('social-login', array('google')) }}" class="btn btn-social btn-google-plus">
                              <i class="fa fa-google-plus"></i> Log in with Google
                          </a>
                        </div>
                        <div class='forgot-link'>
                          <a href='forgot'>TROUBLE SIGNING IN</a>
                        </div>
                      </form>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
            <li><a href='register' data-bypass>Sign Up</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <div class='container'>
      <div class='row'>
        <div class='col-xs-12 col-md-8 col-md-offset-2 text-center'>
          <header>
            <h1>Welcome to <strong>Kloj</strong></h1>
            <h2>Really Sexy Syndication</h2>

            <p class='lead'>
              <a href='browse' class='btn btn-success'>Get Started</a>
            </p>
            <p>Kloj is an RSS reader and publisher made for following and discussing any website in a beautiful way. With several easily customizable templates and themes, you can keep up with your favorite feeds in exactly the way you want.</p>
          </header>
        </div>
      </div>
      <div class='row'>
        <div class='col-xs-12 col-md-8 col-md-offset-2'>
          <div id='carousel' class='carousel slide'>
            <ol class='carousel-indicators'>
              <li data-target='#carousel' data-slide-to='0' class='active'></li>
              <li data-target='#carousel' data-slide-to='1'></li>
              <li data-target='#carousel' data-slide-to='2'></li>
              <li data-target='#carousel' data-slide-to='3'></li>
              <li data-target='#carousel' data-slide-to='4'></li>
            </ol>
            <div class='carousel-inner'>
              <div class='item active'>
                <img src='img/welcome/grid-dark-comp.png'></a>
              </div>
              <div class='item'>
                <img src='img/welcome/list.png'>
              </div>
              <div class='item'>
                <img src='img/welcome/mag-c.png'>
              </div>
              <div class='item'>
                <img src='img/welcome/list-dark.png'>
              </div>
              <div class='item'>
                <img src='img/welcome/brick-c.png'>
              </div>
            </div>
            <a class='left carousel-control' href='#carousel' data-slide='prev'>
              <span class='glyphicon glyphicon-chevron-left'></span>
            </a>
            <a class='right carousel-control' href='#carousel' data-slide='next'>
              <span class='glyphicon glyphicon-chevron-right'></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id='features'>
    <div class='container'>
      <div class='row'>
        <div class='col-md-4'>
          <h3 class='text-center'>1000 feeds and growing</h3>
          <div class="row">
            <div class="col-xs-3">
              <img src="/img/feedicons/bbc-c.png" width="45" height="36" style="margin-top: 7px;">
            </div>
            <div class="col-xs-3">
              <img src="/img/feedicons/npr-icon.png" width="50" height="50">
            </div>
            <div class="col-xs-3">
              <img src="/img/feedicons/reddit.png" width="68" height="26" style="margin-top:15px">
            </div>
            <div class="col-xs-3">
              <img src="/img/feedicons/UXMagazine-c.png" width="51" height="51" style="margin-top: 7px;">
            </div>
          </div>
          <p>If there's a website we don't have, you can add it! Set it to private to prevent other users from using it.</p>
          <p class='text-center'><i>Not really 1000 yet.</i></p>
        </div>
        <div class='col-md-4'>
          <h3 class='text-center'>See what people are saying</h3>
          <p>The web is meant to be social. Comment on any item for just your friends or for everyone, or see comments from the original source, Facebook, Twitter, or Reddit. Publish your kloj for others to read and follow what others are reading.</p>
        </div>
        <div class="col-md-4">
          <h3 class='text-center'>Quick and Easy</h3>
          <p>Create a new kloj in seconds. You don't even have to sign in. Promote, remove, or save any post while reading. Live preview new item structure, colors, fonts, and behavior.</p>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class='container'>
      <div class='row'>
        <div class='col-xs-12 text-center'>
          <span>An unfinished experiment by <a href='http://edfialk.com'>Ed Fialkowski</a></span>
          <span><a href='/contact'>Contact Me</a></span>
        </div>
      </div>
    </div>
  </footer>
</div>