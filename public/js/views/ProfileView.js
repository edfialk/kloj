define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      template = require('text!templates/user/profile.html');

  var ProfileView = Backbone.View.extend({

    id: 'profile',

    events: {
      'click .edit-link' : 'clickEdit'
    },

    initialize: function(options) {
      _.bindAll(this);
      this.isEditing = [];
      this.isEditing['username'] = false;
      this.isEditing['email'] = false;
      this.isEditing['password'] = false;
    },

    render: function(){
      // App.trigger('sidebar:select', 'profile-tab');

      var view = this;
    	App.User.deferred.done(function(){
    		if (!App.User.isLoggedIn()){
    			App.Router.navigate('', {trigger: true});
    		}else{
          view.$el.html(_.template( template, {User: App.User} ));
          App.renderNav('profile');
        }
    	});

      return this.el;
    },

    clickEdit: function(e){
      var field = e.target.getAttribute('data-target');
      var $group = this.$('div[data-group="'+field+'"]');
      var group = $group.data('group');
      if (this.isEditing[group]){ //was editing, clicked done
        var $input = $('input', $group).addClass('hidden');
        $('.text-span', $group).text($input.val()).removeClass('hidden');
        $('.edit-link', $group).text('Edit');
      }else{ //wasn't editing, clicked edit
        $('.text-span', $group).addClass('hidden');
        $('input', $group).removeClass('hidden').focus();
        $('.edit-link', $group).text('done');
      }
      this.isEditing[group] = !this.isEditing[group];
    }

  });

  return ProfileView;
});