define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Kloj = require('models/Kloj'),
      Post = require('models/Post'),
      Posts = require('collections/Posts'),
      Theme = require('models/Theme'),
      template = require('text!templates/kloj/viewTemplate.html'),
      EditView = require('views/kloj/EditView');

  var KlojView = Backbone.View.extend({

    id: 'kloj',
    className: 'bg',
    events: {
      'click .post-next' : 'clickNext',
      'click .post-prev' : 'clickPrev',
      'click .post-close' : 'clickClose',
      'click #goTop' : 'clickTop',
      'click .kloj-refresh' : 'clickRefresh',
      'click .kloj-edit' : 'clickEdit',
      'click [data-action="delete"]': 'clickDelete',
      'click .kloj-feature': 'clickFeature',
      'click .render-theme': 'clickTheme',
      'click .post-hide' : 'clickHide'
    },

    //behaviorial keys are dealt with separately
    themeKeys: [
      'bg', 'postbg', 'linkcolor', 'headlinefont', 'headlinecolor', 'postheadlinecolor', 'popupbg', 'popupcolor', 'popupborder', 'textcolor', 'textfont', 'subtextcolor', 'headlinesize'
    ],

    initialize: function(options){
      _.bindAll(this);

      var kloj;
      if (module.config().kloj && module.config().kloj.id == options.klojid){
        //bootstrapped model
        kloj = new Kloj(module.config().kloj);
        kloj.deferred = $.Deferred();
        kloj.deferred.resolve();
      }else if (options.model && options.model instanceof Backbone.Model){
        //passed in model through init
        kloj = options.model;
        kloj.deferred = $.Deferred();
        kloj.deferred.resolve();
      }else if (options.klojid){
        //fetch kloj
        kloj = new Kloj({id: options.klojid});
        kloj.deferred = kloj.fetch();
      }else{
        console.error("Invalid Kloj option", options);
      }
      this.kloj = kloj;

      this.kloj.set('view', this);

      if (!this.kloj.id){
        console.error('No kloj id!');
        debugger;
      }

      if (!kloj.collection){
        var posts = new Posts([], { id: this.kloj.id });
        posts.deferred = posts.fetch();
        this.kloj.collection = posts;
      }else if (!kloj.collection.deferred){
        this.kloj.collection.deferred = this.kloj.collection.fetch();
      }

      var view = this;
      this.listenTo(this.kloj, 'change:open', this.openListener);
      this.listenTo(this.kloj, 'change:title', function(){
        if (view.$header){
          view.$header.find('.title').text(view.kloj.get('title'));
        }
      });

      this.kloj.deferred.done(function(){
        _.each(view.kloj.get('feeds').models, function(feed){
          var id = feed.id;
          var cssid = 'feed-css-'+id;
          if (!document.getElementById(cssid)){
            var css = '/css/modules/'+id+'.css';
            var link = document.createElement('link');
            link.setAttribute('type', 'text/css');
            link.setAttribute('href', css);
            link.setAttribute('id', cssid);
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('class', 'feed-style');
            document.getElementsByTagName('head')[0].appendChild(link);
          }
        });
      });
    },

    close: function(){
      this.$scrollable.off('scroll', this.scrollListener);
      App.$body.off('keypress', this.keyListener);
      this.stopListening();
      this.undelegateEvents();
      if (this.subview && this.subview.close){
        this.subview.close();
      }
      $('.feed-style').remove();
      this.kloj.unset('view');

    },

    render: function(){
      this.$el.html('<div class="loader"></div>');
      // this.$scrollable = $('#content');
      this.$scrollable = $(window);
      this.$scrollable.scroll(this.scrollListener);
      App.$body.on('keydown', this.keyListener);

      var view = this;
      $.when(this.kloj.deferred, App.User.deferred).then(function(){
        document.title = view.kloj.get('title') || 'Kloj';
        console.log('kloj render: %O', view.kloj);
        var data = {
          fullpage: true,
          User: App.User,
          kloj: view.kloj,
        };
        view.$el.html(_.template(template, data));
        view.$body = view.$('#kloj-body');
        view.$header = view.$('#kloj-header');
        view.$postActions = view.$('.post-action');
        view.$web = view.$('.kloj-web');

        view.setupTheme();

        if (!view.kloj.get('theme').get('template')){
          view.$body.html('');
          App.alert('danger', 'Sorry, this theme has become corrupted. Please choose a theme from the settings menu.', null, view.$body, false);
          return;
        }

        if (view.subview !== undefined){
          view.renderSubview();
          view.subview.delegateEvents();
        }else{
          var file = 'views/kloj/'+view.kloj.get('theme').get('template').get('file');
          require([file], function(Subview){
            view.subview = new Subview({model: view.kloj, parent: view});
            view.renderSubview();
          });
        }
      });

      this.kloj.deferred.fail(function(){
        view.$el.html("<div class='well'>There is no Kloj with that url.</div>");
        App.renderNav();
      });

      return this.el;
    },

    renderSubview: function(){
      this.kloj.set('width', this.$body.width() - App.getScrollbarWidth());
      this.$body.html(this.subview.render());
      this.scrollListener();
    },

    setupTheme: function(){
      var $head = $('head'),
          $css = $('#kloj-theme', $head);

      if ($css.length === 0){
        $css = $("<style id='kloj-theme'/>");
      }else{
        $css.detach().text('');
      }

      // console.log('klojview setuptheme, id: ' + this.kloj.get('theme').cid + ': %O', this.kloj.get('theme'));

      var view = this;
      var theme = this.kloj.get('theme');
      this.themeKeys.forEach(function(key){
        // console.log('option['+key+'] = ' + theme.get('options.'+key));
        if (theme.get('options.' + key)){
          $css.append(theme.getCSS(key));
        }
      });
      $css.appendTo($head);

      // this.$scrollable.off('scroll');
      // this.$scrollable.scroll(this.scrollListener);
    },

    scrollListener: function(){
      var pos = this.$scrollable.scrollTop(),
          height = this.$scrollable.height(),
          view = this;

      if (!this.$header) return; //scroll between loading

      this.$header.toggleClass('down', pos > 25);

      if (this.subview && this.subview.append && ( pos + $(window).height() + 200 > this.$el.height() ) && !this.loading && this.kloj.collection.length > 0){
        this.loading = true;
        this.kloj.collection.start = this.kloj.collection.length;
        this.kloj.collection.after = this.kloj.collection.at(this.kloj.collection.length - 1).id;
        this.kloj.collection.fetch({
          remove: false,
          success: function (collection, resp){
            var newModels = view.kloj.collection.models.slice(view.kloj.collection.start);
            if (newModels.length > 0){
              view.subview.append(newModels);
              view.loading = false;
            }
          }
        });
      }
    },
    keyListener: function(e){
      var code = (typeof e.which == "number") ? e.which : e.keyCode
      if (this.kloj.get('open')){
        if (code == 74){ //j
          this.next();
        }else if (code == 75){ //k
          this.prev();
        }
      }
    },
    openListener: function(kloj, options){
      if (kloj.get('open')){
        this.openPost(kloj.get('open'));
      }else{
        this.closePost();
      }
    },

    openPost: function(post){
      this.selected = post;
      this.$postActions.show();
      this.$web.attr('href', this.selected.get('url')).off().on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        window.open(post.get('url'), '_blank');
      });
    },
    closePost: function(){
      this.selected = null;
      this.$postActions.hide();
    },

    clickNext: function(e){
      e.preventDefault();
      e.stopPropagation();
      this.next();
    },
    clickPrev: function(e){
      e.preventDefault();
      e.stopPropagation();
      this.prev();
    },
    clickClose: function(e){
      e.preventDefault();
      e.stopPropagation();
      if (!this.selected) return;
      this.$scrollable.scrollTop(this.selected.get('el').position().top);
      this.selected.set('open', false);
    },
    clickRefresh: function(e){
      if (this.selected){
        this.selected.set('open', false);
      }
      this.kloj.collection.clear();
      this.kloj.collection.deferred = this.kloj.collection.fetch();
      this.subview.render();
    },
    clickTop: function(){
      if (this.selected){
        this.selected.set('open', false);
      }
      this.$scrollable.scrollTop(0);
    },
    clickEdit: function(e){
      e.preventDefault();
      e.stopPropagation();
      if (this.selected){
        this.selected.set('open', false);
      }
      this.editView = new EditView({klojview: this});
      App.Router.navigate("edit/"+this.kloj.id);
      App.$el.html(this.editView.render());
    },
    clickDelete: function(e){
      e.preventDefault();
      e.stopPropagation();
      var modalTemplate = require('text!templates/modalTemplate.html');
      var $confirm = $(_.template(modalTemplate, {
        title: "Are you sure you want to delete this kloj?",
        body: "This action cannot be undone.",
        showCancel: true,
        saveText: 'Delete'
      }));
      $('body').append($confirm);
      $confirm.modal();
      var view = this;
      $('#save', $confirm).click(function(){
        var $req = $.ajax({
          url: App.apiRoot + '/kloj/' + view.kloj.id,
          type: 'DELETE',
          data: {
            '_method': 'delete'
          }
        });
        $req.done(function(json){
          $confirm.modal('hide');
          App.alert('success', 'Kloj is deleted.', 3000);
          window.setTimeout(function(){
            App.Router.navigate('/', {trigger: true});
          }, 1000);
        });
        $req.fail(function(){
          $confirm.modal('hide');
          App.alert('danger', 'Sorry, delete failed. Please try again.', 5000);
        });
      });
    },
    clickFeature: function(e){
      e.preventDefault();
      this.kloj.set('isFeatured', !this.kloj.get('isFeatured'));
      this.kloj.save().done(function(){
        App.alert('success', 'saved.', '1000');
      });
      if (this.kloj.get('isFeatured')){
        $('.kloj-feature', this.$header).text('Unfeature');
      }else{
        $('.kloj-feature', this.$header).text('Feature');
      }
    },
    clickTheme: function(e){
      e.preventDefault();
      var view = this,
          themeId = $(e.target).data('id'),
          theme = new Theme({id: themeId});

      if (this.kloj.get('open')){
        this.kloj.get('open').set('open', false);
      }

      this.kloj.clearTheme();
      this.clearTheme();
      this.kloj.set('theme', theme);

      theme.deferred = theme.fetch({
        success: function(){
          view.render();
        }
      });
    },
    clickHide: function(e){
      console.log(e);
    },

    next: function(){
      if (!this.selected) return;
      var post = this.selected;
      post.set('open', false);
      //relateds
      var index = this.kloj.collection.indexOf(post);
      if (index != this.kloj.collection.length - 1){
        var next = this.kloj.collection.at(index+1);
        next.set('open', true);
      }
    },
    prev: function(){
      if (!this.selected) return;
      var post = this.selected;
      post.set('open', false);
      //relates
      var index = this.kloj.collection.indexOf(post);
      if (index !== 0){
        var prev = this.kloj.collection.at(index-1);
        prev.set('open', true);
      }
    },

    clearTheme: function(){
      if (this.subview){
        if (this.subview.close){
          this.subview.close();
        }
        delete this.subview;
      }
    },

  });

  return KlojView;
});
