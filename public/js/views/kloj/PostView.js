define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      contentTemplate = require('text!templates/kloj/contentTemplate.html'),
      actionsTemplate = require('text!templates/kloj/actionsTemplate.html');

  var PostView = Backbone.View.extend({

    tagName: 'div',

    className: 'post postbg',

    events: {
      'click .post-title a' : 'clickLink',
      'click .post-subtext a' : 'clickLink',
      'click' : 'clickPost',
      // 'click .post-hide' : 'clickHide'
    },

    initialize: function(options){
      _.bindAll(this);
      this.options = options;
      this.model = options.model;
      this.theme = options.theme;
      this.template = options.template;
      this.parent = options.parent;
      this.mode = options.mode;

      this.model.set('view', this);
      this.model.set('el', this.$el);

      this.$el.attr('data-id', this.model.id);

      this.showVideo = this.model.hasVideo() && !this.model.isVideoInText();
      this.showGallery = this.model.hasGallery();
      this.showImage = this.model.hasImage() && !this.model.isImageInText() && !this.model.get('defaultImage') && !this.showVideo && !this.showGallery;

      this.isVideo = this.showVideo && !this.model.get('extract');
      this.isGallery = this.showGallery && !this.model.get('extract');
      this.isImage = this.showImage && !this.model.get('extract');

      this.listenTo(this.model, 'change:open', this.openListener);

    },

    render: function(){
      var data = {
        post: this.model,
        theme: this.theme,
        options: this.options,
        view: this
      };
      this.$el.html(_.template(this.template, data));

      return this.el;
    },

    openListener: function(post, options){
      if (!post.get('open')){
        this.stopVideo();
      }

    },

    renderContent: function(){
      var data = {
        post: this.model,
        view: this
      };
      var $content = $(_.template( contentTemplate, data ));

      $content.find('iframe').each(function(){
        var src = this.getAttribute('src');
        if (src.indexOf('youtube') !== -1 && src.indexOf('enablejsapi') === -1){
          src += src.indexOf('?') !== -1 ? '&' : '?';
          src += 'enablejsapi=1';
          this.setAttribute('src', src);
        }
      });

      this.model.set('content', $content);
    },

    clickPost: function(e){
      if (!this.model.get('open')){
        // e.preventDefault();
        e.stopPropagation();
        this.model.set('open', true);
      }
    },

    clickLink: function(e){
      if (e.which == 2){ //middle click
        e.stopPropagation();
        return;
      }
      e.preventDefault();
    },

    renderSubtext: function(){
      var pieces = [],
          post = this.model,
          theme = this.theme;

      //default is to show pieces so if no theme or if option is not set to false
      if (!theme  || theme.get('options.showSource') !== false && post.get('source')){
        var feed = post.get('source');
        var html = '<span class="post-source">';
        if (feed.get('link')){
          html += '<a href="'+feed.get('link')+'">'+feed.get('title')+'</a>';
        }else{
          html += feed.get('title');
        }
        if (post.get('related')){
          html += ' and '+post.get('related').length+' more';
        }

        html += '</span>';
        pieces.push(html);
      }

      if (!theme || theme.get('options.showAuthor') !== false && post.getAuthor() !== ''){
        pieces.push('<span class="post-author">'+post.getAuthor()+'</span>');
      }

      if (!theme || theme.get('options.showDate') !== false && post.getDate() !== ''){
        pieces.push('<span class="post-date">'+post.getDate()+'</span>');
      }

      return pieces.join (' - ');
    },

    renderVideo: function(){
      var video = this.model.get('video');
      if (!video){
        console.log('Getting video embed for post without video.');
        return '';
      }
      var vid = video.split(":"), //format is type:id
          type = vid[0],
          id = vid[1],
          extract = this.model.getExtract(),
          html = '<div class="embed-wrap">';
      if (type == 'youtube'){
        this.video = 'youtube';
        if (extract.indexOf('/embed/'+id) !== -1) return ''; //don't double embed
        html += '<iframe class="youtube-video" src="http://www.youtube.com/embed/'+id+'?enablejsapi=1&origin='+encodeURI(App.domain)+'" frameborder="0" allowfullscreen></iframe>';
      }else if (type == 'vimeo'){
        this.video = 'vimeo';
        if (extract.indexOf('/vimeo/video/'+id) !== -1) return ''; //don't double embed
        html += '<iframe src="//player.vimeo.com/video/'+id+'?badge=0&amp;color=ffd503" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
      }else if (type == 'url'){
        this.video = 'html5';
        id = video.replace('url:', ''); //did not think ahead on using : as delimiter with http://
        html = '<div class="player"><video><source type="video/mp4" src="'+id+'"></video></div>';
      }
      html += '</div>';
      return html;
    },

    renderGallery: function(){
      return _.template(
        require('text!templates/kloj/galleryTemplate.html'),
        {
          images: this.model.get('images'),
          id: this.model.id
        }
      );
    },

    stopVideo: function(){
      if (!this.model || !this.model.get('content')){
        return;
      }
      this.model.get('content').find('iframe').each(function(){
        if (this.contentWindow){
          this.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        }
      });
    },

    renderActions: function(){
      return _.template( actionsTemplate, {mode: this.mode, post: this.model} );
    }

  });

  return PostView;
});
