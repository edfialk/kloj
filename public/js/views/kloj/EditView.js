define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Kloj = require('models/Kloj'),
      Post = require('models/Post'),
      Posts = require('collections/Posts'),
      Themes = require('collections/Themes'),
      Templates = require('collections/Templates'),
      template = require('text!templates/kloj/edit/template.html'),
      MiniColors = require('minicolors');

  var EditView = Backbone.View.extend({

    id: 'kloj-edit',

    events: {
      'click #btn-done' : 'clickDone',
      'click #btn-save' : 'clickSave',
      'keyup #title' : 'setTitle',
      'change #template' : 'setTemplate',
      'change .content form' : 'formChange',
      'change #subtextinherit' : 'subtextInheritChange',
      'click .iphone-style' : 'clickSlider'
    },

    initialize: function(options){
    	if (!options){
    		throw new Exception('Cannot make EditView without options.');
    	}
    	this.klojview = options.klojview;
      this.kloj = this.klojview.kloj;
      if (!this.kloj.deferred){
        this.kloj.deferred = this.kloj.fetch();
      }

      this.templates = new Templates();
      this.templates.deferred = this.templates.fetch();

      this.schemes = new Themes([], {defaults: true});
      this.schemes.deferred = this.schemes.fetch();

    },

    render: function(options){
      var view = this;
      $.when(this.kloj.deferred, this.templates.deferred, this.schemes.deferred).then(function(){
        view.theme = view.kloj.get('theme');

        view.$el.html(_.template(template, {
          kloj: view.kloj,
          theme: view.theme,
          template: view.theme.get('template'),
          templates: view.templates,
          schemes: view.schemes
        }));

        view.$klojBlock = view.$('#kloj');
        view.klojview.setElement(view.$klojBlock);
        view.klojview.delegateEvents();
        view.renderKloj();
        App.trigger('resize');

        view.$('.colorpicker').minicolors({
          position: 'top left',
          theme: 'bootstrap',
          letterCase: 'uppercase',
          change: function(hex, opacity){
            if (hex === '') return;
            // this.value = hex;
            view.theme.set('options.'+this.id, this.value);

            if (this.id == 'subtextcolor' && this.value != view.theme.get('options.headlinecolor')){
              view.$('#subtextinherit').prop('checked', false);
            }

            if (!view.subinheriting){
              $(this).focus();
            }else{
              view.subinheriting = false;
            }

            if (this.id == 'popupbg' || this.id == 'popupcolor' && !view.kloj.get('open')){
              var first = view.kloj.collection.models[0];
              first.set('open', true);
            }
          }
        });

        view.$saveBtn = view.$('#btn-save');
        view.$status = view.$('#edit-status');

        view.listenTo(view.theme, 'change:options.*', function(){
          if (view.rerender){
            view.clearKlojView();
            view.renderKloj();
            view.rerender = false;
          }else{
            view.renderTheme();
          }
        }, this);

      });

    	return this.el;

    },

    renderKloj: function(){
      this.klojview.render();
    },

    renderTheme: function(){
      this.klojview.setupTheme();
    },

    clickDone: function(e){
      if (this.klojview.selected){
        this.klojview.selected.set('open', false);
      }
      App.Router.navigate(App.root + App.klojRoot + this.kloj.id, {trigger: true});
    },

    clickSave: function(e){
      this.$status.html("<img src='/img/loading.gif'>");
      this.$saveBtn.addClass('disabled').text('Saving');

      //unset klojview to prevent circular json
      var klojview = this.kloj.get('view');
      this.kloj.unset('view');

      var save = this.kloj.save();

      this.kloj.set('view', klojview);

      if (!save){
        this.$status.html('Save failed');
        this.$saveBtn.removeClass('disabled');
        return;
      }

      var view = this;
      save.done(function(model, response, options){
        view.$saveBtn.text('Saved');
        view.$status.html('');
      });
      save.fail(function(jqxhr, response, options){
        view.$saveBtn.removeClass('disabled').text('Save');
        view.$status.html('Save failed');
        debugger;
      });


    },

    setTitle: function(e){
      this.kloj.set('title', e.target.value);
      this.formChange();
    },

    setTemplate: function(e){
      var template = this.templates.findWhere({id: e.target.value});
      this.theme.set('template', template);
      this.clearKlojView();
      this.renderTheme();
      this.renderKloj();
    },

    clearKlojView: function(){
      this.klojview.subview.close();
      this.klojview.subview = undefined;
      this.kloj.collection.invoke('set', {'el' : undefined, 'width' : undefined, 'content' : undefined});
      if (this.theme.get('template').get('file') == 'grid'){
        this.kloj.collection.invoke('set', {'filler' : undefined});
      }
    },

    formChange: function(){
      this.$saveBtn.removeClass('disabled').text('Save');
    },

    subtextInheritChange: function(e){
      if (e.target.checked){
        var headlinecolor = this.theme.get('options.headlinecolor');
        this.subinheriting = true;
        this.$('#subtextcolor').minicolors('value', headlinecolor);
      }
    },

    clickSlider: function(e){
      console.log(e.target);
      var $e = $(e.target);
      var id = $e.attr('id');
      $e.toggleClass('on off');

      if ($e.attr('data-render') !== undefined){
        this.rerender = true;
      }

      this.theme.set('options.'+id, $e.hasClass('on'));
    }

  });

  return EditView;
});
