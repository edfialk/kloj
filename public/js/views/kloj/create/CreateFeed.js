define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Feed = require('models/Feed'),
  		template = require('text!templates/kloj/create/feedsTemplate.html'),
      feedTemplate = require('text!templates/kloj/create/feed.html'),
      modalTemplate = require('text!templates/modalTemplate.html');

  require('goog!feeds,1');

  var PickFeedView = Backbone.View.extend({

    className: 'feed-page',

  	events: {
  		'click .selectable' : 'pick',
      'click .feed-btn' : 'go',
      'submit #form-feed' : 'addFeed'
  	},

  	initialize: function(options){
  		_.bindAll(this);
      this.fc = options.fc;
      var view = this;
      this.fc.deferred.done(function(){
        view.cats = view.fc.getCategories();
        view.cats.sort(function(a, b){
          return a.name > b.name;
        });
      });

      google.setOnLoadCallback(function(){
        console.log('google load!');
      });

      this.fc.on('add', this.render);

      this.fc.on('change:selected', function(feed){
        view.$('li[data-id="'+feed.id+'"]').toggleClass('selected', feed.get('selected'));
        view.cats.forEach(function(cat){
          var catid = cat.id;
          var catfeeds = [];
          var catselected = [];
          view.fc.models.forEach(function(feed){
            if (feed.get('category').id == catid){
              catfeeds.push(feed);
              if (feed.get('selected')){
                catselected.push(feed);
              }
            }
          });
          if (catselected.length === 0){
            view.$('.topic[data-id="'+catid+'"]').removeClass('selected');
          }else if (catselected.length == catfeeds.length){
            view.$('.topic[data-id="'+catid+'"]').addClass('selected');
          }
        });
      });

  	},

  	close: function(){
  		this.undelegateEvents();
  	},

    render: function(options){
      var view = this;
      this.$el.html("<div class='loader'></div>");
      this.fc.deferred.done(function(){
        var data = {
          User: App.User,
          categories: view.cats,
          fc: view.fc
        };
        view.$el.html(_.template(template, data));
        view.$gobtn = view.$('.feed-btn');
        view.$feeds = view.$('.feed-picker');
        view.$head = view.$('#pick-feed-head');
        view.$status = view.$('.status');

        view.$gobtn.toggleClass('disabled', view.fc.getBySelected().length === 0);
      });

      return this.el;
    },

    go: function(){
      if (this.fc.getBySelected().length === 0){
        this.$status.html("You have to choose a feed before continuing.");
        return;
      }
      this.trigger('pickFeed');
    },

  	pick: function(e){
      this.$status.html('');
  		var $t = $(e.currentTarget);
      var id = $t.attr('data-id');
      if ($t.hasClass('topic')){
        $t.toggleClass('selected');
        this.fc.models.forEach(function(feed){
          if (feed.get('category').id == id){
            feed.set('selected', $t.hasClass('selected'));
          }
        });
      }else{
        this.fc.get(id).toggle();
      }

      this.$gobtn.toggleClass('disabled', this.fc.getBySelected().length === 0);

  	},

    addFeed: function(e){
      e.preventDefault();

      var view = this,
          $button = this.$('#feed-add').addClass('disabled'),
          $status = this.$('#feed-status').html('').show().append('<div class="loader"></div>'),
          $textfield = this.$('#feed-custom'),
          val = $textfield.val(),
          type = 'name';
/*
      // so I want the ability to type in name of website (i.e. BBC) and possibly figure out RSS from name
      // but I'll be getting to that later. remember to change input type='url' on template
      if (val.indexOf('http') != -1){
        type = 'url';
      }
*/

      var present = this.fc.findWhere({url: val});
      if (!present && val.indexOf('www') !== -1){
        present = this.fc.findWhere({url: val.replace('www.', '')});
      }else if (!present && val.indexOf('www') === -1){
        present = this.fc.findWhere({url: val.replace('://', '://www.')});
      }

      if (present){
        $textfield.val('');
        present.set('selected', true);
        $status.html("We had that feed, it has been selected for you.");
        $button.removeClass('disabled');
        window.setTimeout(function(){
          $status.fadeOut();
        }, 3000);
        return;
      }

      if (!view.getCategories){
        view.getCategories = App.getCategories();
      }

      var gfeed = new google.feeds.Feed(val);
      var feed = new Feed();
      gfeed.load(function(result){
        $button.removeClass('disabled');
        // console.log('feed result: %O', result);
        if (!result.error){
          feed.set('url', result.feed.feedUrl);
          feed.set('title', result.feed.title);
          feed.set('link', result.feed.link);
          feed.set('description', result.feed.description);

          view.getCategories.done(function(data){
            var body = "<select>";
            data.forEach(function(cat){
              body += "<option value='"+cat.id+"'>"+cat.name+"</option>";
            });
            body += "</select>";
            var $modal = $(_.template(modalTemplate, {
              title: "We couldn't decide on a category for this feed. Can you pick one?",
              body: body,
              saveText: 'Save',
              showCancel: true
            }));
            $('body').append($modal);
            $modal.modal();
            $('#save', $modal).click(function(){
              var catId = $('select', $modal).val();
              var cat = _.findWhere(data, {id: catId});
              feed.set('category', cat);
              $modal.modal('hide');
              feed.save([], {
                success: function(model, response, options){
                  if (response.status == 'error'){
                    $status.html(response.message);
                  }else{
                    feed.set('selected', true);
                    view.fc.add(feed);
                  }
                },
                error: function(model, response, options){
                  $status.html("Error saving feed: "+response.responseText);
                }
              });
            });
          });

        }else if (result.error.message){
          $status.html(result.error.message);
        }else{
          console.log('gfeed problem, %O', result);
        }
      });
    }

  });
  return PickFeedView;
});