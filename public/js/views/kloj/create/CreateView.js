define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Kloj = require('models/Kloj'),
      Theme = require('models/Theme'),
      Feeds = require('collections/Feeds'),
      Themes = require('collections/Themes'),
      Templates = require('collections/Templates'),
  		template = require('text!templates/kloj/create/template.html'),
      ThemeView = require('views/kloj/create/CreateTheme'),
      FeedView = require('views/kloj/create/CreateFeed'),
      Preview = require('views/kloj/create/Preview');

  var CreateView = Backbone.View.extend({

    id: 'create',

  	initialize: function(options){
  		_.bindAll(this);
      this.model = new Kloj();

      this.theme = new Theme({id: 1});
      this.theme.deferred = this.theme.fetch();
      // this.theme.deferred.fail(function(){
      // });
      this.model.set('theme', this.theme);

      this.feeds = new Feeds();
      this.feeds.deferred = this.feeds.fetch();

      this.templates = new Templates();
      this.templates.deferred = this.templates.fetch();

      this.schemes = new Themes({defaults: true});
      this.schemes.deferred = this.schemes.fetch();

      if (App.User.isLoggedIn()){
        this.model.set('userid', App.User.id);
        this.theme.set('userid', App.User.id);
      }

  	},

  	close: function(){
  		this.stopListening();
  		this.undelegateEvents();
      if (this.preview) this.preview.close();
  	},

    render: function(){
      App.trigger('sidebar:select', 'create-tab');
      this.$el.html(template);
      this.$page = this.$('.create-subpage');
      this.renderFeedPage();
      App.renderNav('create');
      return this.el;
    },

    // renderThemePage: function(){
    //   if (this.themeView){
    //     this.$page.html(this.themeView.el);
    //     this.themeView.delegateEvents();
    //     return;
    //   }

    //   this.themeView = new ThemeView({templates: this.templates});
    //   this.$page.html(this.themeView.render());
    //   this.listenTo(this.themeView, 'pickTheme', this.setTheme);
    // },

    renderFeedPage: function(){
      // App.Router.navigate('create/feeds');

      if (this.feedView){
        this.$page.html(this.feedView.el);
        this.feedView.delegateEvents();
        return;
      }

      this.feedView = new FeedView({fc: this.feeds});
      this.$page.html(this.feedView.render());
      this.listenTo(this.feedView, 'pickFeed', this.setFeeds);
    },

    renderPreview: function(){
      // App.Router.navigate('create/finished');

      this.$('.create-header').hide();

      if (this.preview){
        this.$page.html(this.preview.el);
        this.preview.delegateEvents();
        return;
      }

      this.preview = new Preview({
        model: this.model,
        schemes: this.schemes,
        templates: this.templates
      });
      this.$page.html(this.preview.render());
    },

    setTheme: function(template){
      this.renderFeedPage();
      this.model.get('theme').set('template', template);
    },

    setFeeds: function(){
      this.model.set('feeds', this.feeds.getBySelected());
      var view = this;
      var $status = this.$('.status').html('<img src="img/loading.gif">');
      this.model.save().done(function(){
        view.renderPreview();
      });
    }

  });
  return CreateView;
});
