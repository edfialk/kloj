define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Templates = require('collections/Templates'),
  		template = require('text!templates/kloj/create/themeTemplate.html');

  var CreateTheme = Backbone.View.extend({

    className: 'theme-page',

  	events: {
  		'click .template' : 'pick'
  	},

  	initialize: function(options){
  		_.bindAll(this);
      this.templates = options.templates;
  	},

  	pick: function(e){
  		var $t = $(e.currentTarget);
  		var id = $t.data('id');
  		var template = this.templates.get(id);
  		this.trigger('pickTheme', template);
  	},

  	render: function(){
      var view = this;
      this.$el.html(template);
      this.templates.deferred.done(function(){
        view.renderThemes();
      });

  		return this.el;
  	},

    renderThemes: function(){
      var $ul = this.$('.theme-picker ul').detach();
      this.templates.models.forEach(function(template){
        var $li = $('<li class="template"/>').data('id', template.id);
        $('<img/>').attr('src', 'img/templates/'+template.id+'.png').appendTo($li);
        $li.appendTo($ul);
      });
      this.$('.theme-picker').append($ul);
    }

  });
  return CreateTheme;
});
