define(function(require, exports, module){
  "use strict";
  var App = require('app'),
  		template = require('text!templates/kloj/create/previewTemplate.html'),
      KlojView = require('views/kloj/KlojView'),
      Minicolors = require('minicolors');

  var PreviewView = Backbone.View.extend({

    className: 'preview-page',

    events: {
      'change #name' : 'changeName',
      'click #btn-save' : 'clickSave',
      'click #btn-done' : 'clickDone',
      'submit .form-signup' : 'signUp',
      'click .toggle-signin' : 'toggleSignin',
      'change #template' : 'changeTemplate',
      'change #scheme' : 'changeScheme',
      'change #density' : 'changeDensity'
    },

  	initialize: function(options){
  		_.bindAll(this);
      this.model = options.model;
      this.theme = this.model.get('theme');
      this.schemes = options.schemes;
      this.templates = options.templates;
  	},

  	close: function(){
      if (this.klojview){
        this.klojview.close();
      }
  	},

  	render: function(options){

      var view = this;
      var data = {
        theme: this.theme,
        kloj: this.model,
        User: App.User
      };
      this.$el.html(_.template(template, data));
      this.$preview = this.$('#klojpreview');
      this.$status = this.$('#status-save');
      this.$scheme = this.$('#scheme');
      this.$density = this.$('#density');
      this.$template = this.$('#template');

      this.klojview = new KlojView({
        model: this.model
      });
      this.renderPreview();

      this.renderFonts();
      this.renderColorPicker();

      this.templates.deferred.done(this.renderTemplates);
      this.schemes.deferred.done(this.renderSchemes);

      // this.$('.toggle-signin').click(this.toggleSignin);

  		return this.el;
  	},

    clearPreview: function(){
      delete this.klojview.subview;
    },

    renderPreview: function(){
      this.$preview.html(this.klojview.render());
    },

    renderFonts: function(){

      window.WebFontConfig = {
        google: { families: [ 'Arvo::latin', 'Droid+Sans::latin', 'Lato::latin', 'Lora::latin', 'Maven+Pro::latin', 'Merriweather::latin', 'Open+Sans::latin', 'Oswald::latin', 'PT+Sans::latin', 'Raleway::latin', 'Roboto::latin', 'Rokkitt::latin', 'Ubuntu::latin', 'Yanone+Kaffeesatz::latin'  ] }
      };

      var wf = document.createElement('script');
      wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.class = 'webfonts';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);

      var view = this;
      var fontSelect = this.$(".font-picker");
      $(WebFontConfig.google.families).each(function(i, family){
        var label = family.replace("::latin", "");
        label = label.replace("+", " ");
        var option = $("<label class='radio'/>");
        option.css({
          'font-size' : '18px',
          'font-family' : label
        });
        option.text(label);
        var radio = $("<input type='radio' name='font' data-model='theme' data-id='font' value='"+label+"'>").appendTo(option);
        if (label == view.theme.get('headlinefont')){
          radio.attr('checked', true);
        }
        fontSelect.append(option);
      });

      fontSelect.find('input').change(function(){
        view.theme.set('headlinefont', this.value);
        view.theme.set('textfont', this.value);
        view.theme.set('subtextfont', this.value);
      });
    },

    renderColorPicker: function(){
      this.$(".minicolor").each(function(input){
        var $in = $(this), key = $in.attr('id'), defaultVal = view.theme.get(key);
        $in.minicolors({
          theme: 'bootstrap',
          defaultValue: defaultVal,
          letterCase: 'uppercase',
          change: function(hex, opacity){
            view.showStatus();
            if (key == 'color'){
              view.theme.set('headlinecolor', hex);
              view.theme.set('textcolor', hex);
              view.theme.set('popupcolor', hex);
            }else{
              view.theme.set(key, hex);
            }
          }
        });
      });
    },

    renderTemplates: function(){
      var view = this;
      this.templates.models.forEach(function(template){
        var $opt = $('<option value="'+template.id+'">'+template.get('name')+'</option>').appendTo(view.$template);
        if (template.id = view.theme.get('template_id')){
          $opt.prop('selected', true);
        }
      });
    },

    renderSchemes: function(){
      var view = this;
      this.schemes.models.forEach(function(theme){
        if (theme.get('template_id') == view.theme.get('template_id')){
          $('<option value="' + theme.id + '">'+theme.id+'</option>').appendTo(view.$scheme);
        }
      });
    },

    toggleSignin: function(e){
      e.preventDefault();
      e.stopPropagation();
      this.$(".signup").toggle();
      this.$(".signin").toggle();
    },

    changeName: function(e){
      //validate?
      this.model.set('title', e.target.value);
      this.showStatus();
    },

    showStatus: function(){
      if (this.$status.is('hidden')){
        this.$status.removeClass('alert-success alert-danger').addClass('alert-info');
        this.$status.text('Remember to save after editing!');
        this.$status.show();
      }
    },

    clickSave: function(e){
      e.preventDefault();
      var $btn = $(e.target).addClass('disabled').text('');
      var $loading = $("<img src='img/gif-load.gif' style='margin-left: 20px;'>").appendTo(e.target);
      var view = this;
      var save = this.model.save([], {
        success: function(model, response, options){
          view.$status.removeClass('alert-info alert-danger').addClass('alert-success').text('Changes have been saved.');
          $(e.target).removeClass('disabled').text('Save!');
          $loading.remove();
          setTimeout(function(){
            view.$status.fadeOut();
          }, 2000);
          view.renderPreview();
        },
        error: function(){
          $btn.removeClass('disabled').text('Save!');
          $loading.remove();
          view.$status.removeClass('alert-info').addClass('alert-danger').text('Oops! There was an error. Please try again.');
        }
      });
    },

    signUp: function(e){
      e.preventDefault();

      var view = this,
          $btn = this.$('.form-signup button').addClass('disabled'),
          $status = this.$('#status').html("<img src='imgs/loading.gif'>"),
          name = this.$('#username').val().trim(),
          email = this.$('#email').val().trim(),
          pass = this.$('#password').val().trim();

      App.User.register(name, email, pass).done(function(data){
        $btn.removeClass('disabled');
        $status.html('');
        if (data.status == 'error'){
          if (data.message.indexOf("Username") != -1){
            $status.html("Username is taken.");
            $("#username").focus();
            return;
          }
          if (data.message.indexOf("Password") != -1){
            $status.html("Invalid password.");
            $("#password").focus();
            return;
          }
          if (data.message.indexOf("Email") != -1){
            $status.html("Email address is already registered.");
            $("#email").focus();
            return;
          }
          return;
        }else{
          $("#username").val('');
          $("#email").val('');
          $("#username").val('');
          App.User.login(name, pass, function(){
            App.Router.navigate('edit/'+view.model.id, {trigger: true});
          });
        }
      }).fail(function(jqxhr, textstatus, error){
        $btn.removeClass('disabled');
        $status.html(textstatus);
      });
    },

    signIn: function(e){
      e.preventDefault();
      var view = this,
          $btn = this.$('.form-signin button').addClass('disabled'),
          $status = this.$('#status'),
          $user = this.$('#username'),
          username = $user.val().trim(),
          $pass = this.$('#password'),
          password = $pass.val().trim();

      App.User.login(username, password).done(function(data){
        $btn.removeClass('disabled');
        $status.html('');
        if (data.status == 'error'){
          $status.html("Invalid username or password");
          $user.focus();
        }else{
          App.Router.navigate('edit/'+view.model.id, {trigger: true});
        }
      });
    },

    clickDone: function(e){
      e.preventDefault();
      App.Router.navigate(this.model.getURL(), {trigger: true});
    },

    changeScheme: function(e){
      var val = this.$schema.val();
      this.model.set('theme', this.schemas.get(val));
      this.renderPreview();
    },

    changeTemplate: function(e){
      var val = this.$template.val();
      var template = this.templates.findWhere({id: val});
      this.theme.set('template', template);
      this.clearPreview();
      this.renderPreview();
    },

    changeDensity: function(e){
      var val = this.$density.val();
    }

  });
  return PreviewView;
});