define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      PostView = require('views/kloj/PostView'),
      template = require('text!templates/kloj/listTemplate.html');

  var ListView = Backbone.View.extend({

    className: 'list',

    events: {
      'click .post-header' : 'clickHeader'
    },

    initialize: function(options) {
      _.bindAll(this);
      if (!options){
        console.error('list KlojView requires options');
      }
      this.parent = options.parent;
      this.kloj = options.model;
      this.theme = this.kloj.get('theme');
      this.collection = this.kloj.collection;
      this.selected = null;

      this.listenTo(App, 'hidePost', this.hidePost);
    },

    close: function(){
      this.stopListening();
      // this.trigger('closeview');
    },

    render: function(){
      this.counter = 1;
      this.setupTheme();
      var view = this;
      this.collection.deferred.done(function(){
        if (view.collection.models.length === 0){
          var template = require('text!templates/kloj/emptyTemplate.html');
          view.$el.html(template);
          return;
        }
        view.$el.html('');
        view.append(view.collection.models);
      });

      return this.el;
    },

    append: function(posts){

      if (posts.length === 0){
        this.$(".progress").remove();
        return;
      }

      var view = this;
      posts.forEach(function(post){
        if (post.get('el')){
          view.$el.append(post.get('el'));
          post.get('view').delegateEvents();
        }else{
          view.listenTo(post, 'change:open', view.openListener);
          var postview = new PostView({
            model: post,
            theme: view.theme,
            template: template,
            counter: view.counter++,
            parent: this
          });
          view.$el.append(postview.render());
        }
      });

    },
    setupTheme: function(){
      if (this.theme.get('options.density') == 'compressed'){
        this.$el.addClass('compressed');
      }
      if (this.theme.get('options.subtextpos') == 'right'){
        this.$el.addClass('subtextright');
      }
    },

    openListener: function(post, options){
      if (post.get('open')){
        this.openPost(post);
      }else{
        this.closePost();
      }
    },

    openPost: function(post){
      console.log('list open post: %O', post);
      if (post === null){
        throw new Error('Cannot open null post!');
      }
      if (this.selected !== null){
        this.selected.set('open', false);
      }
      this.selected = post;
      this.kloj.set('open', post);

      var postView = post.get('view');
      if (!post.get('content')){
        postView.renderContent();
      }
      var $el = postView.$el;
      var $ex = $el.find('.extract');
      if ($ex.html() === ''){
        $ex.html( post.get('content') );
      }
      $el.addClass('selected popupbg popupcolor');
      $el.find('.post-header .headlinecolor').toggleClass('headlinecolor popupcolor');
      $el.find('.post-subtext').toggleClass('subtextcolor popupcolor');

      // $('#content').scrollTop($el.position().top);
      App.$body.scrollTop($el.position().top);
    },

    closePost: function(){
      this.kloj.set('open', false);

      if (this.selected === null) return;

      // this.selected.get('view').stopVideo();

      var $el = this.selected.get('el');
      $el.find('.post-header .popupcolor').toggleClass('headlinecolor popupcolor');
      $el.find('.post-subtext').toggleClass('subtextcolor popupcolor');
      $el.removeClass('selected popupbg popupcolor');

      this.selected.set('open', false);
      this.selected = null;

    },

    hidePost: function(post){
      if (this.selected !== null && this.selected.get('id') == post.get('id')){ //post is open
        App.trigger('closePost');
      }
      this.collection.remove(post);
      var div = this.$(".post[data-id='"+post.get('id')+"']");
      div.slideDown(400, function(){
        div.remove();
      });
    },

    clickHeader: function(e){
      console.log('list js click header');
      if (this.selected !== null){
        e.stopPropagation();
        this.selected.set('open', false);
      }
    },

    getMaxHeight: function(post){
      var maxheight = 0;
      _.each(post.get('images'), function(image, index){
        if (image.height > maxheight){
          maxheight = image.height;
        }
      });
      // var windowheight = $('#content').height() - $('#kloj-fixed-header').height() - 20;
      var windowheight = App.$el.height() - $('#kloj-fixed-header').height() - 20;
      if (maxheight > windowheight){
        maxheight = windowheight;
      }
      return maxheight;
    },

/*    getHeadlineActions: function(post){
      var headlineActions = $("<div/>", {class: 'headlineActions smaller'}),
          view = this;

      $("<i class='fa fa-times'/>").click(function(e){
        e.stopPropagation();
        App.trigger('closePost');
      }).appendTo(headlineActions);

      $("<i class='fa fa-star'/>").click(function(e){
        e.stopPropagation();
        view.parent.bookmark(post);
      }).appendTo(headlineActions);

      headlineActions.find("i").hover(function(){
        $(this).toggleClass("fa-white");
      });

      return headlineActions;
    }*/

  });
  return ListView;
});
