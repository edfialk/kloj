define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      PostView = require('views/kloj/PostView'),
      lazyload = require('lazyload'),
      // unveil = require('unveil'),
      // echo = require('echo'),
      template = require('text!templates/kloj/gridTemplate.html');

  var GridView = Backbone.View.extend({

    className: 'grid',

    initialize: function(options) {
      _.bindAll(this);
      if (!options){
        console.error('grid KlojView requires options');
        return;
      }

      this.parent = options.parent;
      this.kloj = options.model;
      this.theme = this.kloj.get('theme');
      this.collection = this.kloj.collection;

      this.postHeight = 200;
      this.selected = null;

      var view = this;
      this.resize = _.debounce(function(){
        view.row = 0;
        view.leftover = 0;
        view.kloj.set('width', view.$el.width());
        view.build(view.collection.models);
      }, 250);
      $(window).resize(this.resize);
      App.on('resize', this.resize);

    },

    close: function(){
      App.off('resize', this.resize);
      $(window).off('resize', this.resize);
      this.stopListening();
    },

    render: function(){
      this.$container = App.$el;
      this.setupTheme();

      //reset
      // this.row = 0;
      // this.leftover = 0;
      // this.collection.invoke('set', {'el' : undefined});
      // this.collection.invoke('set', {'popup' : undefined});

      var view = this;
      this.collection.deferred.done(function(){
        if (view.collection.models.length === 0){
          var template = require('text!templates/kloj/emptyTemplate.html');
          view.$el.html(template);
          return;
        }
        view.build(view.collection.models);
        view.parent.scrollListener();
        var selected = view.collection.findWhere({open: true});
        if (selected){
          window.setTimeout(function(){
            view.openPost(selected);
            view.parent.openPost(selected);
          }, 0);
        }
      });

      return this.el;
    },

    build: function(posts){

      if (!this.kloj.get('width')){
        this.kloj.set('width', $(document).width() - App.getScrollbarWidth());
      }

      // -1 is IE fix
      this.containerWidth = this.kloj.get('width') - 1;
/*      if (App.$el.get(0).scrollHeight <= App.$el.get(0).clientHeight){
        //no scrollbar yet, need to make room
        this.containerWidth -= this.getScrollbarWidth();
        this.kloj.set('width', this.containerWidth);
      }*/

      var items = posts.slice(),
          rows = [];

      //calculate rows of images with each row fitting into specified containerwidth
      while(items.length > 0) {
        if (this.leftover > 100){ //minimum width for leftover so don't smush image -> maybe turn into ad space
          rows.push(this.buildImageRow(this.leftover, items));
        }else{
          this.row++;
          rows.push(this.buildImageRow(this.containerWidth, items));
        }
      }

      for (var r in rows){
        for (var i in rows[r]){
          var post = rows[r][i];
          if (post.get("el")) {
            //image is already on screen
            this.updateImageElement(post);
          } else {
            //create this image
            this.createImageElement(post);
          }
        }
      }

      // this.$('img').lazyload({
      // })

    },

    append: function(posts){
      this.build(posts);
      $(".progress").remove();
    },

    setupTheme: function(){
      // var view = this;
    },

    openListener: function(post, options){
      if (post.get('open')){
        this.openPost(post);
      }else{
        this.closePost();
      }
    },

    openPost: function(post){
      console.log('grid open post: %O', post);
      if (post === null){
        throw new Error('Cannot open null post!');
      }
      if (this.selected !== null){
        this.selected.set('open', false);
      }
      this.selected = post;
      this.kloj.set('open', post);

      if (!post.get('content')){
        post.get('view').renderContent();
      }

      //get last item in row
      var $el = post.get('el'),
          $last,
          found = false,
          $test = $el;

      while(!found){
        var pos = $test.offset();
        var $next = $test.next();
        var nextPos = $test.next().offset();
        if ($next.length === 0 || nextPos.left < pos.left){
          $last = $test;
          found = true;
        }
        $test = $test.next();
      }

      var filler;
      if (post.get('filler')){
        filler = post.get('filler');
      }else{
        filler = document.createElement('div');
        filler.setAttribute('class', 'fill popupbg');
        var arrowLeft = $el.position().left + ( post.get('vwidth') / 2 - 10 );
        var arrowColor = this.kloj.get('theme').get('options.popupbg') || '#222';
        filler.innerHTML = '<div class="arrow" style="left: '+arrowLeft+'px; border-bottom-color: '+arrowColor+';"></div>';
        var $container = $('<div class="container"/>').appendTo(filler);
        var $content = post.get('content').appendTo($container);
        var $img = $('.post-image', $content);
        if ($img.length > 0 && !$img.hasClass('loaded')){
          $img.hide();
          var prediv = document.createElement('div'),
            preimage = document.createElement('img'),
            image = post.get('images')[0];
          prediv.setAttribute('class', 'preload');
          preimage.setAttribute('src', post.getThumbnailByHeight(this.postHeight));
          preimage.setAttribute('style', 'width:'+image.width+'px;');
          prediv.appendChild(preimage);
          $img.after(prediv);

          $img.load(function(){
            $img.show().addClass('loaded');
            $(prediv).remove();
          });
        }
        post.set('filler', filler);
      }

      $last.after(filler);
      this.$filler = $(filler);
      // this.$container.scrollTop(this.$filler.position().top);
      // this.$el.scrollTop(this.$filler.position().top);
      App.$doc.scrollTop(this.$filler.position().top);

/*
      //so google does this absolute positioned element inside the "filler".
      //not sure why, maybe easier to move to next open post.
      var content = document.createElement('div');
      content.setAttribute('class', 'k_expand popupbg');
      content.innerHTML = post.get('view').renderContent();
      var contentTop = this.$filler.position().top;
      content.setAttribute('style', 'top: '+contentTop+'px;');
      this.$selected = $(content);
      this.$el.append(this.$selected);
*/
    },

    closePost: function(){
      this.kloj.set('open', false);
      if (this.selected !== null){
        this.selected.set('open', false);
        this.selected = null;
      }
      if (this.$filler){
        this.$filler.detach();
        this.$filler = null;
      }
    },

    buildImageRow : function(maxwidth, posts){
      var row = [], len = 0, marginsOfImage = 6, item;

      while(posts.length > 0 && len < maxwidth){
        var post = posts.shift();
        if (!post.hasImage()){
          post.set('images', [{url: '/img/logo.png', ratio: 1.53}]);
          post.set('defaultImage', true);
        }
        row.push(post);

        var width = Math.round(parseFloat(post.getRatio()) * this.postHeight);
        post.set("width", width);
        len += (post.get("width") + marginsOfImage);
      }

      for (var i in row){
        row[i].set("row", this.row);
      }

      var delta = len - maxwidth;
      if (row.length > 0 && delta > 0){
        //if the line is too long, make images smaller
        var cutoff = this.calculateCutOff(len, delta, row);
        for (i in row){
          var pixelsToRemove = cutoff[i];
          item = row[i];
          //move the left border inwards by half the pixels
          item.set("vx", Math.floor(pixelsToRemove / 2));
          item.set("vwidth", parseInt(item.get("width")) - (pixelsToRemove));
         }
         this.leftover = 0;
      } else {
        //all images do fit in the row
        for (i in row){
          item = row[i];
          item.set("vx", 0);
          item.set("vwidth", item.get("width"));
        }
        this.leftover = maxwidth - len;
      }
      return row;
    },

    /**
    * Creates a new thumbnail in the image area.
    */
    createImageElement:  function(post){
      var postView = new PostView({
        model: post,
        theme: this.theme,
        template: template,
        height: this.postHeight,
        width: Math.round(this.postHeight * post.getRatio()),
        parent: this
      });

      postView.render();
      var $el = postView.$el;
      $el.css('width', post.get('vwidth'));
      var $img = $el.find('img').css({
        'margin-left': '-' + post.get('vx') + 'px'
      });

      this.listenTo(post, 'change:open', this.openListener);

      this.$el.append($el);

      // $img.lazyload({
      //   container: this.$container,
      //   threshold: 200
      // });
    },

    /**
    * Update an existing thumbnail in the image area
    */
    updateImageElement : function(post){
      var overflow = post.get("el");
      var img = overflow.find("img:first");

      overflow.animate({
        width: post.get('vwidth')
      }, 500);
      img.animate({
        'margin-left': post.get('vx') ? -post.get('vx') : 0
      }, 500);
      post.get('view').delegateEvents();
    },

  /**
   * Distribute a delta (integer value) to n items based on
   * the size (width) of the items thumbnails.
   * @author Florian Maul (edited)
   * @method calculateCutOff
   * @property len the sum of the width of all thumbnails
   * @property delta the delta (integer number) to be distributed
   * @property posts an array with items of one row
   */
    calculateCutOff: function(len, delta, posts){

      var cutoff = [], cutsum = 0;

      for (var i in posts){
        var post = posts[i];
        if (post.get("width") === undefined){
          if (post.getRatio() === 0){
            //i dunno, something
          }else{
            width = Math.round(post.getRatio() * this.postHeight);
            post.set("width", width);
          }
        }
        var fractOfLen = post.get("width") / len;
        cutoff[i] = Math.floor(fractOfLen * delta);
        cutsum += cutoff[i];
      }

      var stillToCutOff = delta - cutsum;
      while(stillToCutOff > 0){
        for (i in cutoff){
          cutoff[i]++;
          stillToCutOff--;
          if (stillToCutOff === 0) break;
        }
      }
      return cutoff;
    }

  });

  return GridView;
});

