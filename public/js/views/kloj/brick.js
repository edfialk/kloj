define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      PostView = require('views/kloj/PostView'),
      Masonry = require('masonry'),
      template = require('text!templates/kloj/brickTemplate.html'),
      overlayTemplate = require('text!templates/overlayTemplate.html');

  var BrickView = Backbone.View.extend({

    className: 'brick',

    initialize: function(options) {
      _.bindAll(this);
      if (!options){
        console.error('brick KlojView requires options');
      }

      this.parent = options.parent;
      this.kloj = options.model;
      this.theme = this.kloj.get('theme');
      this.collection = this.kloj.collection;
      this.columnWidth = 200;
      this.selected = null;
    },

    close: function(){
      this.stopListening();
      if (this.$overlay){
        App.$body.removeClass('overlayed');
        this.$overlay.remove();
      }
    },

    render: function(){
      this.$container = App.view.$el;
      this.setupTheme();

      var view = this;

      this.collection.deferred.done(function(){

        if (view.collection.models.length === 0){
          var template = require('text!templates/kloj/emptyTemplate.html');
          view.$el.html(template);
          return;
        }

        require(['jquery-bridget/jquery.bridget'], function(){

          view.$el.html('');

          $.bridget( 'masonry', Masonry );
          view.$el.masonry({
            columnWidth: view.columnWidth,
            itemSelector: '.post',
            gutter: 10
          });

          view.append(view.collection.models);

          var selected = view.collection.findWhere({open: true});
          if (selected){
            window.setTimeout(function(){
              view.openPost(selected);
              view.parent.openPost(selected);
            }, 0);
          }

        });

      });

      return this.el;
    },

    append: function(posts){
      var view = this;
      posts.forEach(function(post){
        post.set('width', 200); //if ('featured', etc.)
        var ratio = post.getRatio();
        post.set('height', Math.round(200 / ratio) );
        var postview = new PostView({
          model: post,
          theme: view.theme,
          template: template,
          parent: this
        });

        view.$el.append(postview.render()).masonry('appended', post.get('el'));

        view.listenTo(post, 'change:open', view.openListener);

      });
    },

    setupTheme: function(){
      // var view = this;
    },

    openListener: function(post, options){
      if (post.get('open')){
        this.openPost(post);
      }else{
        this.closePost();
      }
    },

    openPost: function(post){
      var view = this;
      console.log('brick open post: %O', post);
      if (post === null){
        throw new Error('Cannot open null post!');
      }
      if (this.selected !== null){
        if (this.selected.id == post.id) return; //ugh i know, but if an event doublefires (IT SHOULDNT) Ima nip that.
        this.selected.set('open', false);
      }
      this.selected = post;
      this.kloj.set('open', post);

      var $el = post.get('el');
      if (!post.get('content')){
        post.get('view').renderContent();
      }
      var overlay = _.template(overlayTemplate, {content: ''});
      this.$overlay = $(overlay);
      App.$body.addClass('overlayed');
      this.$el.append(this.$overlay);

      var $content = post.get('content');
      var $img = $('.post-image', $content);
      if ($img.length > 0 && !$img.hasClass('loaded')){
        $img.hide();
        var prediv = document.createElement('div'),
          preimage = document.createElement('img'),
          image = post.get('images')[0];
        prediv.setAttribute('class', 'preload');
        preimage.setAttribute('src', $el.find('img').attr('src'));
        preimage.setAttribute('style', 'width:'+image.width+'px;');
        prediv.appendChild(preimage);
        $img.after(prediv);

        $img.load(function(){
          $(prediv).remove();
          $img.show().addClass('loaded');
        });
      }

      $('.contentWrap', this.$overlay).append($content);
      $('.screenWrap', this.$overlay).click(function(e){
        if (e.target == this){
          view.closePost();
        }
      });
    },

    closePost: function(){
      this.kloj.set('open', false);
      if (this.selected !== null){
        this.selected.set('open', false);
        this.selected = null;
      }
      App.$body.removeClass('overlayed');
      this.$overlay.remove();
    },



  });

  return BrickView;
});

