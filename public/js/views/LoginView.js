define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      // template = require('text!templates/account/loginTemplate.html');
      template = require('text!templates/loginTemplate.html');

  var LoginView = Backbone.View.extend({

    title: 'Sign in',

    className: 'signup-page',

    events: {
      'submit .form' : 'submit',
      'click #forgot a' : 'forgot'
    },

    render: function(){
      if (App.User.isLoggedIn()){
        App.Router.navigate( "", {trigger:true} );
        return;
      }

      this.$el.html(_.template(template));
      this.$('#username').ready(function(){
        $(this).focus();
      });
      return this.el;
    },

    submit: function(event){

        event.preventDefault();

        var view = this,
            $btn = this.$('button').addClass('disabled'),
            $status = this.$("#status").html("<img src='img/gif-load.gif'>"),
            $u = this.$('#username'),
            u = $u.val().trim(),
            $p = this.$('#password'),
            p = $p.val().trim();

        App.User.login(u, p).done(function(data){
          $status.html('');
          $btn.removeClass('disabled');
          if (data.status == 'error'){
            $status.html(data.message);
            $u.focus();
            return;
          }

          // App.Router.navigate( 'home', {trigger:true} );
          App.view.refresh();
        });
    },

    forgot: function(e){
      e.preventDefault();
    }

  });

  return LoginView;
});