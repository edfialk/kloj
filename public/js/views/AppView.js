define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      // SidebarView = require('views/SidebarView'),
      template = require('text!templates/appTemplate.html'),
      navTemplate = require('text!templates/navTemplate.html');

  var AppView = Backbone.View.extend({
    // id: 'app',
    template: _.template(template),

  	initialize: function(options){
      _.bindAll(this);
      this.currentView = null;
  	},

  	render: function(){
      this.$el.html(template);
      return this.el;
  	},

    renderView: function(view){
      if (this.currentView && this.currentView.close){
        this.currentView.close();
      }
      this.currentView = view;
      if (view.waitForTrigger){
        view.render();
        var self = this;
        view.on('render', function(){
          self.$el.html(view.$el);
        });
      }else{
        this.$el.html(view.render());
      }
    },

    renderNav: function(page){
      var view = this;
      App.User.deferred.done(function(){
        var nav = _.template(navTemplate, {User: App.User});
        view.currentView.$el.prepend(nav);
        $('#'+page+'_tab', view.currentView.$el).addClass('active');
      });
    },

    refresh: function(){
      this.currentView.render();
    },

  	close: function(){
      this.stopListening();
      this.undelegateEvents();
  	}

  });

  return AppView;
});