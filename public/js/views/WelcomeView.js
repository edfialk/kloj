define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      template = require('text!templates/welcome.blade.php');

  var WelcomeView = Backbone.View.extend({

    title: 'Kloj',

    className: 'h100',

    initialize: function(){
      _.bindAll(this);
      App.cache.home = this;
    },

    render: function(){
      this.$el.html(template);
      return this.el;
    },


  });

  return WelcomeView;
});