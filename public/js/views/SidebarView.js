define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      template = require('text!templates/sidebarTemplate.html');

  var SidebarView = Backbone.View.extend({

    activeId: 'homepage',
    open: false,

    events: {
      'click .folder > .title' : 'clickFolderTitle',
      'click .toggle' : 'clickToggle',
      'click li.page' : 'clickPage',
      'click .logout' : 'clickLogout'
    },

    initialize: function(options) {
      _.bindAll(this);
      this.listenTo(App, 'kloj:sub', this.addSub);
      this.listenTo(App, 'kloj:unsub', this.delSub);
      this.listenTo(App, 'sidebar:select', this.setActive);
      this.listenTo(App.User, 'login', this.render);
      this.listenTo(App.User, 'logout', this.render);
    },

    render: function() {
      //change this to if klojlist is open, remove guest - which should be called 'thin' or something
/*      if (App.User.isLoggedIn()){
        this.$el.removeClass('guest');
      }else{
        this.$el.addClass('guest');
      }*/

      var view = this;
      var data = { activeId: this.activeId, User: App.User, App: App };
      this.$el.html(_.template(template, data));
      // this.$('.sidebar-content').TrackpadScrollEmulator();
      this.$toggleIcon = this.$('.toggle i');
      this.$subs = this.$('#subfolder');

      this.open = $(window).width() >= 767;
      if (this.open){
        this.$el.addClass('open');
      }

      this.$('i').tooltip({
        placement: 'right',
        container: 'body'
      }).on('show.bs.tooltip', function(){
        if (view.$el.hasClass('open')){
          return false;
        }
      });

      var $w = $(window);
      this.size = $w.width();
      $w.resize(_.debounce(function(){
        if (view.size > 767 && $w.width() < 767) view.hideSide() //from wide to small
        if (view.size < 767 && $w.width() > 767) view.showSide() //from small to wide
        view.size = $w.width();
      }, 500));

      // this.delegateEvents(); //do this because view init was before el was created
      return this.el;
    },

    setActive: function(id, isKloj){
      this.activeId = id;
      this.$('.active').removeClass('active');
      if (id == null) return;
      if (isKloj){
        this.$('li[data-id="'+id+'"]', '#klojfolder').addClass('active');
      }else{
        this.$('#'+id).addClass('active');
      }
    },

    clickToggle: function(e){
      this.$el.toggleClass('open');
      this.open = !this.open;
      App.trigger('resize');
    },
    showSide: function(){
      if (this.open) return;
      this.$el.addClass('open');
      this.open = true;
      App.trigger('resize');
    },
    hideSide: function(){
      if (!this.open) return;
      this.$el.removeClass('open');
      this.open = false;
      App.trigger('resize');
    },
    clickFolderTitle: function(e){
      $(e.currentTarget).parent().toggleClass('open');
    },

    addSub: function(kloj){
      if (!kloj.id) throw new Error('Cannot sub to kloj without id!');

      this.$subs.addClass('open');
      var $li = $('<li data-id="'+kloj.id+'" class="'+kloj.id+'"/>').hide()
        .append('<span class="kloj-title">'+kloj.getTitle()+'</span>')
        .append('<div class="kloj-icon"/>');

      if (this.activeId == 'klojfolder'){
        $li.addClass('active');
      }

      this.$subs.find('.folderlist').append($li);
      $li.fadeIn('slow');
    },
    delSub: function(kloj){
      if (!kloj.id) throw new Error('Cannot unsub to kloj without id!');

      this.$subs.addClass('open');
      var $li = this.$subs.find('li[data-id="'+kloj.id+'"]');
      $li.fadeOut('slow', function(){
        $(this).remove();
      });
    },

    clickPage: function(e){
      e.preventDefault();
      e.stopPropagation();
      this.$('i').tooltip('hide');
      var p = e.currentTarget,
          url = $(p).attr('data-url'),
          id = $(p).attr('id');
      App.Router.navigate(url, {trigger:true});
    },

    clickLogout: function(e){
      e.preventDefault();
      e.stopPropagation();
      App.User.logout().done(function(){
        // App.view.renderView(App.view.currentView);
        App.view.refresh();
      });
    }

  });

  return SidebarView;
});