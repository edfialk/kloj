define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      template = require('text!templates/signupTemplate.html');

  var SignupView = Backbone.View.extend({

    title: 'Signup',

    className: 'signup-page',

    events: {
      'submit #signup' : 'submit'
    },

    render: function(){
      if (App.User.isLoggedIn()){
        this.$el.html("<div class='well'>You must <a href='#/logout'>logout</a> before registering.</div>");
        return;
      }

      this.$el.html(template);
      return this.el;
    },

    submit: function(event){

        var form = event.target;
        if (!form.checkValidity()){
          return;
        }
        event.preventDefault();

        var $status = this.$("#status");
        $status.html("<img src='imgs/gif-load.gif'>");

        var view = this,
            username = $("#username").val().trim(),
            email = $("#email").val().trim(),
            pwd = $("#password").val().trim();

        $.post("system/user/register.php",
          {
            username: username,
            email: email,
            password: pwd
          },
          function( data ) {
            $status.html("");
            if (data.status == "error"){
              if (data.message== "Username taken"){
                $status.html("Username is taken.");
                $("#username").focus();
                return;
              }
              if (data.message == "Invalid password"){
                $status.html("Invalid password.");
                $("#password").focus();
                return;
              }
              if (data.message == "Email taken"){
                $status.html("Email address is already registered.");
                $("#email").focus();
                return;
              }
              return;
            }else{
              $("#username").val("");
              $("#email").val("");
              $("#username").val("");
              //$("#sp_reg_status").html("Success! You may now log in.");
              App.User.login(username, pwd, function(){
                App.Router.navigate("", {trigger: true});
              });
            }
          },
          "json"
        );
    }//end register

  }); //end new AccountView()

  return SignupView;
});