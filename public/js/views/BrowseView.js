define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Klojs = require('collections/Klojs'),
      Feeds = require('collections/Feeds'),
      template = require('text!templates/browseTemplate.html');

  var BrowseView = Backbone.View.extend({

    id: 'browse',

    events: {
      'click .btn-sub': 'click_subscribe',
      'click .btn-unsub': 'click_unsubscribe',
      'click .kloj-view-block': 'click_view',
      'submit .form-search': 'search'
    },

    initialize: function(options) {
      _.bindAll(this);
      this.feeds = new Feeds();
      this.feeds.comparator = function(feed){
        return feed.get('title');
      };
      this.feeds.deferred = this.feeds.fetch();
      this.klojs = new Klojs();
      this.klojs.deferred = this.klojs.fetch();

      App.cache.browse = this;
    }, //end init

    render: function(){
      var view = this;
      this.$el.html('<div class="loader"></div>');

      $.when(App.User.deferred, view.klojs.deferred, view.feeds.deferred).then(
        function(){
          if (!view.categories){
            view.categories = view.feeds.getCategories();
            view.categories.sort(function(a, b){
              return a.value > b.value;
            });
          }

          var allKloj = new Klojs();
          var featuredKloj = new Klojs([], {
            comparator: function(kloj){
              return kloj.get('updated_at');
            }
          });

          _.each(view.klojs.models, function(kloj){
            allKloj.add(kloj);
            if (kloj.get('isFeatured')){
              featuredKloj.add(kloj);
            }
          });

          var data = {
            categories: view.categories,
            // klojs: view.klojs,
            allKloj: allKloj,
            featuredKloj: featuredKloj,
            feeds: view.feeds,
            User: App.User,
            _: _
          };

          view.$el.html( _.template( template, data ) );
          App.renderNav('browse');
        },
        function(){
          console.log(arguments);
          view.$el.html("<div class='alert alert-dange'>Error, check console</div>");
        }
      );
      return this.el;
    },

    click_subscribe: function(event){
      event.preventDefault();
      event.stopPropagation();
      var $t = $(event.target);
      $t.addClass("disabled");
      var id = $.attr(event.target, 'data-id');
      App.User.subscribeTo(id, function(response){
        if (response.status == 'success'){
          $t.replaceWith($("<button class='btn btn-unsub btn-primary' data-id='"+id+"'>Unfollow</button>"));
        }else{
          console.log("error subscribing: " + response);
        }
      });
    },
    click_unsubscribe: function(event){
      event.preventDefault();
      event.stopPropagation();
      var $t = $(event.target);
      $t.addClass("disabled");
      var id = $.attr(event.target, 'data-id');
      App.User.unsubscribeTo(id, function(response){
        if (response.status == 'success'){
          $t.replaceWith($("<button class='btn btn-sub btn-success' data-id='"+id+"'>Follow</button>"));
        }else{
          console.log("error unsubscribing: " + response);
        }
      });
    },

    click_view: function(e){
      var id = e.target.getAttribute('data-id');
      App.Router.navigate(App.klojRoot + id, {trigger: true});
    },

    search: function(e){
      e.preventDefault();
    }
  });

  return BrowseView;
});
