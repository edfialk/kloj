define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      Posts = require('collections/Posts'),
      blockTemplate = require('text!templates/blockTemplate.html'),
      homeTemplate = require('text!templates/home/homeTemplate.html'),
      navTemplate = require('text!templates/navTemplate.html'),
      recentBlockTemplate = require('text!templates/home/recentBlock.html'),
      introTemplate = require('text!templates/home/intro.html'),
      overlayTemplate = require('text!templates/overlayTemplate.html'),
      SettingsView = require('views/home/SettingsView'),
      PostView = require('views/kloj/PostView');

  var HomeView = Backbone.View.extend({

    id: 'home',
    // waitForTrigger: true,

    events: {
      'click #recent .post-title' : 'clickRecentItem',
      'click #recent .image-wrap img' : 'clickRecentItem',
      'click .post-close' : 'closePost',
      'click #customize' : 'clickCustomize'
    },

    initialize: function(options) {
      _.bindAll(this);
      App.cache.home = this;
      this.$body = $('body');
      this.selected = null;
    },

    close: function(){
      if (this.selected !== null){
        this.closePost();
      }
      this.stopListening();
      $(document).off('keydown', this.keyListener);
    },

    render: function(){
      var view = this;

      if ($('.block', this.$el).length !== 0){
        //if cached
        this.delegateEvents();
        this.listenTo(this.recent, 'change:open', this.openListener);
        return this.el;
      }
      this.$el.css({
        'opacity' : 0,
        'transition' : '0.3s all'
      });
      this.$el.html(homeTemplate);
      this.$recent = this.$('#recent');
      this.$intro = this.$('#intro');

      $.getJSON(App.root + 'user/recent', function(json){
        json.length > 0 ? view.renderRecent(json) : view.renderIntro();
      });

      App.renderNav('home');
      this.renderRecent();

      $(document).on('keydown', this.keyListener);

      return this.el;
    },

    renderRecent: function(json){
      var view = this,
          $ul = $('<ul class="block-items" />');

      var posts = new Posts(json);
      view.recent = posts;
      view.recentJson = json;
      _.each(posts.models, function(post){
        var postview = new PostView({
          model: post,
          mode: 'Home'
        });
        $ul.append(_.template(recentBlockTemplate, {post: post}));
        view.listenTo(post, 'change:open', view.openListener);
      });
      view.$recent.html($ul);
      view.trigger('render');
      view.$el.css('opacity', 1);
      this.$recentList = $ul;
    },

    renderIntro: function(){
      this.$recent.hide();
      this.$intro.html(_.template(introTemplate));

      this.$intro.removeClass('hidden');
    },

    openListener: function(post){

      if (post.get('open')){
        this.openPost(post);
      }else{
        this.closePost();
      }

    },

    openPost: function(post){
      var view = this;

      if (!post.get('content')){
        post.get('view').renderContent();
      }

      this.selected = post;

      var overlay = _.template(overlayTemplate, {content: ''});
      this.$overlay = $(overlay).css('top', 0);
      this.$body.addClass('overlayed');
      this.$el.append(this.$overlay);

      var $content = post.get('content');
      var $img = $('.post-image', $content);
      if ($img.length > 0 && !$img.hasClass('loaded')){
        $img.hide();
        var prediv = document.createElement('div'),
          preimage = document.createElement('img'),
          image = post.get('images')[0];
        prediv.setAttribute('class', 'preload');
        preimage.setAttribute('src', post.getThumbnailByHeight(120));
        preimage.setAttribute('style', 'width:'+image.width+'px;');
        prediv.appendChild(preimage);
        $img.after(prediv);

        $img.load(function(){
          $(prediv).remove();
          $img.show().addClass('loaded');
        });
      }

      this.$overlay.find('.contentWrap').append($content);
      this.$overlay.find('.screenWrap').click(function(e){
        if (e.target == this){
          view.closePost();
        }
      });

    },

    closePost: function(){
      this.selected.set('open', false);
      this.selected = null;
      this.$body.removeClass('overlayed');
      this.$overlay.remove();
    },

    clickRecentItem: function(e){
      e.preventDefault();
      e.stopPropagation();
      var id = $(e.target).parents('li[data-id]').attr('data-id'),
        post = this.recent.findWhere({id: id});
      post.set('open', true);
      // this.$recentBody.html(postview.renderContent());
    },

    clickKlojLink: function(e){
      e.stopPropagation();
    },

    keyListener: function(e){
      if (this.selected && this.selected.get('open') && e.keyCode == 27){
        this.selected.set('open', false);
      }
    },

    clickCustomize: function(e){
      if (!this.settingsView){
        this.settingsView = new SettingsView();
        this.$settings = this.settingsView.render();
      }
      this.$body.addClass('overlayed');
      this.$el.append(this.$settings);
    }

  });

  return HomeView;
});