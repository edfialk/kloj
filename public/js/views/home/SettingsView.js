define(function(require, exports, module){
  "use strict";
  var App = require('app'),
      template = require('text!templates/home/settings.html');

  var SettingsView = Backbone.View.extend({

    id: 'settings',
    className: 'overlay',

    initialize: function(options){

    },

    render: function(){
      this.$el.html(template);
      return this.el;
    }

  });

  return SettingsView;
});